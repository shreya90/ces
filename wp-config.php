<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ces2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=AbH+)s@ OhAEF!TsL8 Ap[*bL|:5ggOZ22?KTZMn^JiOejb.6G (wlcz<&5ZhdH');
define('SECURE_AUTH_KEY',  '`~l94&!2Z672G^h``:O;?0GYB/`yJrvx2h&aRA|Bq]{Zi3U$^Hl4{@uFP3WmsZb.');
define('LOGGED_IN_KEY',    'Vj*G=f)q:4$}fDrlIgBHR=X_{L7{%afk9#&mKz.EbSI`fM.}Nm%pV#jql.Vih&SH');
define('NONCE_KEY',        'c*Q^q9^wv2dVjj LY}[IoxQ#G^C%{%6+fD(1 U;ABRBEdX|u8h-gwekZt4v`3pKL');
define('AUTH_SALT',        '}VhcJ&H;.L$b{{1o#3~@0%sfo^Qh94v?bb3|RFWqycfBD`dBA|!IE ,M?p=D.yUP');
define('SECURE_AUTH_SALT', 'HUC,ru[_OiS2<{!Ou$DXudbxOpM0$KkH+h<!D+CY[F_U lM7Q!>wQh>>WG{7!hJW');
define('LOGGED_IN_SALT',   'zK$5,&3YeYn.|V%dr]gnwe^ce$htQ@#DJ-M),-KB/[&EkE2]15*:2_/!0cjl5e)L');
define('NONCE_SALT',       'jV%s?n<&8TQ`k`P<o=,.x[2t:jV_]&tR%yYOuf;UkCNk=j3Fo@;^7]0ieU8j{vKw');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

