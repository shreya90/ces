== Changelog ==

= 1.2 =
* Fixed error in function set_user_id()

= 1.1 =
* Added compatibility with LearnDash v2.4 course prerequisite changes

= 1.0 =
* Initial