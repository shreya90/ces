<?php
namespace AL;

if ( !defined( "ABSPATH" ) ) exit;

/**
 * Upgrade function
 */
function al_upgrade () {
    // upgrade process
}

/**
 * Checks if a parent course or child course
 */
function is_parent_course ( $course ) {
	if ( !$course->ID ) {
		return false;
	}

	$post_meta = get_post_meta ( $course->ID, "_sfwd-courses", 1 );
	if ( !$post_meta["sfwd-courses_course_prerequisite"] ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Fetches quiz IDs by course ID
 */
function get_quiz_ids_for_course ( $id ) {
	if ( !$id ) {
		return false;
	}

	global $wpdb;
	$results = $wpdb->get_results (
    	$wpdb->prepare ( "SELECT post_id AS quiz_id FROM {$wpdb->prefix}postmeta WHERE meta_key = 'course_id' AND meta_value = %d", $id ) );

	$arr = [];
	if ( $results ) {
		foreach ( $results as $result ) {
			$arr[] = $result->quiz_id;
		}
	}

	return $arr;
}