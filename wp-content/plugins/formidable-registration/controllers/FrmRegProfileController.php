<?php

/**
 * @since 2.0
 */
class FrmRegProfileController {

	/**
	 * Show usermeta on profile page
	 */
	public static function show_user_meta() {
		global $profileuser;

		$meta_keys = array();

		// Get register user actions for all forms
		$register_actions = FrmFormAction::get_action_for_form( 'all', 'register' );

		foreach ( $register_actions as $opts ) {
			if ( ! isset( $opts->post_content['reg_usermeta'] ) || empty( $opts->post_content['reg_usermeta'] ) ) {
				continue;
			}

			foreach ( $opts->post_content['reg_usermeta'] as $user_meta_vars ) {
				$meta_keys[ $user_meta_vars['meta_name'] ] = $user_meta_vars['field_id'];
			}
		}

		//TODO: prevent duplicate user meta from showing

		if ( ! empty( $meta_keys ) ) {
			include( FrmRegAppHelper::path() . '/views/show_usermeta.php' );
		}
	}

	// TODO: maybe move to model
	public static function get_avatar( $avatar = '', $id_or_email, $size = '96', $default = '', $alt = false, $args = array() ) {
		// Don't override the default, and stop here if Pro is not installed
		if ( ( ! empty( $args ) && isset( $args['force_default'] ) && $args['force_default'] ) ||
		     ! class_exists( 'FrmProFieldsHelper' ) ) {

			return $avatar;
		}

		if ( is_numeric( $id_or_email ) ) {
			$user_id = (int) $id_or_email;
		} else if ( is_string( $id_or_email ) ) {
			if ( $user = get_user_by( 'email', $id_or_email ) ) {
				$user_id = $user->ID;
			}
		} else if ( is_object( $id_or_email ) && ! empty( $id_or_email->user_id ) ) {
			$user_id = (int) $id_or_email->user_id;
		}

		if ( ! isset( $user_id ) ) {
			return $avatar;
		}

		$avatar_id = get_user_meta( $user_id, 'frm_avatar_id', true );

		if ( ! $avatar_id ) {
			return $avatar;
		}

		// TODO: get sizes on this site
		if ( $size < 150 ) {
			$temp_size = 'thumbnail';
		} else if ( $size < 250 ) {
			$temp_size = 'medium';
		} else {
			$temp_size = 'full';
		}

		if ( is_callable( 'FrmProFieldsHelper::get_displayed_file_html' ) ) {
			$local_avatars = FrmProFieldsHelper::get_displayed_file_html( (array) $avatar_id, $temp_size );
		} else if ( is_callable( 'FrmProFieldsHelper::get_media_from_id' ) ) {
			$local_avatars = FrmProFieldsHelper::get_media_from_id( $avatar_id, $temp_size );
		}

		if ( ! isset( $local_avatars ) || empty( $local_avatars ) ) {
			return $avatar;
		}

		if ( ! is_numeric( $size ) ) {
			// ensure valid size
			$size = '96';
		}

		if ( empty( $alt ) ) {
			$alt = get_the_author_meta( 'display_name', $user_id );
		}

		$author_class = is_author( $user_id ) ? ' current-author' : '';
		$avatar       = "<img alt='" . esc_attr( $alt ) . "' src='" . $local_avatars . "' class='avatar avatar-{$size}{$author_class} photo' height='{$size}' width='{$size}' />";

		return $avatar;
	}

}