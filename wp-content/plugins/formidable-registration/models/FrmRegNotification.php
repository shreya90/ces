<?php

class FrmRegNotification {

	/**
	 * Trigger all actions that have "Successful user registration" selected as an event
	 *
	 * @since 2.0
	 *
	 * @param int $form_id
	 * @param int $entry_id
	 */
	public static function trigger_after_registration_actions( $form_id, $entry_id ) {
		FrmFormActionsController::trigger_actions( 'user_registration', $form_id, $entry_id );
	}

    /*
    * Sends activation link to pending user
    *
    * @since 1.11
    *
    * @param integer $user_id
    * @param string $key
    * @return email
    */
	public static function new_user_activation_notification( $user_id, $key = '' ) {
		global $wpdb;

		$user = new WP_User( $user_id );

		$user_login = stripslashes( $user->user_login );
		$user_email = stripslashes( $user->user_email );

		if ( empty( $key ) ) {
			$key = $user->user_activation_key;

			if ( empty( $key ) ) {
				$key = wp_generate_password( 20, false );
				$wpdb->update( $wpdb->users, array( 'user_activation_key' => $key ), array( 'user_login' => $user_login ) );
			}

		}

		$blogname = FrmRegAppHelper::get_site_name();

        // Create activation URL
        $params = array('action' => 'frm_activate_user', 'key' => $key, 'login' => rawurlencode( $user_login ) );
        $activation_url = FrmRegModerationController::create_ajax_url( $params );

		$title    = sprintf( __( '[%s] Activate Your Account', 'frmreg' ), $blogname );
		$message  = sprintf( __( 'Thanks for registering at %s! To complete the activation of your account please click the following link: ', 'frmreg' ), $blogname ) . "\r\n\r\n";
		$message .=  $activation_url . "\r\n";

        // Hooks to customize subject and message
        $title   = apply_filters( 'user_activation_notification_title',   $title,   $user_id );
        $message = apply_filters( 'user_activation_notification_message', $message, $activation_url, $user_id );
        $content_type = apply_filters('frmreg_email_content_type', 'plain', array('email' => 'activation'));
        if ( $content_type != 'plain' ) {
            add_filter( 'wp_mail_content_type', 'FrmRegNotification::set_html_content_type' );
        }

        $result = wp_mail( $user_email, $title, $message );
        remove_filter( 'wp_mail_content_type', 'FrmRegNotification::set_html_content_type' );

        return $result;
	}

    /*
    * Allow an HTML email
    */
    public static function set_html_content_type() {
        return 'text/html';
    }

	/*
    * Send Formidable and Registration emails for users who have just confirmed their email address
    *
    * @since 1.11
    */
	// TODO: rename
	public static function send_all_notifications( $user ) {
        if ( ! is_object( $user ) ) {
            $user = new WP_User( $user );
        }

        // Get entry ID from user meta
        $entry_id = get_user_meta( $user->ID, 'frmreg_entry_id', 1 );
        $entry = FrmEntry::getOne( $entry_id );

		self::trigger_after_registration_actions( $entry->form_id, $entry->id );
	}

    /*
    * Resend activation link to pending user
    */
	public static function resend_activation_notification() {
        if ( isset( $_GET['user_id'] ) && is_numeric( $_GET['user_id'] ) ) {

    		// Send activation e-mail
    		FrmRegNotification::new_user_activation_notification( $_GET['user_id'], '' );

            // Get login URL
	        $login_url = FrmRegLoginController::login_page_url( 'wordpress' );
	        $login_url = add_query_arg( 'frm_message', 'activation_sent', $login_url );

	        //Redirect user now
	        wp_redirect( $login_url );
        }
		exit();
    }

	public static function new_user_notification() {
		_deprecated_function( __FUNCTION__, '2.0', 'an email action');
	}

	public static function send_paid_user_notification() {
		_deprecated_function( __FUNCTION__, '2.0', 'an email action');
	}

	public static function stop_entry_create_emails() {
		_deprecated_function( __FUNCTION__, '2.0', 'custom code');
	}
}