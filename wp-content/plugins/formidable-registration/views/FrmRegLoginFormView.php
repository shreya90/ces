<?php

/**
 * @since 2.0
 */
class FrmRegLoginFormView {

	/**
	 * Render a login form's HTML
	 *
	 * @since 2.0
	 *
	 * @param FrmRegLoginForm $login_form
	 */
	public static function render( $login_form ) {

		self::render_opening_tags( $login_form );
		self::render_username_field( $login_form );
		self::render_password_field( $login_form );
		self::render_hidden_fields( $login_form );

		if ( $login_form->get_layout() == 'v' ) {
			do_action( 'login_form' );
		}

		self::render_submit_button( $login_form );

		if ( $login_form->get_layout() == 'h' ) {
			do_action( 'login_form' );
		}

		self::render_remember_and_lost_password_elements( $login_form );

		self::render_closing_tags();
	}


	/**
	 * Render the opening tags in a login form
	 *
	 * @since 2.0
	 *
	 * @param FrmRegLoginForm $login_form
	 */
	private static function render_opening_tags( $login_form ) {
		?>
		<div id="<?php echo esc_attr( $login_form->get_html_id() ) ?>" class="<?php echo esc_attr( $login_form->get_class() ) ?>">
			<?php self::render_slide_html( $login_form ) ?>
			<form method="post" action="<?php echo esc_url( site_url( 'wp-login.php', 'login_post' ) ); ?>" ><?php
				self::display_errors( $login_form );
				self::display_success_messages( $login_form );?>
				<div class="frm_form_fields">
					<fieldset>
		<?php
	}

	/**
	 * Display the form errors
	 *
	 * @since 2.0
	 *
	 * @param FrmRegLoginForm $login_form
	 */
	private static function display_errors( $login_form ) {
		if ( $login_form->get_show_messages() && count( $login_form->get_errors() ) > 0 ) {
			?>
			<div class="frm_error_style">
				<?php foreach ( $login_form->get_errors() as $error ) : ?>
					<?php echo $error; ?>
				<?php endforeach; ?>
			</div>
			<?php
		}
	}

	/**
	 * Display the form success messages
	 *
	 * @since 2.0
	 *
	 * @param FrmRegLoginForm $login_form
	 */
	private static function display_success_messages( $login_form ) {
		if ( $login_form->get_show_messages() && count( $login_form->get_messages() ) > 0 ) {
			?>
			<div class="frm_message">
				<?php foreach ( $login_form->get_messages() as $message ) : ?>
					<?php echo $message; ?>
				<?php endforeach; ?>
			</div>
			<?php
		}
	}

	/**
	 * Render the HTML for a sliding login form if slide property is true
	 *
	 * @since 2.0
	 *
	 * @param FrmRegLoginForm $login_form
	 */
	private static function render_slide_html( $login_form ) {
		if ( $login_form->get_slide() ) {
			?>
			<span class="frm-open-login">
				<a href="#"><?php echo $login_form->get_submit_label() ?> &rarr;</a>
			</span><?php
		}
	}

	/**
	 * Render the username field
	 *
	 * @since 2.0
	 *
	 * @param FrmRegLoginForm $login_form
	 */
	private static function render_username_field( $login_form ) {
		?>
		<div class="<?php echo esc_attr( $login_form->get_username_class() ) ?>">
		<label for="<?php echo esc_attr( $login_form->get_username_id() ) ?>" class="frm_primary_label"><?php
			echo esc_html( $login_form->get_username_label() ) ?>
		</label>
		<input id="<?php echo esc_attr( $login_form->get_username_id() ) ?>" name="log" value="<?php echo esc_attr( $login_form->get_username_value() ) ?>" placeholder="<?php echo esc_attr( $login_form->get_username_placeholder() ) ?>" type="text">
		</div>

		<?php
	}

	/**
	 * Render the password field
	 *
	 * @since 2.0
	 *
	 * @param FrmRegLoginForm $login_form
	 */
	private static function render_password_field( $login_form ) {
		?>
		<div class="<?php echo esc_attr( $login_form->get_password_class() ) ?>">
			<label for="<?php echo esc_attr( $login_form->get_password_id() ) ?>" class="frm_primary_label"><?php
				echo esc_html( $login_form->get_password_label() ) ?>
			</label>
			<input id="<?php echo esc_attr( $login_form->get_password_id() ) ?>" name="pwd" value="" type="password" placeholder="<?php echo esc_attr( $login_form->get_password_placeholder() ) ?>" >
		</div>
		<?php
	}

	/**
	 * Render hidden redirect field
	 *
	 * @since 2.0
	 *
	 * @param FrmRegLoginForm $login_form
	 */
	private static function render_hidden_fields( $login_form ) {
		?><input type="hidden" name="redirect_to" value="<?php echo esc_url( $login_form->get_redirect() ) ?>" /><?php
	}

	/**
	 * Render remember me and lost password elements
	 *
	 * @since 2.0
	 *
	 * @param FrmRegLoginForm $login_form
	 */
	private static function render_remember_and_lost_password_elements( $login_form ) {
		self::render_remember_me_checkbox( $login_form );
		self::render_lost_password_link( $login_form );
		self::render_clear_both_div();
	}

	/**
	 * Render remember me checkbox
	 *
	 * @since 2.0
	 *
	 * @param FrmRegLoginForm $login_form
	 */
	private static function render_remember_me_checkbox( $login_form ) {
		if ( $login_form->get_show_remember() ) {
			?><div class="<?php echo esc_attr( $login_form->get_remember_class() ) ?>">
				<div class="frm_opt_container">
					<div class="frm_checkbox">
						<label for="<?php echo esc_attr( $login_form->get_remember_id() ) ?>">
						<input name="rememberme" id="<?php echo esc_attr( $login_form->get_remember_id() ) ?>" value="forever"<?php echo ( $login_form->get_remember_value() ? ' checked="checked"' : '' )?> type="checkbox"><?php
						echo esc_html( $login_form->get_remember_label() )?>
						</label>
					</div>
				</div>
			</div><?php
		}
	}

	/**
	 * Render lost password link if lost_password link is true
	 *
	 * @since 2.0
	 *
	 * @param FrmRegLoginForm $login_form
	 */
	private static function render_lost_password_link( $login_form ) {
		if ( $login_form->get_show_lost_password_link() ) {
			?><div class="<?php echo esc_attr( $login_form->get_lost_password_class() ) ?>">
				<a class="forgot-password" href="<?php echo esc_url( wp_lostpassword_url() ) ?>">
					<?php echo esc_html( $login_form->get_lost_password_label() ) ?>
				</a>
			</div><?php
		}
	}

	/**
	 * Render the clear:both div
	 *
	 * @since 2.0
	 */
	private static function render_clear_both_div() {
		?><div style="clear:both;"></div><?php
	}

	/**
	 * Render submit button
	 *
	 * @since 2.0
	 *
	 * @param FrmRegLoginForm $login_form
	 */
	private static function render_submit_button( $login_form ) {
		?><div class="<?php echo esc_attr( $login_form->get_submit_class() ) ?>">
		<input type="submit" name="wp-submit" id="<?php echo esc_attr( $login_form->get_submit_id() ) ?>" value="<?php echo esc_attr( $login_form->get_submit_label() ) ?>" />
		</div>
		<div style="clear:both;"></div>
		<?php
	}

	/**
	 * Render the closing tags for a login form
	 *
	 * @since 2.0
	 */
	private static function render_closing_tags() {
		?>
					</fieldset>
				</div>
			</form>
		</div>
		<?php
	}
}