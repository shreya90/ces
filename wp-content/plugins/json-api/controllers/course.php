<?php

/*
  Controller name: Course
  Controller description: Basic introspection methods
 */

class JSON_API_Course_Controller {

     public function get_courses($user_id) {
        global $wpdb;
        $user_id = $_REQUEST['user_id'];
       if ($user_id == '') {
            $response = array("course_data" => $course_data);
            return (array("status" => false, "message" => "All fields required", "data" => array()));
            die;
        } else {
        $course = $wpdb->get_results("
      SELECT p.post_title , p.ID ,p.post_content ,pm.meta_value
      FROM $wpdb->posts AS p,
           $wpdb->postmeta AS pm
      WHERE pm.meta_key = '_sfwd-courses'
        AND p.ID = pm.post_id AND p.post_status = 'publish';    
    ");
        $course_data = array();
        $i = 0;
        foreach ($course as $course_count) {
            $course_data[$i]['course_id'] = $course_count->ID;
            $course_data[$i]['course_title'] = $course_count->post_title;
            $course_data[$i]['course_desc'] = $course_count->post_content;
            $meta_val = unserialize($course_count->meta_value);
            
            if(count($meta_val)>0)
            {
                $course_price = isset($meta_val['sfwd-courses_course_price'])?$meta_val['sfwd-courses_course_price']:0;
                if($course_price == ""){
                   $course_price = 0;
                }   
            }
            else
            {
                $course_price = 0;
            }
            $course_data[$i]['course_price'] = $course_price;
            
              $access = $meta_val['sfwd-courses_course_access_list'];
                $accs = explode(',', $access);
                //print_r($accs);
                if (in_array($user_id, $accs))
                        
               {
                    $course_data[$i]['is_enrolled'] = "true";
               }else{
                    $course_data[$i]['is_enrolled'] = "false";
               }
            
            
            
            $id = $course_data[$i]['course_id'];
            $image = $wpdb->get_results("select wp_postmeta.meta_value from wp_postmeta where wp_postmeta.post_id ='$id' and wp_postmeta.meta_key ='_thumbnail_id'");
           if(count($image)> 0){
            foreach ($image as $images) {
                $course_data[$i]['course_image'] = wp_get_attachment_url($images->meta_value);
           }}else{
                $course_data[$i]['course_image'] = "";
           }
            $i++;
        }
      //exit;

        return (array("course_data" => $course_data, "status" => true));
        die;
     }}
   
//     public function get_courses() {
//         global $wpdb;
//         $course = $wpdb->get_results("
//       SELECT p.post_title , p.ID ,p.post_content 
//       FROM $wpdb->posts AS p,
//            $wpdb->postmeta AS pm
//       WHERE pm.meta_key = '_sfwd-courses'
//         AND p.ID = pm.post_id AND p.post_status = 'publish';    
//     ");
//         $course_data = array();
//         $i = 0;
//         foreach ($course as $course_count) {
//             $course_data[$i]['course_id'] = $course_count->ID;
//             $course_data[$i]['course_title'] = $course_count->post_title;
//              $course_data[$i]['course_desc'] = $course_count->post_content;
//            $id = $course_data[$i]['course_id'];
//            $image = $wpdb->get_results("select wp_postmeta.meta_value from wp_postmeta where wp_postmeta.post_id ='$id' and wp_postmeta.meta_key ='_thumbnail_id'");
//             foreach ($image  as $images ) {
//              $course_data[$i]['course_image_id'] = wp_get_attachment_url($images->meta_value);
//          }
//             // get lesson data by course 
// //        $res_lesson = get_lesson($course_count->ID);
// //        $course_data[$i]['lesson_data'] = $res_lesson;
//             $i++;
//         }
//         return (array("course_data" => $course_data, "status" => true));
//         die;
//     }

//    function popular_course() {
//     global $wpdb;
//    $data = $wpdb->get_results("SELECT Count(wp_learndash_user_activity.course_id) as count , wp_learndash_user_activity.course_id ,wp_posts.post_title FROM wp_learndash_user_activity inner join wp_posts where wp_learndash_user_activity.course_id = wp_posts.ID Group BY course_id order by count desc limit 5");
//      
//      for($i= 0; $i < count($data); $i++){
//        $details[$i] = $data[$i]->post_title;
//         // echo '<div id="data" style="font-size:12px;font-weight: bold;color: rgba(118,118,118,1.0);">-> '.$details.'</div>';
//      }
//   
//     return (array("course" => $details, "status" => true));
//        die;
//     }

    // function popular_course() {
    // global $wpdb;
    //     $data = $wpdb->get_results("SELECT Count(wp_learndash_user_activity.course_id) as count , wp_learndash_user_activity.course_id ,wp_posts.post_title FROM wp_learndash_user_activity inner join wp_posts where wp_learndash_user_activity.course_id = wp_posts.ID Group BY course_id order by count desc limit 5");

    //       for($i= 0; $i < count($data); $i++){
    //         $details[$i]['course_id'] = $data[$i]->course_id;
    //    $id = $details[$i]['course_id'];
    //         $details[$i]['post_title'] = $data[$i]->post_title;
    //          $image = $wpdb->get_results("select wp_postmeta.meta_value from wp_postmeta where wp_postmeta.post_id ='$id' and wp_postmeta.meta_key ='_thumbnail_id'");
    //          foreach ($image  as $images ) {
    //              $details[$i]['course_image_id'] = wp_get_attachment_url($images->meta_value);
    //          }
    //       }

    //      return (array("course" => $details, "status" => true));
    //    die;
    // }
    
public function edit_avatar($user_id) {
    echo "sdfsadf" ;die();
    global $wpdb;
    $response = FrmProFileField::ajax_upload_avatar();
    //print_r($response);exit;
    $media_ids = $response['media_ids'][0];
    //print_r($media_ids);exit;
    $user_id = $_REQUEST['user_id'];
    $select = $wpdb->get_results("select wp_frm_items.id from wp_frm_items where wp_frm_items.user_id = $user_id");
        $data = $select[0]->id;
    $update = $wpdb->get_results("update wp_frm_item_metas set meta_value = '$media_ids where field_id = 176 AND item_id = '$data");
    $update1 = $wpdb->get_results("UPDATE wp_usermeta SET meta_value = $media_ids WHERE user_id = $user_id AND meta_key = 'frm_avatar_id'");
    return (array("status" => true));
    die;
//       global $wpdb;
//       $user_id = $_REQUEST['user_id'];
//       
//       $pp_avatar = pp_get_avatar_url( $user_id = '', $size = null );
//       print_r($pp_avatar);exit;
//      $avatar = $wpdb->get_results("select wp_usermeta.meta_value from wp_usermeta where wp_usermeta.meta_key = 'frm_avatar_id' and wp_usermeta.user_id = '$user_id'");
//     // echo $avatar[0]->meta_value;  exit;
//      $avatar_link =  wp_get_attachment_url($avatar[0]->meta_value);
// //       print_r($att);exit;
//      return $avatar_link; 
   }

function popular_course($user_id) {
        global $wpdb;
        $data = $wpdb->get_results("SELECT Count(wp_learndash_user_activity.course_id) as count , wp_learndash_user_activity.course_id ,wp_posts.post_title  FROM wp_learndash_user_activity inner join wp_posts where wp_learndash_user_activity.course_id = wp_posts.ID Group BY course_id order by count desc limit 5");

        for ($i = 0; $i < count($data); $i++) {
            $details[$i]['course_id'] = $data[$i]->course_id;
            $id = $details[$i]['course_id'];
            $details[$i]['post_title'] = $data[$i]->post_title;
            
             $price = $wpdb->get_results("select wp_postmeta.meta_value from wp_postmeta where wp_postmeta.post_id ='$id' and wp_postmeta.meta_key ='_sfwd-courses'");
            foreach ($price as $prices) {
                
            $meta_val = unserialize($prices->meta_value);
            if(count($meta_val)>0)
            {
                $course_price = isset($meta_val['sfwd-courses_course_price'])?$meta_val['sfwd-courses_course_price']:0;
                if($course_price == ""){
                   $course_price = 0;
                }   
            }
            else
            {
                $course_price = 0;
            }
            $details[$i]['course_price'] = $course_price;
               
            $access = $meta_val['sfwd-courses_course_access_list'];
                $accs = explode(',', $access);
                //print_r($accs);
                if($user_id != ''){
                if (in_array($user_id, $accs))
               {
                    $details[$i]['is_enrolled'] = true;
               }else{
                    $details[$i]['is_enrolled'] = false;
               }
                }else{
                    $details[$i]['is_enrolled'] = false;
                } 
            }
            
            $image = $wpdb->get_results("select wp_postmeta.meta_value from wp_postmeta where wp_postmeta.post_id ='$id' and wp_postmeta.meta_key ='_thumbnail_id'");
            foreach ($image as $images) {
                $details[$i]['course_image_id'] = wp_get_attachment_url($images->meta_value);
            }
        }

        return (array("course" => $details, "status" => true));
        die;
    }


    
      public function course_detail() {
        $course_id = $_REQUEST['course_id'];
//        $course = $this->get_courses();
        if ($course_id == '' || $course_id == '0') {
            $response = array("course_data" => $course_data);
            return (array("status" => false, "message" => "All fields required", "data" => array()));
            die;
        } else {
            $course_data = array();

            $level_fields = array(
                'action' => 'select_a_topicapi',
                'course_id' => $course_id,
            );
            // level == lesson
            $topics = $this->httpPost("http://192.168.1.140/ces2/wp-admin/admin-ajax.php", $level_fields);
            $topics_data = json_decode($topics, true);
//            $topics_data = array();
            if (empty($topics_data)) { // check for empty
                $course_data['topic_data'] = array();  // if null, pass blank array
            } else {
                $lesson_data = $topics_data['topic_data']; // level == lesson
                $course_data['topic_data'] = $lesson_data;
                $j = 0; // for level
                foreach ($lesson_data as $lesson_datas) {
                    $lesson_id = "";
                    $lesson_id = $lesson_datas['topic_id'];
                    $topicdata = learndash_course_get_children_of_step($course_id,$lesson_id, 'sfwd-topic');
                    if(count($topicdata) > 0 )
                    {
                    foreach ($topicdata as $topic) {
                        $levels = array();
                     $levels['level_id'] =   $topic;   //print_r($topic); exit;
                     $levels['level_title'] =  get_the_title( $topic );
                               $course_data['topic_data'][$j]['level_data'][] = $levels;                              
                        }   
                    }
                    else
                    {
                        $course_data['topic_data'][$j]['level_data'][] = array(); // if null, pass blank array
                    }
                    $j++; // for level
                }
            }
            $response = array("course_data" => $course_data);
            return (array("status" => true, "message" => "Success", "data" => $response));
            die;
        }
    }
    
    
     public function level($course_id , $lesson_id) {
         $course_id = $_REQUEST['course_id'];
         $lesson_id = $_REQUEST['lesson_id'];
         
         if ($course_id == '' || $lesson_id == ''){
           $response = array("level_data" => $level_data);
            return (array("status" => false, "message" => "All fields required", "data" => array()));
            die;  
         } else{
         $leveldata = learndash_course_get_children_of_step($course_id,$lesson_id, 'sfwd-topic');
         $i = 0;
        $levels = array();
         foreach ($leveldata as $level) {
                     
                     $levels[$i]['level_id'] =  $level;   //print_r($topic); exit;
                     $levels[$i]['level_title'] =  get_the_title( $level );
                    // $course_data['topic_data'][$j]['level_data'][] = $levels;
                    $i++;
         }
     }
     $response = array("level_data" => $levels);   
              return (array("status" => true, "message" => "Success", "data" => $response));
            die;
     }
    
    
//     public function course_detail() {
//        $course_id = $_REQUEST['course_id'];
////        $course = $this->get_courses();
//        if ($course_id == '' || $course_id == '0') {
//            $response = array("course_data" => $course_data);
//            return (array("status" => false, "message" => "All fields required", "data" => array()));
//            die;
//        } else {
//            $course_data = array();
//
//            $level_fields = array(
//                'action' => 'select_a_lessonapi',
//                'course_id' => $course_id,
//            );
//            // level == lesson
//            $levels = $this->httpPost("http://cesdemo.cogisignals.com/wp-admin/admin-ajax.php", $level_fields);
//            $levels_data = json_decode($levels, true);
////            $levels_data = array();
//            if (empty($levels_data)) { // check for empty
//                $course_data['level_data'] = array();  // if null, pass blank array
//            } else {
//                $lesson_data = $levels_data['lesson_data']; // level == lesson
//                $course_data['level_data'] = $lesson_data;
//                $j = 0; // for level
//                foreach ($lesson_data as $lesson_datas) {
//                    $lesson_id = "";
//                    $lesson_id = $lesson_datas['lesson_id'];
//                    $quiz_fields = array(
//                        'action' => 'select_a_quizapi',
//                        'course_id' => $course_id,
//                        'lesson_id' => $lesson_id
//                    );
//                    $quiz = $this->httpPost("http://cesdemo.cogisignals.com/wp-admin/admin-ajax.php", $quiz_fields);
//                    $quiz_data = json_decode($quiz, true);
//                    $quiz_datas = $quiz_data['quiz_data'];
//                    if (empty($quiz_datas)) {   // check for empty
//                        $course_data['level_data'][$j]['quiz_data'] = array(); // if null, pass blank array
//                    } else {
//                        $course_data['level_data'][$j]['quiz_data'] = $quiz_datas;
//                    }
//                    $j++; // for level
//                }
//            }
//            $response = array("course_data" => $course_data);
//            return (array("status" => true, "message" => "Success", "data" => $response));
//            die;
//        }
//    }
    
    function httpPost($url, $params) {
        $postData = '';
        //create name value pairs seperated by &
        foreach ($params as $k => $v) {
            $postData .= $k . '=' . $v . '&';
        }
        $postData = rtrim($postData, '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $output = curl_exec($ch);

        curl_close($ch);
        return $output;
        die();
    }

    
    function level_details($level_id) {
        global $wpdb;
        $level_id = $_REQUEST['level_id'];
        if ($level_id != '') {
            $h5p = $wpdb->get_results("select wp_posts.post_content from wp_posts where wp_posts.ID = '$level_id'");
            $id = $h5p[0];
            $hp1 = $id->post_content;
            $id1 = explode('"', $hp1);
            $h5p_id = $id1[1];

            $get_data = $wpdb->get_results("SELECT wp_h5p_contents.parameters,wp_h5p_contents.filtered FROM wp_h5p_contents where wp_h5p_contents.id = '$h5p_id'");
            //$account_data = array();
              return (array("status" => true, "message" => "Success", "data" => $get_data));
            die;
        }else{
              return (array("status" => false, "message" => "All fields required", "data" => array()));
            die;
            
        }
    }

public function user_certificates($user_id) {
        global $wpdb;
        $user_id = $_REQUEST['user_id'];
        if ($user_id != '') {
            $account = $wpdb->get_results("select wp_posts.ID, wp_postmeta.meta_key, wp_postmeta.meta_value from wp_posts inner join wp_postmeta where wp_posts.post_author = '$user_id' and wp_postmeta.post_id = wp_posts.ID and wp_posts.post_type = 'sfwd-transactions' and wp_postmeta.meta_key= 'course_id'");
            print_r($account);exit;
            $accounts_data = array();
            $i = 0;

            foreach ($account as $accounts) {
                $accounts_data[$i]['ID'] = $accounts->ID;
                $accounts_data[$i]['course_id'] = $accounts->meta_value;
                $postid = $accounts_data[$i]['course_id'];
                $course = $wpdb->get_results("select wp_posts.post_title, wp_posts.ID from wp_posts where wp_posts.ID = '$postid'");
                foreach ($course as $courses) {
                    $accounts_data[$i]['course_title'] = $courses->post_title;
                }
                $image = $wpdb->get_results("select wp_postmeta.meta_value from wp_postmeta where wp_postmeta.post_id ='$postid' and wp_postmeta.meta_key ='_thumbnail_id'");
                foreach ($image as $images) {
                    $accounts_data[$i]['course_image'] = wp_get_attachment_url($images->meta_value);
                }
                $accounts_data[$i]['status'] = learndash_course_status($postid, $user_id = null);
                // $accounts_data[$i]['certificates'] = learndash_get_course_certificate_link($postid, $user_id = null);
                $accounts_data[$i]['certificates'] = "http://cesdemo.cogisignals.com/Certificates.pdf";
                $accounts_data[$i]['rating'] = 5;
                $i++;
            }

            $result = array();
            $r = 0;
            foreach ($accounts_data as $element) {
                $result[$r] = $element;
                $r++;
            }
            return (array("account_data" => $result, "status" => true));
            die();
        }
    }
  public function my_account($user_id) {
        global $wpdb;
        $user_id = $_REQUEST['user_id'];
        if ($user_id != '') {
            $account = $wpdb->get_results("select wp_posts.ID, wp_postmeta.meta_key, wp_postmeta.meta_value from wp_posts inner join wp_postmeta where wp_posts.post_author = '$user_id' and wp_postmeta.post_id = wp_posts.ID and wp_posts.post_type = 'sfwd-transactions' and wp_postmeta.meta_key= 'course_id'");
            $accounts_data = array();
            $i = 0;

            foreach ($account as $accounts) {
                $accounts_data[$i]['ID'] = $accounts->ID;
                $accounts_data[$i]['course_id'] = $accounts->meta_value;
                $postid = $accounts_data[$i]['course_id'];
                $course = $wpdb->get_results("select wp_posts.post_title, wp_posts.ID from wp_posts where wp_posts.ID = '$postid'");
                foreach ($course as $courses) {
                    $accounts_data[$i]['course_title'] = $courses->post_title;
                }
                $image = $wpdb->get_results("select wp_postmeta.meta_value from wp_postmeta where wp_postmeta.post_id ='$postid' and wp_postmeta.meta_key ='_thumbnail_id'");
                foreach ($image as $images) {
                    $accounts_data[$i]['course_image'] = wp_get_attachment_url($images->meta_value);
                }
                
                $stripid = $wpdb->get_results("select wp_posts.ID, wp_posts.post_date ,wp_postmeta.meta_value from wp_posts inner join wp_postmeta where wp_posts.post_author = '$user_id' and wp_postmeta.post_id = wp_posts.ID and wp_posts.post_type = 'sfwd-transactions' and wp_postmeta.meta_key = 'stripe_token_id'");
                foreach ($stripid as $traid) {
                      if(count($traid->meta_value) > 0){
                    $accounts_data[$i]['transaction_id'] = $traid->meta_value;
                      }else{
                       $accounts_data[$i]['transaction_id'] = "";   
                      }
                    $accounts_data[$i]['transaction_date'] = $traid->post_date;
                }
                
                $prise = $wpdb->get_results("select wp_postmeta.meta_value from wp_postmeta where wp_postmeta.post_id ='$postid' and wp_postmeta.meta_key ='_thumbnail_id'");
                foreach ($image as $images) {
                    $accounts_data[$i]['course_image'] = wp_get_attachment_url($images->meta_value);
                }
                
                $price = $wpdb->get_results("select wp_postmeta.meta_value from wp_postmeta where wp_postmeta.post_id ='$postid' and wp_postmeta.meta_key ='_sfwd-courses'");
            foreach ($price as $prices) {
                
                $meta_val = unserialize($prices->meta_value);
            if(count($meta_val)>0)
            {
                $course_price = isset($meta_val['sfwd-courses_course_price'])?$meta_val['sfwd-courses_course_price']:0;
                if($course_price == ""){
                   $course_price = 0;
                }   
            }else
            {
                $course_price = 0;
            }
            $accounts_data[$i]['course_price'] = $course_price;
            }   
                
                
                
                $accounts_data[$i]['status'] = learndash_course_status($postid, $user_id = null);
            
                $i++;
            }

            $result = array();
            $r = 0;
            foreach ($accounts_data as $element) {
                $result[$r] = $element;
                $r++;
            }
            return (array("account_data" => $result, "status" => true));
            die();
        }else{
             $response = array("account_data" => $result);
            return (array("status" => false, "message" => "All fields required", "data" => array()));
            die;
        }
    }


    public function my_account_old($user_id) {
        global $wpdb;
        $user_id = $_REQUEST['user_id'];
        if ($user_id != '') {
            $account = $wpdb->get_results("select wp_posts.ID, wp_postmeta.meta_key, wp_postmeta.meta_value from wp_posts inner join wp_postmeta where wp_posts.post_author = '$user_id' and wp_postmeta.post_id = wp_posts.ID and wp_posts.post_type = 'sfwd-transactions'");
            $accounts_data = array();
            $i = 0;

            foreach ($account as $accounts) {
                $accounts_data[$i]['ID'] = $accounts->ID;
                $accounts_data[$i]['meta_key'] = $accounts->meta_key;
                $accounts_data[$i]['meta_value'] = $accounts->meta_value;
                $i++;

                if($accounts->meta_key == 'stripe_course_id'){
                $id =  $accounts->meta_value;
                $image = $wpdb->get_results("select wp_postmeta.meta_value from wp_postmeta where wp_postmeta.post_id ='$id' and wp_postmeta.meta_key ='_thumbnail_id'");
                
            foreach ($image  as $images ) {
                    $accounts_data[$i]['ID'] = $accounts->ID;
                    $accounts_data[$i]['meta_key'] = 'course_image_id';
                    $accounts_data[$i]['meta_value'] = wp_get_attachment_url($images->meta_value);
                    
                    $i++;
            }
            }

            }

            $result = array();
            foreach ($accounts_data as $element) {
                $result[$element['ID']][] = $element;
            }
            return (array("account_data" => $result, "status" => true));
            die();
            }
    }

    public function my_courses($user_id) {
        global $wpdb;
        $user_id = $_REQUEST['user_id'];
        if ($user_id != '') {
            $account = $wpdb->get_results("select wp_posts.ID, wp_postmeta.meta_key, wp_postmeta.meta_value from wp_posts inner join wp_postmeta where wp_posts.post_author = '$user_id' and wp_postmeta.post_id = wp_posts.ID and wp_posts.post_type = 'sfwd-transactions' and wp_postmeta.meta_key= 'course_id'");
            $accounts_data = array();
            $i = 0;

            foreach ($account as $accounts) {
                $accounts_data[$i]['ID'] = $accounts->ID;
                $accounts_data[$i]['course_id'] = $accounts->meta_value;
                $postid = $accounts_data[$i]['course_id'];
                $course = $wpdb->get_results("select wp_posts.post_title, wp_posts.ID from wp_posts where wp_posts.ID = '$postid'");
                foreach ($course  as $courses ) {
                    $accounts_data[$i]['course_title'] = $courses->post_title;
                }
                $image = $wpdb->get_results("select wp_postmeta.meta_value from wp_postmeta where wp_postmeta.post_id ='$postid' and wp_postmeta.meta_key ='_thumbnail_id'");
          foreach ($image  as $images ) {
                 $accounts_data[$i]['course_image'] = wp_get_attachment_url($images->meta_value);
            }
         $accounts_data[$i]['status']=  learndash_course_status($postid, $user_id = null);
          $accounts_data[$i]['rating']=  5;
                $i++;
            }

            $result = array();
            $r=0;
            foreach ($accounts_data as $element) {
                $result[$r] = $element;
                $r++;
            }
            return (array("account_data" => $result, "status" => true));
            die();
        }
    }

    public function get_lesson_front($value){
         global $wpdb;
          $value = $_REQUEST['user_id'];
        $lesson = $wpdb->get_results("select wp_postmeta.meta_value from wp_postmeta where wp_postmeta.meta_key = '_sfwd-lessons' and wp_postmeta.post_id = '$value'
");
        $lesson_data = array();
        $i = 0;
        foreach ($lesson as $lesson_count) {
            $lesson_data[$i]['meta_value'] = $lesson_count->meta_value;
            $lesson_data[$i]['meta_value'] = unserialize($lesson_data[$i]['meta_value']);
            $lesson_data[$i]['meta_value'] = $lesson_data[$i]['meta_value']['sfwd-lessons_forced_lesson_time'];
            // get lesson data by course 
//        $res_lesson = get_lesson($course_count->ID);
//        $course_data[$i]['lesson_data'] = $res_lesson;
            $i++;
        }
        return (array("lesson_data" => $lesson_data, "status" => true));
        die;
        
    }
    
    public function get_lesson_front_update($id,$value){
        global $wpdb;
          $id = $_REQUEST['post_id']; 
          $value = $_REQUEST['meta_value']; 
        
      $lesson = $wpdb->get_results("update wp_postmeta set wp_postmeta.meta_value ='$value' where wp_postmeta.meta_key = '_sfwd-lessons' and wp_postmeta.post_id = '$id'
");
        if($lesson){
          return (array("status" => true));
        } else {
             return (array("status" => false));
        }
        die;  
        
        
        
    }
    
  //   NOT WORKING MODE//*****************************************************///
    public function get_course_by_id($course_id) {
        global $wpdb;
        $course = $wpdb->get_results("
      SELECT p.post_title , p.ID, p.post_content 
      FROM $wpdb->posts AS p,
           $wpdb->postmeta AS pm
      WHERE pm.meta_key = '_sfwd-courses' 
        AND p.ID = pm.post_id AND p.post_status = 'publish' AND p.ID = '$course_id';    
    ");
        $course_data = array();
        $i = 0;
        foreach ($course as $course_count) {
            $course_data[$i]['course_id'] = $course_count->ID;
            $id = $course_data[$i]['course_id'];
            $course_data[$i]['course_title'] = $course_count->post_title;
            $course_data[$i]['course_desc'] = $course_count->post_content;
            $image = $wpdb->get_results("select wp_postmeta.meta_value from wp_postmeta where wp_postmeta.post_id ='$id' and wp_postmeta.meta_key ='_thumbnail_id'");
            foreach ($image as $images) {
                $course_data[$i]['course_image_id'] = $images->meta_value;
            }
            // get lesson data by course 
//        $res_lesson = get_lesson($course_count->ID);
//        $course_data[$i]['lesson_data'] = $res_lesson;
            $i++;
        }
        return (array("course_data" => $course_data, "status" => true));
        die;
    }
    
    //******************************************************************************************//  
    
//  public function get_lesson($course_id) {
//    global $wpdb;
//    $course = $wpdb->get_results("
//      SELECT p.post_title , p.ID
//      FROM $wpdb->posts AS p,
//           $wpdb->postmeta AS pm
//      WHERE pm.meta_key = '_sfwd-courses'
//        AND p.ID = pm.post_id AND p.post_status = 'publish';    
//    ");
//    $lesson_data = array();
//    $i = 0;
//    foreach ($course as $course_count) {
//        $lesson_data[$i]['course_id']  = $course_count->ID;
//        $lesson_data[$i]['course_title']  = $course_count->post_title;
//        $lesson_data[$i]['course_title']  = $course_count->post_title;
//        $i++;
//      }
//    return $lesson_data;
//  }
}

?>