<div id="postbox-container-1" class="postbox-container">

    <div class="meta-box-sortables" style="text-align: center; margin: auto">
        <div class="postbox">
            <div class="handlediv" title="Click to toggle"><br></div>
            <h3 class="hndle ui-sortable-handle"><span><?php _e('Support / Documentation', 'profilepress'); ?></span>
            </h3>

            <div class="inside">
                <p><?php printf(__('For support, visit the %s', 'profilepress'), '<strong><a href="http://support.profilepress.net" target="blank">' . __('forum', 'profilepress') . '</a>.</strong>'); ?></p>

                <p><?php printf(__('Visit the %s for guidance', 'profilepress'), '<strong><a href="http://docs.profilepress.net" target="blank">' . __('Documentation', 'profilepress') . '</a></strong>'); ?></p>

            </div>
        </div>

        <div class="postbox">
            <div class="handlediv" title="Click to toggle"><br></div>
            <h3 class="hndle ui-sortable-handle"><span><?php _e('ProfilePress Extensions', 'profilepress'); ?></span>
            </h3>

            <div class="inside">
                <ul>
                    <li>
                        <a href="https://profilepress.net/downloads/mailchimp/" target="_blank"><?php _e('MailChimp', 'profilepress'); ?></a>: <?php _e('automatically subscribe new users to your MailChimp list.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/email-confirmation/" target="_blank"><?php _e('Email Confirmation', 'profilepress'); ?></a>: <?php _e('ensures new registered users confirm their email addresses before they can login.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/multisite/" target="_blank"><?php _e('WordPress Multisite Integration', 'profilepress'); ?></a>: <?php _e('allows front-end registration of new user sites in WordPress Multisite Network.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/join-buddypress-groups/" target="_blank"><?php _e('Join BuddyPress Groups', 'profilepress'); ?></a>: <?php _e('allows users to select BuddyPress groups to join during registration.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/campaignmonitor/" target="_blank"><?php _e('Campaign Monitor', 'profilepress'); ?></a>: <?php _e('Subscribe users to Campaign Monitor a list on registration.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/wordpress-buddypress-extended-profile-sync/" target="_blank"><?php _e('WordPress & BuddyPress Profile Sync', 'profilepress'); ?></a>: <?php _e('provides 2-way syncing of WordPress profile fields with BuddyPress extended profile.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/polylang/" target="_blank"><?php _e('Polylang Integration', 'profilepress'); ?></a>: <?php _e('build multilingual login, registration, password reset, edit profile forms & more for WordPress.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/woocommerce/" target="_blank"><?php _e('WooCommerce Integration', 'profilepress'); ?></a>: <?php _e('manage Shipping and Billing fields with ProfilePress, replace the default WooCommerce login and edit account form with that of ProfilePress and more.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/admin-bar-dashboard-access-control/" target="_blank"><?php _e('Admin Bar & Dashboard Control', 'profilepress'); ?></a>
                        <strong>(Free)</strong>: <?php _e('disables admin bar and control users access to WordPress dashboard.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/wordpress-navigation-menu-links/" target="_blank"><?php _e('WordPress Navigation Menu Links', 'profilepress'); ?></a>
                        <strong>(Free)</strong>: <?php _e('seamless addition of login, login/logout, sign up, password reset, edit profile, profile links to WordPress menu.', 'profilepress'); ?>
                    </li>
                </ul>
                <div><a href="https://profilepress.net/extensions/" target="_blank">
                        <button class="button-primary" type="button"><?php _e('Shop Now', 'profilepress'); ?></button>
                    </a></div>
            </div>
        </div>

        <div class="postbox">
            <div class="handlediv" title="Click to toggle"><br></div>
            <h3 class="hndle ui-sortable-handle"><span><?php _e('ProfilePress Themes', 'profilepress'); ?></span>
            </h3>

            <div class="inside">
                <ul>
                    <li>
                        <a href="https://profilepress.net/downloads/pinnacle-account-form/" target="_blank">Pinnacle</a>: <?php _e('a smooth and clean custom login, registration and password reset form theme with social login.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/montserrat-account-form/" target="_blank">Montserrat</a>: <?php _e('a beautiful one-page form that combines a login, registration, password reset and social login together thus eliminating the need of creating separate forms.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/stride-wordpress-multi-step-registration-form/" target="_blank">Stride</a>: <?php _e('an elegant, customizable WordPress multi-step registration form.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/bash-account-form/" target="_blank">Bash</a>: <?php _e('a simple, pretty, responsive one-page account form with social login.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/tag/parallel/" target="_blank">Parallel</a>: <?php _e('a Horizontal, responsive and aesthetic login, registration & password reset form with social login.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/bash-edit-profile-form/" target="_blank">Bash Edit
                            Profile</a>: <?php _e('an elegant, responsive front-end edit profile form for WordPress.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/memories-edit-profile-form/" target="_blank">Memories
                            edit profile
                            form</a>: <?php _e('a clean and simple front-end edit profile form for WordPres.', 'profilepress'); ?>
                    </li>
                    <li>
                        <a href="https://profilepress.net/downloads/tag/perfecto/" target="_blank">Perfecto
                            form</a>: <?php _e('a clean & polished login, registration and password reset form.', 'profilepress'); ?>
                    </li>
                </ul>
                <div><a href="https://profilepress.net/themes" target="_blank">
                        <button class="button-primary" type="button"><?php _e('Shop Now', 'profilepress'); ?></button>
                    </a></div>
            </div>
        </div>

        <div class="postbox" style="text-align: center">
            <div class="handlediv"><br></div>
            <h3 class="hndle ui-sortable-handle"><span>OmniPay WordPress</span></h3>

            <div class="inside">
                <p>WordPress payment extension for <strong>Easy Digital Downloads</strong> and
                    <strong>WooCommerce</strong> that bundles several payment gateways together with an intuitive
                    drag-and-drop interface for the gateways set up and management.</p>
                <div style="margin:10px">
                    <a href="https://omnipay.io/?utm_source=profilepress-dashboard" target="_blank">
                        <button class="button-primary" type="button">Get it Now</button>
                    </a></div>
                <div style="margin:10px">
                    <a href="https://omnipay.io/edd-payment-gateways/?ref=admin-bar-dashboard" target="_blank">
                        <button class="button-primary" type="button">EDD Payment Gateways</button>
                    </a></div>
                <div style="margin:10px">
                    <a href="https://omnipay.io/woocommerce-payment-gateways/?ref=admin-bar-dashboard" target="_blank">
                        <button class="button-primary" type="button">WooCommerce Payment Gateway</button>
                    </a></div>
            </div>
        </div>
    </div>
</div>