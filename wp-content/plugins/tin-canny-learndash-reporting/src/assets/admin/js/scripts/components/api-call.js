var uoReportingAPI = {

    reportingApiCall: function (type, data) {

        if(typeof data == 'undefined'){
            data = '';
        }else{
            data = '/'+data
        }

        return jQuery.ajax({
            url: reportingApiSetup.root + type + data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', reportingApiSetup.nonce);
            }
        });

    },

    reportingApiCallData: function (type, data) {

        // Todo Deprecated function remove and change an instance to above

        return jQuery.ajax({
            url: reportingApiSetup.root + type + '/'+data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', reportingApiSetup.nonce);
            }
        });

    }





};