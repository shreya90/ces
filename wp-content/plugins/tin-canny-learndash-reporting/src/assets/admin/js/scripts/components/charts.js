var chartVars = {

    courseOverviewGraphTable: [],

    setCourseOverviewGraphTable: function (table, sortOn) {

        if (typeof sortOn == 'undefined') {
            sortOn = 1;
        }

        // Todo Fix Sorting
        this.courseOverviewGraphTable = table.sort(function (a, b) {
            return (a[sortOn] > b[sortOn]) ? 1 : ((b[sortOn] > a[sortOn]) ? -1 : 0);
        });

        // No sorting
        //this.courseOverviewGraphTable = table;
    },

    courseOverviewGraph: function () {

        //TODO What if there is only one or two courses
        var courseSummary = [];

        courseSummary[0] = {};
        courseSummary[0].name = this.courseOverviewGraphTable[0][1];
        courseSummary[0].notStarted = this.courseOverviewGraphTable[0][3];
        courseSummary[0].inProgress = this.courseOverviewGraphTable[0][4];
        courseSummary[0].completed = this.courseOverviewGraphTable[0][5];

        courseSummary[1] = {};
        courseSummary[1].name = this.courseOverviewGraphTable[1][1];
        courseSummary[1].notStarted = this.courseOverviewGraphTable[1][3];
        courseSummary[1].inProgress = this.courseOverviewGraphTable[1][4];
        courseSummary[1].completed = this.courseOverviewGraphTable[1][5];

        courseSummary[2] = {};
        courseSummary[2].name = this.courseOverviewGraphTable[2][1];
        courseSummary[2].notStarted = this.courseOverviewGraphTable[2][3];
        courseSummary[2].inProgress = this.courseOverviewGraphTable[2][4];
        courseSummary[2].completed = this.courseOverviewGraphTable[2][5];

        return {
            "type": "serial",
            "categoryField": "category",
            "rotate": true,
            "plotAreaFillAlphas": 1,
            "plotAreaFillColors": "#FFFFFF",
            "startDuration": 1,
            "theme": "light",
            "categoryAxis": {
                "gridPosition": "start",
                "fontSize": 15,
                "labelOffset": 6,
                "axisColor": "#C4C3C3",
                "axisThickness": 3,
                "tickLength": 0,
                "titleFontSize": 14
            },
            "graphs": [
                {
                    "balloonText": "",
                    "fillAlphas": 1,
                    "fillColors": "#f1f1f1",
                    "gapPeriod": 2,
                    "id": "AmGraph-1",
                    "labelText": "[[value]]",
                    "lineColor": "#E1E1E1",
                    "tabIndex": 1,
                    "title": "Not Started",
                    "type": "column",
                    "valueField": "notStarted"
                },
                {
                    "balloonText": "",
                    "color": "#FFFFFF",
                    "fillAlphas": 1,
                    "fillColors": "#7bc47b",
                    "gapPeriod": 2,
                    "id": "AmGraph-2",
                    "labelText": "[[value]]",
                    "lineColor": "#E1E1E1",
                    "tabIndex": 1,
                    "title": "In Progress",
                    "type": "column",
                    "valueField": "inProgress"
                },
                {
                    "balloonText": "",
                    "color": "#FFFFFF",
                    "fillAlphas": 1,
                    "fillColors": "#29779e",
                    "gapPeriod": 2,
                    "id": "AmGraph-3",
                    "labelText": "[[value]]",
                    "lineColor": "#E1E1E1",
                    "tabIndex": 1,
                    "title": "Completed",
                    "type": "column",
                    "valueField": "completed"
                }
            ],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "axisColor": "#C4C3C3",
                    "axisThickness": 3,
                    "stackType": "regular",
                    "title": ""
                }
            ],
            "legend": {
                "enabled": true,
                "useGraphSettings": true,
                "position": "right"
            },
            "titles": [
                {
                    "id": "Title-1",
                    "text": ""
                }
            ],
            "dataProvider": [
                {
                    "category": courseSummary[0].name,
                    "notStarted": courseSummary[0].notStarted,
                    "inProgress": courseSummary[0].inProgress,
                    "completed": courseSummary[0].completed
                },
                {
                    "category": courseSummary[1].name,
                    "notStarted": courseSummary[1].notStarted,
                    "inProgress": courseSummary[1].inProgress,
                    "completed": courseSummary[1].completed
                },
                {
                    "category": courseSummary[2].name,
                    "notStarted": courseSummary[2].notStarted,
                    "inProgress": courseSummary[2].inProgress,
                    "completed": courseSummary[2].completed
                }
            ]
        };

    },

    courseSingleOverviewPieChartData: function (courseID) {

        var completed = dataObject.courseList[courseID].completed,
            inProgress = dataObject.courseList[courseID].inProgress,
            notStarted = dataObject.courseList[courseID].notStarted;

        return {
            "type": "pie",
            "balloonText": "",
            "innerRadius": 0,
            "labelRadius": 15,
            "labelText": "[[title]] [[percents]]%",
            "startAngle": 43.2,
            "baseColor": "",
            "colors": [
                "#29779e",
                "#f1f1f1",
                "#7bc47b"
            ],
            "labelTickAlpha": 0.56,
            "marginBottom": 0,
            "marginTop": 0,
            "outlineAlpha": 1,
            "outlineColor": "#E1E1E1",
            "outlineThickness": 2,
            "sequencedAnimation": false,
            "startDuration": 0,
            "titleField": "category",
            "valueField": "column-1",
            "backgroundAlpha": 1,
            "fontSize": 14,
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider": [
                {
                    "category": "Completed",
                    "column-1": completed
                },
                {
                    "category": "Not Started",
                    "column-1": notStarted
                },
                {
                    "category": "In Progress",
                    "column-1": inProgress
                }
            ]
        };
    },

    courseSingleCompletionChartData: function ( courseID ) {

        var completionDates = [];
        var completedOn;
        var amountUsers = {};

        var temp = {};

        jQuery.each( dataObject.userList, function( userID, userData){

            if( typeof userData !== 'undefined' && typeof userData[courseID] !== 'undefined' ){
                var usersCourseData  = userData[courseID];

                if( typeof usersCourseData['completed_on'] !== 'undefined' || userID == 1){

                    completedOn = usersCourseData['completed_on'];

                    if( typeof temp[completedOn] == 'undefined') {
                        temp[completedOn] = 1
                    }else{
                        temp[completedOn]++;
                    }

                }
            }

        });

        jQuery.each( temp, function( completedOn, amountUsers){
            completionDates.push({"date": completedOn, "amountUsers": amountUsers } );
        });

        completionDates.sort(function(a,b){
            // Turn your strings into dates, and then subtract them
            // to get a value that is either negative, positive, or zero.
            return (new Date(b.date) - new Date(a.date))*-1;
        });

        var title = [];
        var minimum = 0;
        if(0 === completionDates.length){
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            if(dd<10) {
                dd='0'+dd
            }

            if(mm<10) {
                mm='0'+mm
            }

            today = yyyy+'-'+mm+'-'+dd;

            completionDates = [{"date":today, "amountUser":0}];

            title = [{
                "id": "Title-1",
                "size": 15,
                "text": "No Data"
            }];

            minimum = 2;
        }

        return {
            "type": "serial",
            "pathToImages": "http://cdn.amcharts.com/lib/3/images/",
            "categoryField": "date",
            "dataDateFormat": "YYYY-MM-DD",
            "marginLeft": 15,
            "plotAreaBorderColor": "#B53A3A",
            "plotAreaFillAlphas": 1,
            "colors": [
                "#29779e"
            ],
            "backgroundAlpha": 0,
            "theme": "default",
            "categoryAxis": {
                "parseDates": true,
                "axisColor": "#C4C3C3",
                "axisThickness": 3,
                "tickLength": 0
            },
            "chartCursor": {
                "enabled": true,
                "categoryBalloonDateFormat": "MMM DD, YY"
            },
            "chartScrollbar": {
                "enabled": true
            },
            "trendLines": [],
            "graphs": [
                {
                    "balloonColor": "#F1F1F1",
                    "bullet": "round",
                    "bulletBorderThickness": 0,
                    "gapPeriod": 1,
                    "id": "AmGraph-1",
                    "legendAlpha": 0,
                    "legendColor": "",
                    "lineColor": "#29779E",
                    "lineThickness": 4,
                    "title": "graph 1",
                    "valueField": "amountUsers"
                }
            ],
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "axisColor": "#C4C3C3",
                    "axisThickness": 3,
                    "firstDayOfWeek": 0,
                    "tickLength": 0,
                    "title": "",
                    "titleBold": false,
                    "minimum": minimum,
                    "precision": 0
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": title,
            "dataProvider": completionDates
        }
    }


};