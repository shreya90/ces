var dataObject = {

    dataObjectPopulated : false,
    courseList: {},
    lessonList: {},
    topicList: {},
    quizList: {},
    assignmentList: {},
    userList: {},
    groupsList: {},
    dataTables: {},
    learnDashLabels: {},
    links: {},

    getData : function(tab){

        if(typeof tab === 'undefined'){
            tab = '';
        }

        if( !reportingQueryString.isTinCanOnly ) {

            dataObject.dataObjectPopulated = true;
            // Load UI from wp dashboard
           //tinCannyDashboard.init();

            // Api Call wrapper get course overview ( User List with data and Course List with data )
            if( typeof reportingApiSetup.isolated_group_id === 'string' && parseInt(reportingApiSetup.isolated_group_id) ){
                var coursesOverviewApiCall = uoReportingAPI.reportingApiCall('courses_overview','/?group_id='+reportingApiSetup.isolated_group_id);
            }else{
                var coursesOverviewApiCall = uoReportingAPI.reportingApiCall('courses_overview');
            }



            coursesOverviewApiCall.error(function (response) {

                var failedResponseEl = document.getElementById('failed-response');
                failedResponseEl.innerHTML = response.responseText;

                jQuery('#reporting-loader').hide('slow', function(){
                    failedResponseEl.style.display = "block";
                });

            });

            coursesOverviewApiCall.done(function (response) {


                var failedResponseEl = document.getElementById('failed-response');

                // Handle unsuccessful data retrieval
                if( ! response.success){

                    failedResponseEl.innerHTML = response.message;

                    jQuery('#reporting-loader').hide('slow', function(){
                        failedResponseEl.style.display = "block";
                    });

                    return;
                }

                // handle unsuccessful course list retrieval
                if( typeof response.data.userList.success !== 'undefined' && !response.data.userList.success){

                    failedResponseEl.innerHTML = response.data.userList.message;

                    jQuery('#reporting-loader').hide('slow', function(){
                        failedResponseEl.style.display = "block";
                    });

                    return;
                }
                // Load initial data into data object
                dataObject.setCourseList(response.data.courseList);
                dataObject.setUserList(response.data.userList);
                dataObject.setGroupsList(response.data.groupsList);
                dataObject.setLearnDashLabels(response.learnDashLabels);
                dataObject.setLinks(response.links);

                reportingTabs.triggerTabGroup(tab);

                // Hide loader and show page
                jQuery('#reporting-loader').hide('slow', function(){

                    jQuery('.uo-admin-reporting-tabs').slideDown('fast', function(){

                        jQuery('.uo-admin-reporting-tabgroup').slideDown('slow')

                    })
                });

                // Load more data in the backgrounded
                if( typeof reportingApiSetup.isolated_group_id === 'string' && parseInt(reportingApiSetup.isolated_group_id) ){
                    var usersCompletedCoursesApiCall = uoReportingAPI.reportingApiCall('users_completed_courses','/?group_id='+reportingApiSetup.isolated_group_id);
                }else{
                    var usersCompletedCoursesApiCall = uoReportingAPI.reportingApiCall('users_completed_courses');
                }


                usersCompletedCoursesApiCall.done(function (response) {

                    jQuery.each(response['data'], function(userID, arrayUserCourseCompletions ){

                        jQuery.each( arrayUserCourseCompletions, function( index, arrayCourseIdTimeStamp){
                            var courseID = arrayCourseIdTimeStamp[0];
                            var name = 'completed_on';
                            var data = arrayCourseIdTimeStamp[1];
                            var type = 'string';
                            dataObject.addToUserList( userID, name, data, type, courseID );
                        });
                    });

                });

                var courseModulesApiCall = uoReportingAPI.reportingApiCall('course_modules');
                courseModulesApiCall.done(function (response) {

                    jQuery.each(response, function(moduleType, moduleList ){
                        dataObject[moduleType] = moduleList;
                    });

                });

                var assignmentDataApiCall = uoReportingAPI.reportingApiCall('assignment_data');
                assignmentDataApiCall.done(function (response) {

                    dataObject.assignmentList = response.assignmentList;

                    jQuery.each(response.userAssignmentData, function(userID, assignments){

                        jQuery.each(assignments, function(assignmentID, assignmentData){

                            dataObject.addToUserList(userID, 'approval_status', assignmentData.approval_status, 'number', assignmentID);
                            dataObject.addToUserList(userID, 'completed_on', assignmentData.completed_on, 'string', assignmentID);
                        });

                    });

                });

            });

        }


    },


    setCourseList: function (courseList) {
        this.courseList = courseList;
    },

    getCourseList: function () {
        return this.courseList;
    },

    addToCourseList: function (courseID, name, data, type) {

        switch (type) {
            case 'string':
                data = String(data);
                break;
            default:
                data = Number(data);
                break;
        }


        if (this.courseList[courseID]) {
            this.courseList[courseID][name] = data;
        }

    },

    setUserList: function (userList) {

        var allUserIds = [];
        var userListObject = [];
        jQuery.each(userList, function (index, value) {
            if( typeof userList[index].ID !== 'undefined'){
                allUserIds.push(index);
                userListObject[index] = value
            }
        });

        this.userList = userListObject;
        this.userList.allUserIds = allUserIds;

    },

    getUserList: function () {
        return this.userList;
    },

    setGroupsList: function (groupsList) {

        this.groupsList = groupsList;
        var vThis = this;

        jQuery.each(groupsList, function (postId, data) {
            if (typeof vThis.courseList[postId] === 'object') {
                jQuery.each(data, function (index, groupID) {

                    if(typeof groupsList[groupID] === 'undefined'){
                        return true;
                    }

                    var userIDsAccess = groupsList[groupID].groups_user;

                    if (typeof vThis.courseList[postId] === 'undefined') {
                        return true;
                    }

                    if (vThis.courseList[postId].course_price_type !== 'open') {
                        vThis.courseList[postId].course_user_access_list = vThis.uniqueArray(vThis.courseList[postId].course_user_access_list.concat(userIDsAccess));
                    }

                });
            }
        });


    },

    getGroupsList: function () {
        return this.groupsList;
    },

    addToUserList: function (userID, name, data, type, courseID) {

        switch (type) {
            case 'string':
                data = String(data);
                break;
            case 'number':
                data = Number(data);
                break;
        }

        if (this.userList[userID]) {
            if (typeof courseID === 'undefined') {
                this.userList[userID][name] = data;
            } else {
                if (typeof this.userList[userID][courseID] === 'undefined') {
                    this.userList[userID][courseID] = {};
                }
                this.userList[userID][courseID][name] = data;
            }
        }


    },

    setLearnDashLabels: function (learnDashLabels) {
        this.learnDashLabels = learnDashLabels;
    },

    getLearnDashLabels: function (label) {
        return this.learnDashLabels(label);
    },

    setLinks: function (links) {
        this.links = links;
    },

    getLinks: function (links) {
        return this.learnDashLabels(links);
    },

    addToDataTables: function (tableName, tableData) {
        this.dataTables[tableName] = tableData;
    },

    getDataTable: function (tableName) {
        return this.dataTables[tableName];
    },

    tableEmptyCellPlaceholder: function () {
        return '';
    },

    addToTinCanStatements: function(userID, courseStatements){

        if(typeof this.userList[userID] !== 'undefined'){
            this.userList[userID].tinCanStatements = courseStatements;
        }
        
    },

    singleUserCoursePercentComplete: function (userID, courseID) {

        var percentComplete = this.tableEmptyCellPlaceholder();

        if (typeof this.userList[userID][courseID] !== 'undefined' && typeof this.userList[userID][courseID].completed !== 'undefined') {
            percentComplete = Math.ceil(this.userList[userID][courseID].completed / this.userList[userID][courseID].total * 100)+'%';
        }

        return percentComplete;

    },

    singleUserCourseStatus: function (userID, courseID) {

        var percentComplete = this.singleUserCoursePercentComplete(userID, courseID);

        percentComplete = parseInt(percentComplete);

        var status = 'Not started';

        if (percentComplete < 100 && percentComplete != 0) {
            status = 'In Progress';
        }

        if (percentComplete == 100) {
            status = 'Completed';
        }

        return status;

    },

    singleUserCourseCompletionTime: function (userID, courseID) {

        var timeSpent = this.tableEmptyCellPlaceholder();

        if (typeof this.userList[userID][courseID] !== 'undefined' && typeof this.userList[userID][courseID].completed_time !== 'undefined') {
            timeSpent = this.userList[userID][courseID].completed_time;
            timeSpent = this.formatSecondsToDate(timeSpent);
        }

        return timeSpent;

    },

    singleUserCourseTimeSpent: function (userID, courseID) {

        var timeSpent = this.tableEmptyCellPlaceholder();

        if (typeof this.userList[userID][courseID] !== 'undefined' && typeof this.userList[userID][courseID].time_spent !== 'undefined') {
            timeSpent = this.userList[userID][courseID].time_spent;
            timeSpent = this.formatSecondsToDate(timeSpent);
        }

        return timeSpent;

    },

    singleUserCourseCompletionDate: function (userID, courseID) {

        var completionDate = this.tableEmptyCellPlaceholder();

        if (typeof this.userList[userID][courseID] !== 'undefined' && typeof this.userList[userID][courseID].completed_on !== 'undefined') {
            completionDate = this.userList[userID][courseID].completed_on;
        }

        return completionDate;

    },

    singleUserCourseAvgQuizScore: function (userID, courseID) {


        var avgQuizScore = this.tableEmptyCellPlaceholder();

        if( typeof this.courseList[courseID].associatedPosts === 'undefined'){
            return avgQuizScore;
        }

        var courseAssociatedPosts = this.courseList[courseID].associatedPosts;

        var cumulativePercentage = 0;

        var amountQuizzes = 0;

        // Loop through all modules associated with current course
        jQuery.each(courseAssociatedPosts, function (index, courseAssociatedPost) {

                // Make sure the user has data for the current module before access module data
                if( typeof dataObject.userList[userID]['quizzes'] !== 'undefined' &&
                    typeof dataObject.userList[userID]['quizzes'][0][courseAssociatedPost] !== 'undefined'
                ){

                    // Make sure the current module is a quiz
                    if(typeof dataObject.userList[userID]['quizzes'][0][courseAssociatedPost].type !== 'undefined' &&
                        dataObject.userList[userID]['quizzes'][0][courseAssociatedPost].type === 'quiz'
                    ){

                        // Make sure there is quiz data
                        if( 1 <= dataObject.userList[userID]['quizzes'][0][courseAssociatedPost].attempts ){

                            // Choose the last(most recent quiz attempt); the attempts are indexed and there is one type object
                            var lastAttempt = dataObject.userList[userID]['quizzes'][0][courseAssociatedPost].attempts - 1;

                            cumulativePercentage+= parseInt( dataObject.userList[userID]['quizzes'][0][courseAssociatedPost][lastAttempt].percentage );
                            amountQuizzes++;

                        }
                    }
                }
        });

        if (0 !== amountQuizzes || 0 !== cumulativePercentage) {
            avgQuizScore = Math.ceil(cumulativePercentage / amountQuizzes)+'%';
        }

        return avgQuizScore;
    },

    singleUserModuleAvgQuizScore: function () {

    },

    formatSecondsToDate: function (timeInSeconds) {

        if(isNaN(timeInSeconds)){

            return '';
        }
        
        var sec_num = timeInSeconds;
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = Math.floor(sec_num - (hours * 3600) - (minutes * 60));

        if (hours < 10) {
            hours = "0" + hours;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }

        seconds = Math.ceil(seconds);
        if (seconds < 10) {
            seconds = "0" + seconds;
        }

        return hours + ':' + minutes + ':' + seconds;
    },

    uniqueArray: function (array) {
        var a = array.concat();
        for (var i = 0; i < a.length; ++i) {
            for (var j = i + 1; j < a.length; ++j) {
                if (a[i] == a[j])
                    a.splice(j--, 1);
            }
        }

        return a;
    },

    objectLength : function( obj ){
            var result = 0;
            for(var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    // or Object.prototype.hasOwnProperty.call(obj, prop)
                    result++;
                }
            }
            return result;
    }

};