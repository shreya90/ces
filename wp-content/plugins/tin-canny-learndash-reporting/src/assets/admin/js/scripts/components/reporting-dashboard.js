var tinCannyDashboard = null;

jQuery(document).ready(function ($) {

    tinCannyDashboard = {

        init: function () {

            var _class = this;

            var welcomePanel = jQuery('#uo-welcome-panel').parent();
            jQuery(_class.html).insertAfter(welcomePanel);
            jQuery(_class.html).insertAfter('#coursesOverviewGraphHeading');
            jQuery('.uo-reporting-dashboard-container').children().hide();
            jQuery('#reporting-loader-dashboard').show();
            jQuery('#reporting-loader-message').show();

            // Hide Dashboard View Report Buttons if we are already on the reporting page
            if (typeof reportingQueryString !== 'undefined' && reportingQueryString.vars.hasOwnProperty('page') && 'uncanny-learnDash-reporting' === reportingQueryString.vars.page) {
                jQuery('.uo-dashboard-button').css('visibility', 'hidden');

            }

            // Hide Dashboard View Report Buttons if we are on the frontend
            if (typeof reportingQueryString !== 'undefined' && reportingQueryString.vars.hasOwnProperty('')) {
                jQuery('.uo-dashboard-button').css('visibility', 'hidden');

            }

            // Add toggle more most and least completed courses
            var completedCoursesSelect = $('.reporting-dashboard-select');

            completedCoursesSelect.on('change', function () {
                // Sort the list
                _class.showMostLeast($(this).val());

            });


            // Api Call wrapper get course overview ( User List with data and Course List with data )
            if (typeof reportingApiSetup.isolated_group_id === 'string' && parseInt(reportingApiSetup.isolated_group_id)) {

                var coursesOverviewApiCall = this.reportingApiCall('dashboard_data', '/?group_id=' + reportingApiSetup.isolated_group_id);
            } else {

                var coursesOverviewApiCall = this.reportingApiCall('dashboard_data');
            }


            coursesOverviewApiCall.error(function (response) {

                var error = document.createElement('div');
                error.id = 'api-error';
                error.innerHTML = response.responseText;
                document.getElementById('wpbody-content').appendChild(error);


            });

            coursesOverviewApiCall.done(function (response) {

                _class.dataObject = response;

                $('.uo-reporting-dashboard-container').children().show();
                $('#reporting-loader-dashboard').hide();
                $('#reporting-loader-message').hide();
                jQuery('#total-courses').text(_class.dataObject.total_courses);
                jQuery('#total-users').text(_class.dataObject.total_users);

                $('#navigate-to-course-report').on('click', function () {
                    window.location.href = _class.dataObject.report_link;
                });

                $('#navigate-to-user-report').on('click', function () {
                    window.location.href = _class.dataObject.report_link + '&tab=usersReportTab';
                });


                // TODO Change to hide select if less than 3 courses
                //_class.dataObject.top_course_completions.splice( 2, 5);
                var countCourseCompletion = _class.dataObject.top_course_completions.length;

                // Hide most/least toggle when there is less than three graphs
                if (countCourseCompletion < 4) {

                    //$('#dashboard-least-courses-complete').hide().prev().hide();

                }

                AmCharts.makeChart('recentActivitesChart', _class.courseSingleCompletionChartData());

                var courseName,
                    courseCompletionPercentage;

                courseName = _class.top3CourseName({order: 0, index: 0});
                courseCompletionPercentage = _class.top3CompletetionPercentage(0);
                if( false !== courseName && false !== !courseCompletionPercentage){
                    AmCharts.makeChart('topOne', _class.topOne( courseName, courseCompletionPercentage) );
                }

                courseName = _class.top3CourseName({order: 1, index: 1});
                courseCompletionPercentage = _class.top3CompletetionPercentage(1);
                if( false !== courseName && false !== !courseCompletionPercentage) {
                    AmCharts.makeChart('topTwo', _class.topOne(courseName, courseCompletionPercentage));
                }

                courseName = _class.top3CourseName({order: 2, index: 2});
                courseCompletionPercentage = _class.top3CompletetionPercentage(0);
                if( false !== courseName && false !== !courseCompletionPercentage) {
                    AmCharts.makeChart('topThree', _class.topOne(courseName, courseCompletionPercentage));
                }
            });

        },

        dataObject: [],

        top3CourseName: function (top) {

            if (typeof this.dataObject['top_course_completions'][top.index] === 'undefined') {
                return false;
            }

            if (typeof this.dataObject['top_course_completions'][top.index].completions === 'undefined') {
                return false;
            }

            var completions = this.dataObject['top_course_completions'][top.index].completions;

            var userCount = this.dataObject['top_course_completions'][top.index].course_user_access_list.length;
            if (undefined === userCount) {
                userCount = this.size(this.dataObject['top_course_completions'][top.index].course_user_access_list);
            }

            if (this.dataObject['top_course_completions'][top.index].course_price_type == 'open') {
                userCount = this.dataObject.total_users;
            }

            var id = '';

            if (0 === top.order) {
                id = '#topOne';
            } else if (1 === top.order) {
                id = '#topTwo';
            } else if (2 === top.order) {
                id = '#topThree';
            }

            var postTitle = this.dataObject['top_course_completions'][top.index].post_title;

            jQuery(id).siblings('.performance-metric-name').children().remove();
            jQuery(id).siblings('.performance-metric-name').html('<i>' + postTitle + '<br>' + completions + ' of ' + userCount + ' Completed </i>');
            return postTitle;
        },
        top3CompletetionPercentage: function (index) {

            if (typeof this.dataObject['top_course_completions'][index] === 'undefined') {
                return false;
            }
            if (typeof this.dataObject['top_course_completions'][index].completions === 'undefined') {
                return false;
            }

            var completions = this.dataObject['top_course_completions'][index].completions;
            var userCount = this.dataObject['top_course_completions'][index].course_user_access_list.length;
            if (undefined === userCount) {
                userCount = this.size(this.dataObject['top_course_completions'][index].course_user_access_list);
            }
            if (this.dataObject['top_course_completions'][index].course_price_type == 'open') {
                userCount = this.dataObject.total_users;
            }

            if (0 === userCount) {
                return 0;
            }

            return Math.ceil(completions / userCount * 100);
        },

        reportingApiCall: function (type, data) {

            if (typeof data == 'undefined') {
                data = '';
            } else {
                data = '/' + data
            }

            return jQuery.ajax({
                url: reportingApiSetup.root + type + data,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-WP-Nonce', reportingApiSetup.nonce);
                }
            });

        },

        html: '<div class="uo-reporting-dashboard-container">' +
        '<div id="reporting-loader-dashboard" class="sk-folding-cube"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div>' +
        '<h3 id="reporting-loader-message" style="text-align: center;">Loading Dashboard Report</h3>' +
        '<div class="reporting-dashboard-col-container reporting-dashboard-col-1">' +
        '<div class="reporting-dashboard-col-inner-container">' +
        '<h3 class="reporting-dashboard-col-heading">Total ' + reportingApiSetup.learnDashLabels.courses + '</h3>' +
        '<div id="total-courses">--</div>' +
        '<button id="navigate-to-course-report" class="uo-dashboard-button" data-target="">View Report</button>' +
        '</div>' +
        '<div class="reporting-dashboard-col-inner-container">' +
        '<h3 class="reporting-dashboard-col-heading">Total Users</h3>' +
        '<div id="total-users">--</div>' +
        '<button id="navigate-to-user-report" class="uo-dashboard-button" data-target-last="">View Report</button>' +
        '</div>' +
        '</div>' +
        '<div class="reporting-dashboard-col-container reporting-dashboard-col-2">' +
        '<div class="reporting-dashboard-col-inner-container">' +
        '<h3 class="reporting-dashboard-col-heading">Recent Activities</h3>' +
        '<div id="recentActivitesChart"></div>' +
        '</div>' +
        '</div>' +
        '<div class="reporting-dashboard-col-container reporting-dashboard-col-3">' +
        '<div class="reporting-dashboard-col-inner-container">' +
        '<h3 class="reporting-dashboard-col-heading">' +
        '<select class="reporting-dashboard-select">' +
        '<option value="most" selected="selected">Most Completed ' + reportingApiSetup.learnDashLabels.courses + '</option><option value="least">Least Completed ' + reportingApiSetup.learnDashLabels.courses + '</option>' +
        '</select>' +
        '</h3>' +
        '<div class="performance-metric-row">' +
        '<div class="performance-metric" id="topOne"></div>' +
        '<div class="performance-metric-name"></div></div>' +
        '<div class="performance-metric-row">' +
        '<div class="performance-metric" id="topTwo"></div>' +
        '<div class="performance-metric-name"></div></div>' +
        '<div class="performance-metric-row">' +
        '<div class="performance-metric" id="topThree"></div>' +
        '<div class="performance-metric-name"></div></div>' +
        '<div class="clear"></div>' +
        '</div>' +
        '</div>' +
        '</div>',

        courseSingleCompletionChartData: function () {

            var completionAndTinCanDates = this.dataObject.courses_tincan_completed;

            var chart = {
                "type": "serial",
                "pathToImages": "https://cdn.amcharts.com/lib/3/images/",
                "categoryField": "date",
                "dataDateFormat": "YYYY-MM-DD",
                "marginLeft": 15,
                "plotAreaBorderColor": "#B53A3A",
                "plotAreaFillAlphas": 1,
                "colors": [
                    "#29779e"
                ],
                "backgroundAlpha": 0,
                "theme": "default",
                "categoryAxis": {
                    "parseDates": true,
                    "axisColor": "#C4C3C3",
                    "axisThickness": 3,
                    "tickLength": 0
                },
                "chartCursor": {
                    "enabled": true,
                    "categoryBalloonDateFormat": "MMM DD, YY"
                },
                "chartScrollbar": {
                    "enabled": true
                },
                "trendLines": [],
                "graphs": [
                    {
                        "valueAxis": "ValueAxis-1",
                        "balloonColor": "#F1F1F1",
                        "bullet": "round",
                        "bulletBorderThickness": 0,
                        "gapPeriod": 1,
                        "id": "AmGraph-1",
                        "legendAlpha": 0,
                        "legendColor": "",
                        "lineColor": "#29779E",
                        "lineThickness": 4,
                        "title": "Completion",
                        "valueField": "completions"
                    }
                ],
                "guides": [],
                "valueAxes": [
                    {
                        "id": "ValueAxis-1",
                        "axisColor": "#C4C3C3",
                        "axisThickness": 3,
                        "firstDayOfWeek": 0,
                        "tickLength": 0,
                        "title": "",
                        "titleBold": false,
                        "minimum": 0,
                        "strictMinMax": true,
                        "precision": 0,
                        "valueField": "completions",
                        "position": "left",
                        "labelFunction": this.formatLabel
                    }
                ],
                "allLabels": [],
                "balloon": {},
                "legend": {
                    "enabled": true,
                    "useGraphSettings": true
                },
                "titles": [],
                "dataProvider": completionAndTinCanDates,
                "synchronizeGrid": true
            };


            if (jQuery('li#menuTinCan').css('display') !== 'none') {
                chart.graphs = [
                    {
                        "valueAxis": "ValueAxis-1",
                        "balloonColor": "#F1F1F1",
                        "bullet": "round",
                        "bulletBorderThickness": 0,
                        "gapPeriod": 1,
                        "id": "AmGraph-1",
                        "legendAlpha": 0,
                        "legendColor": "",
                        "lineColor": "#29779E",
                        "lineThickness": 4,
                        "title": "Completion",
                        "valueField": "completions"
                    },
                    {
                        "valueAxis": "ValueAxis-2",
                        "balloonColor": "#F1F1F1",
                        "bullet": "round",
                        "bulletBorderThickness": 0,
                        "gapPeriod": 1,
                        "id": "AmGraph-2",
                        "legendAlpha": 0,
                        "legendColor": "",
                        "lineColor": "#7bc47b",
                        "lineThickness": 4,
                        "title": "Tin Can",
                        "valueField": "tinCan"
                    }
                ];
                chart.valueAxes = [
                    {
                        "id": "ValueAxis-1",
                        "axisColor": "#C4C3C3",
                        "axisThickness": 3,
                        "firstDayOfWeek": 0,
                        "tickLength": 0,
                        "title": "",
                        "titleBold": false,
                        "minimum": 0,
                        "precision": 0,
                        "valueField": "completions",
                        "position": "left",
                        "labelFunction": this.formatLabel
                    },
                    {
                        "id": "ValueAxis-2",
                        "axisColor": "#C4C3C3",
                        "axisThickness": 3,
                        "firstDayOfWeek": 0,
                        "tickLength": 0,
                        "title": "",
                        "titleBold": false,
                        "minimum": 0,
                        "precision": 0,
                        "valueField": "tinCan",
                        "position": "right",
                        "labelFunction": this.formatLabel
                    }
                ];
            }

            return chart;

        },

        formatLabel: function (value, formattedValue, valueAxis) {

            // hide axis values less than 0
            if (value < 0) {
                value = '';
            }

            return value;
        },

        topOne: function (courseName, percetage) {
            var perctangeText = percetage + '%';
            var percentCompleted = percetage;
            var percentNotCompleted = 100 - percetage;
            return {
                "type": "pie",
                "balloonText": "",
                "marginBottom": 10,
                "marginTop": 5,
                "startRadius": "10%",
                "pullOutRadius": "0%",
                "innerRadius": "70%",
                "labelText": "",
                "colorField": "column-2",
                "labelsEnabled": false,
                "valueField": "column-1",
                "processTimeout": -3,
                "allLabels": [
                    {
                        "align": "center",
                        "bold": false,
                        "id": "Label-1",
                        "size": 16,
                        "text": perctangeText,
                        "x": "0%",
                        "y": "35%"
                    },
                    {
                        "align": "center",
                        "color": "",
                        "id": "Label-2",
                        "size": 14,
                        "text": "",
                        "x": "0%",
                        "y": "78%"
                    }
                ],
                "balloon": {},
                "titles": [],
                "dataProvider": [
                    {
                        "category": "data",
                        "column-1": percentCompleted,
                        "column-2": "#29779e"
                    },
                    {
                        "category": "rest",
                        "column-1": percentNotCompleted,
                        "column-2": "#dcdcdc"
                    }
                ]
            };
        },

        showMostLeast: function (sortOn) {

            var _class = this;

            var order = {};

            if ('most' == sortOn) {

                order.top = {order: 0, index: 0};
                order.middle = {order: 1, index: 1};
                order.bottom = {order: 2, index: 2};

            } else if ('least' == sortOn) {

                var countCourseCompletion = this.dataObject.top_course_completions.length;

                order.top = {order: 0, index: countCourseCompletion - 3};
                order.middle = {order: 1, index: countCourseCompletion - 2};
                order.bottom = {order: 2, index: countCourseCompletion - 1};

            } else {
                return false;
            }


            AmCharts.makeChart('topOne', _class.topOne(_class.top3CourseName(order.top), _class.top3CompletetionPercentage(order.top.index)));
            AmCharts.makeChart('topTwo', _class.topOne(_class.top3CourseName(order.middle), _class.top3CompletetionPercentage(order.middle.index)));
            AmCharts.makeChart('topThree', _class.topOne(_class.top3CourseName(order.bottom), _class.top3CompletetionPercentage(order.bottom.index)));

        },

        size: function (obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        }


    };

    tinCannyDashboard.init();

});
