var reportingTables = {

    tableColumns: null,
    cachedUserList: null,
    tableObjects: {},
    detailedTableObject: undefined,
    courseSingleOverviewSummaryTableObject: undefined,

    defaultHeadings: {

        coursesOverviewTable: ['ID', reportingApiSetup.learnDashLabels.course, 'Enrolled', 'Not Started', 'In Progress', 'Completed', 'Avg ' + reportingApiSetup.learnDashLabels.quiz + ' Score', 'Avg Time to Complete', 'Avg Time Spent', '% Complete'],//NEW AVG
        courseSingleOverviewSummaryTable: ['Users Enrolled', 'Average Time to Complete ', 'Average ' + reportingApiSetup.learnDashLabels.quiz + ' Score'],
        courseSingleTable: ['ID', 'Display Name', 'Email Address', 'Completion Date', '% Complete', 'Time to Complete', 'Time Spent'],

        usersOverviewTable: ['ID', 'Display Name', 'Email Address'],
        userSingleOverviewTable: [reportingApiSetup.learnDashLabels.courses + ' Enrolled', 'Not Started', 'In Progress', 'Completed'],
        userSingleCoursesOverviewTable: ['ID', reportingApiSetup.learnDashLabels.course, '% Complete', 'Completion Date', 'Avg ' + reportingApiSetup.learnDashLabels.quiz + ' Score', 'Time to Complete', 'Time Spent'],

        userSingleCourseProgressSummaryTable: ['Status', 'Time to Complete', '% Complete', 'Avg ' + reportingApiSetup.learnDashLabels.quiz + ' Score', 'Time Spent'],
        userSingleCourseLessonsTable: [reportingApiSetup.learnDashLabels.lesson + ' Name', 'Status'],
        userSingleCourseTopicsTable: [reportingApiSetup.learnDashLabels.topic + ' Name', 'Status'],
        userSingleCourseQuizzesTable: [reportingApiSetup.learnDashLabels.quiz + ' Name', 'Score', 'Date Completed'],
        userSingleCourseAssignmentsTable: ['Assignment Name', 'Approval', 'Submitted On'],
        userSingleCourseTinCanTable: [reportingApiSetup.learnDashLabels.lesson, 'Module', 'Target', 'Action', 'Result', 'Date'],

        customReportingTable: []
    },

    defaultEmptyTableMessage: 'There are no activities to report.',

    getTableData: function (tableType, ID) {

        var tableData = [];

        switch (tableType) {
            case 'coursesOverviewTable':
                tableData = this.coursesOverviewTableData();
                break;
            case 'courseSingleOverviewSummaryTable':
                tableData = this.courseSingleOverviewSummaryTableDataObject(ID);
                break;
            case 'courseSingleTable':
                tableData = this.courseSingleTableData(ID);
                break;
            case 'usersOverviewTable':
                tableData = this.usersOverviewTableData();
                break;
            case 'userSingleOverviewTable':
                tableData = this.userSingleOverviewTableData(ID);
                break;
            case 'userSingleCoursesOverviewTable':
                tableData = this.userSingleCoursesOverviewTableData(ID);
                break;
            case 'userSingleCourseProgressSummaryTable':
                tableData = this.userSingleCourseProgressSummaryTable(ID);
                break;
            case 'userSingleCourseLessonsTable':
                tableData = this.userSingleCourseLessonsTableData(ID);
                break;
            case 'userSingleCourseTopicsTable':
                tableData = this.userSingleCourseTopicsTableData(ID);
                break;
            case 'userSingleCourseQuizzesTable':
                tableData = this.userSingleCourseQuizzesTableData(ID);
                break;
            case 'userSingleCourseAssignmentsTable':
                tableData = this.userSingleCourseAssignmentsTableData(ID);
                break;
            case 'userSingleCourseTinCanTable':
                tableData = this.userSingleCourseTinCanTableData(ID);
                break;
        }

        return tableData;
    },

    getTableHeadings: function (tableType) {

        var headings = [];

        if (typeof this.defaultHeadings[tableType] !== 'undefined') {
            headings = this.defaultHeadings[tableType];
        }

        return this.createHeadings(headings);
    },

    createHeadings: function (headings) {
        var index = 0,
            len,
            tableColumns = [];
        for (index = 0, len = headings.length; index < len; ++index) {
            tableColumns.push({title: headings[index]});
        }
        return tableColumns;
    },

    getColumnDefs: function (tableType) {

        var columnDefs = [];

        switch (tableType) {
            case 'coursesOverviewTable':
            case 'courseSingleTable':
            case 'usersOverviewTable':
            case 'userSingleCoursesOverviewTable':
                //Hide the first ID Column
                columnDefs = [{"targets": [0], "visible": false, "searchable": false}];
                break;
        }

        return columnDefs;
    },

    getCustomizations: function (tableData, tableType) {

        switch (tableType) {
            case 'courseSingleOverviewSummaryTable':
            case 'userSingleOverviewTable':
            case 'userSingleCourseProgressSummaryTable':
                tableData["paging"] = false;
                tableData["ordering"] = false;
                tableData["info"] = false;
                tableData["bFilter"] = false;
                break;
        }

        return tableData;
    },

    createTable: function (tableType, tableElement, ID) {

        if (!tableType) {

            return false;
        }

        if (!tableElement) {

            return false;
        }

        var headings = this.getTableHeadings(tableType);
        if (0 === headings) {

            return false;
        }

        var tableData = this.getTableData(tableType, ID);

        if (0 === tableData.length) {

        }

        var columnDefs = this.getColumnDefs(tableType);

        var tableVariables = {
            data: tableData,
            columns: headings,
            columnDefs: columnDefs,
            language: {
                "emptyTable": this.defaultEmptyTableMessage
            },
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'csv',
                    text: 'CSV Export',
                    title: this.createCsvFileName(tableType, ID)
                }
            ]
        };

        tableVariables = this.getCustomizations(tableVariables, tableType);

        if (typeof this.tableObjects[tableType] !== 'undefined') {

            this.tableObjects[tableType].destroy();
        }

        this.tableObjects[tableType] = jQuery(tableElement).DataTable(tableVariables);

    },

    /*
     TABLES DATA Creation
     */
    coursesOverviewTableData: function () {

        var courseList = dataObject.getCourseList();
        var userList = dataObject.getUserList();

        // Default values
        var tableData = [];
        var userAverages = [],
            i = 0,
            courseAssociatedPosts,
            courseID,
            courseName,
            courseType;

        // Loop through all courses
        jQuery.each(courseList, function (courseId, courseData) {

            var userAccess = [],
                enrolled = 0,
                notStarted = 0,
                inProgress = 0,
                completed = 0,
                avgQuiz,
                avgCompletionTime,
                avgTimeSpent,
                completion;

            courseID = courseId;
            courseName = courseData.post_title;
            courseType = courseData.course_price_type;

            if ('' === courseName) {
                courseName = '**Course ID: ' + courseData.ID;
            }

            var courseAccessList = courseData.course_user_access_list;

            // Check enrollment
            if ('open' === courseType) {
                enrolled = dataObject.objectLength(userList);
                userAccess = dataObject.userList.allUserIds;
            } else {
                userAccess = courseAccessList;
                enrolled = dataObject.objectLength(userAccess);
            }

            // add amount user enrolled to course
            dataObject.addToCourseList(courseID, 'usersEnrolled', enrolled);

            courseAssociatedPosts = courseData.associatedPosts;
            userAverages.cumalitivePercentage = 0;
            userAverages.amountQuizzes = 0;

            // Loop through all modules associated with current course
            jQuery.each(courseAssociatedPosts, function (moduleId, courseAssociatedPost) {

                // Loop through all users that have access
                jQuery.each(userAccess, function (index, userID) {

                    if (typeof userList[userID] === 'undefined') {
                        return true;
                    }

                    // Make sure the user has data for the current module before access mudule data
                    if (typeof dataObject.userList[userID]['quizzes'] !== 'undefined' &&
                        typeof dataObject.userList[userID]['quizzes'][0][courseAssociatedPost] !== 'undefined'
                    ) {

                        // Make sure the current module is a quiz
                        if (typeof userList[userID]['quizzes'][0][courseAssociatedPost].type !== 'undefined' && userList[userID]['quizzes'][0][courseAssociatedPost].type === 'quiz') {

                            // Make sure there is quiz data
                            if (1 <= userList[userID]['quizzes'][0][courseAssociatedPost].attempts) {

                                // Choose the last(most recent quiz attempt); the attempts are indexed and there is one type object
                                var lastAttempt = userList[userID]['quizzes'][0][courseAssociatedPost].attempts - 1;

                                userAverages.cumalitivePercentage += Number(userList[userID]['quizzes'][0][courseAssociatedPost][lastAttempt].percentage);
                                userAverages.amountQuizzes++;

                            }


                        }
                    }

                });

            });

            if (0 === userAverages.amountQuizzes || 0 === userAverages.cumalitivePercentage) {
                avgQuiz = '';
            } else {
                avgQuiz = Math.ceil(userAverages.cumalitivePercentage / userAverages.amountQuizzes) + '%';
            }

            // add average quiz results to course
            dataObject.addToCourseList(courseID, 'userQuizAverage', avgQuiz, 'string');

            userAverages.cumalitiveCompletionTime = 0;
            userAverages.amountCompletionTime = 0;

            jQuery.each(userAccess, function (innerIndex, userID) {

                if (typeof userList[userID] === 'undefined') {
                    return true;
                }

                if (userList[userID][courseID] && userList[userID][courseID].completed_time) {

                    userAverages.cumalitiveCompletionTime += userList[userID][courseID].completed_time;
                    userAverages.amountCompletionTime++;

                }
            });

            if (0 === userAverages.amountCompletionTime || 0 === userAverages.cumalitiveCompletionTime) {
                avgCompletionTime = '';
            } else {
                avgCompletionTime = dataObject.formatSecondsToDate(userAverages.cumalitiveCompletionTime / userAverages.amountCompletionTime);

            }

            // add average timer results to course
            dataObject.addToCourseList(courseID, 'userCompletionTimeAverage', avgCompletionTime, 'string');

            userAverages.cumalitiveTimeSpent = 0;
            userAverages.amountTimeSpent = 0;

            jQuery.each(userAccess, function (innerIndex, userID) {

                if (typeof userList[userID] === 'undefined') {
                    return true;
                }

                if (userList[userID][courseID] && userList[userID][courseID].time_spent) {

                    userAverages.cumalitiveTimeSpent += userList[userID][courseID].time_spent;
                    userAverages.amountTimeSpent++;

                }
            });

            if (0 === userAverages.amountTimeSpent || 0 === userAverages.cumalitiveTimeSpent) {
                avgTimeSpent = '';
            } else {
                avgTimeSpent = dataObject.formatSecondsToDate(userAverages.cumalitiveTimeSpent / userAverages.amountTimeSpent);
            }

            // add average time spent results to course
            dataObject.addToCourseList(courseID, 'userTimeSpentAverage', avgTimeSpent, 'string');

            jQuery.each(userAccess, function (innerIndex, userID) {

                if (typeof userList[userID] === 'undefined') {
                    return true;
                }

                if (!(courseID in userList[userID]) || 0 == userList[userID][courseID].completed) {
                    notStarted++;
                } else if (userList[userID][courseID].completed !== userList[userID][courseID].total) {
                    inProgress++;
                } else if (userList[userID][courseID].completed === userList[userID][courseID].total) {
                    completed++;
                } else {
                    var error = {};
                    error.userID = userID;
                    error.courseID = courseID;
                    error.userList = userList;
                }

            });


            // add amount user notStarted course
            dataObject.addToCourseList(courseID, 'notStarted', notStarted);

            // add amount user inProgress course
            dataObject.addToCourseList(courseID, 'inProgress', inProgress);

            // add amount user completed course
            dataObject.addToCourseList(courseID, 'completed', completed);


            if (0 == enrolled || 0 == completed) {
                completion = 0 + '%';
            } else {
                completion = Math.floor((completed / enrolled) * 100) + '%';
            }


            // add completion percentage of course
            dataObject.addToCourseList(courseID, 'completion', completion, 'string');

            tableData[i] = [courseID, courseName, enrolled, notStarted, inProgress, completed, avgQuiz, avgCompletionTime, avgTimeSpent, completion];
            i++;
        });
        //}

        dataObject.addToDataTables('coursesOverviewTable', tableData);

        return tableData;

    },

    courseSingleTableData: function (courseID) {

        var userList = dataObject.userList;

        var index,
            len = userList.length,
            courseType,
            userAccess,
            courseSingleTableData = [],
            userDisplayName,
            userEmail,
            userCompletionDate,
            percentCompleted,
            completionTime,
            timeSpent;

        courseType = dataObject.courseList[courseID].course_price_type;

        // Check enrollment
        if ('open' === courseType) {
            userAccess = userList.allUserIds;
        } else {
            userAccess = dataObject.courseList[courseID].course_user_access_list;
        }

        jQuery.each(userAccess, function (key, userID) {

            if (typeof userList[userID] === 'undefined') {
                return true;
            }

            var userData = userList[userID];
            userDisplayName = userData.display_name;
            userEmail = userData.user_email;
            userCompletionDate = '';
            percentCompleted = 'Not Started';
            completionTime = '';
            timeSpent = '';

            if (typeof userData[courseID] !== 'undefined') {

                if (typeof userData[courseID].completed_on !== 'undefined') {
                    userCompletionDate = userData[courseID].completed_on;
                }

                if (0 !== userData[courseID].completed) {
                    percentCompleted = Math.ceil(userData[courseID].completed / userData[courseID].total * 100) + '%';
                }


                if (typeof userData[courseID].completed_time !== 'undefined') {
                    completionTime = dataObject.formatSecondsToDate(userData[courseID].completed_time);
                }

                if (typeof userData[courseID].time_spent !== 'undefined') {
                    timeSpent = dataObject.formatSecondsToDate(userData[courseID].time_spent);
                }


            }

            courseSingleTableData.push([userID, userDisplayName, userEmail, userCompletionDate, percentCompleted, completionTime, timeSpent]);

        });

        return courseSingleTableData;
    },

    courseSingleOverviewSummaryTableDataObject: function (courseID) {

        var usersEnrolled = dataObject.courseList[courseID].usersEnrolled,
            averageCompletionTime = dataObject.courseList[courseID].userCompletionTimeAverage,
            userQuizAverage = dataObject.courseList[courseID].userQuizAverage;

        return [[usersEnrolled, averageCompletionTime, userQuizAverage]];

    },

    usersOverviewTableData: function () {


        var cachedData = dataObject.getDataTable('usersOverviewTableData');

        if (typeof cachedData === 'object') {

            return cachedData;
        }

        var userDataList = dataObject.userList,
            userIdList = dataObject.userList['allUserIds'],
            userListLength = dataObject.objectLength(dataObject.userList['allUserIds']),
            tableData = new Array(userListLength),
            index = 0,
            userID;

        for (index; index < userListLength; index++) {
            userID = userIdList[index];
            tableData[index] = [userID, userDataList[userID].display_name, userDataList[userID].user_email];
        }

        dataObject.addToDataTables('usersOverviewTableData', tableData);

        return tableData;
    },

    userSingleOverviewTableData: function (userID) {

        var courseList = dataObject.getCourseList();
        var userList = dataObject.getUserList();

        var tableData = [];

        var enrolledCourseList = [];
        var coursesEnrolled = 0;
        var notStarted = 0;
        var inProgress = 0;
        var completed = 0;

        jQuery.each(courseList, function (courseID, courseData) {

            var enrolled = false;

            if (courseData.course_price_type === 'open') {
                coursesEnrolled++;
                enrolled = true;
            } else {
                jQuery.each(courseData.course_user_access_list, function (index, userAccessID) {

                    if (typeof userList[userID] === 'undefined') {
                        return true;
                    }

                    if (userAccessID == userID) {
                        coursesEnrolled++;
                        enrolled = true;
                    }
                });
            }

            if (true === enrolled) {
                enrolledCourseList.push(courseID);

                if (typeof userList[userID][courseID] !== 'undefined') {
                    if (userList[userID][courseID].completed === userList[userID][courseID].total) {
                        completed++;
                    } else if (0 === userList[userID][courseID].completed) {
                        notStarted++;
                    } else {
                        inProgress++;
                    }
                } else {
                    notStarted++;
                }

            }

        });

        dataObject.addToUserList(userID, 'enrolledCourseList', enrolledCourseList);
        tableData.push([coursesEnrolled, notStarted, inProgress, completed]);
        return tableData;

    },

    userSingleCoursesOverviewTableData: function (userID) {

        var tableData = [];

        var enrolledCourseList = dataObject.userList[userID].enrolledCourseList;

        if (typeof enrolledCourseList !== 'undefined') {

            jQuery.each(enrolledCourseList, function (index, courseID) {

                var courseName = dataObject.courseList[courseID].post_title;

                var percentComplete = dataObject.singleUserCoursePercentComplete(userID, courseID);

                var completionDate = dataObject.singleUserCourseCompletionDate(userID, courseID);

                var avgQuizScore = dataObject.singleUserCourseAvgQuizScore(userID, courseID);

                var completionTime = dataObject.singleUserCourseCompletionTime(userID, courseID);

                var timeSpent = dataObject.singleUserCourseTimeSpent(userID, courseID);


                if ('' === courseName) {
                    courseName = "*Course ID: " + courseID;
                }

                tableData.push([courseID, courseName, percentComplete, completionDate, avgQuizScore, completionTime, timeSpent]);
            });
        }

        return tableData;
    },

    userSingleCourseProgressSummaryTable: function (ID) {

        var courseID = ID;

        var userID = reportingTabs.viewingCurrentUserID;

        var percentComplete = dataObject.singleUserCoursePercentComplete(userID, courseID);

        var status = dataObject.singleUserCourseStatus(userID, courseID);

        var completionTime = dataObject.singleUserCourseCompletionTime(userID, courseID);

        var avgQuizScore = dataObject.singleUserCourseAvgQuizScore(userID, courseID);

        var timeSpent = dataObject.singleUserCourseTimeSpent(userID, courseID);

        return [[status, completionTime, percentComplete, avgQuizScore, timeSpent]];

    },

    userSingleCourseLessonsTableData: function (ID) {

        var tableData = [];

        // Get all unique associated posts ... with LD 2.5 there maybe a double course association
        if (typeof dataObject.courseList[ID].associatedPosts === 'undefined') {
            return tableData;
        }

        var associatedPosts = dataObject.courseList[ID].associatedPosts.filter(this.arrayUnique);

        if (typeof dataObject.courseList[ID].associatedPosts !== 'undefined') {
            var associatedPosts = dataObject.courseList[ID].associatedPosts.filter(this.arrayUnique);

            jQuery.each(associatedPosts, function (index, postID) {
                if (typeof dataObject.lessonList[postID] !== 'undefined') {

                    var postTitle = dataObject.lessonList[postID].post_title;
                    var viewingCurrentUserID = reportingTabs.viewingCurrentUserID;
                    var status = 'Not Complete';

                    if (typeof dataObject.userList[viewingCurrentUserID][ID] !== 'undefined') {
                        if (typeof dataObject.userList[viewingCurrentUserID][ID].lessons[postID] !== 'undefined') {
                            status = 'Completed';
                        }
                    }

                    tableData.push([postTitle, status]);
                }
            });
        }


        return tableData;
    },

    userSingleCourseTopicsTableData: function (ID) {

        var tableData = [];

        if (typeof dataObject.courseList[ID].associatedPosts === 'undefined') {
            return tableData;
        }

        // Get all unique associated posts ... with LD 2.5 there maybe a double course association
        var associatedPosts = dataObject.courseList[ID].associatedPosts.filter(this.arrayUnique);

        if (typeof dataObject.courseList[ID].associatedPosts !== 'undefined') {
            // Get all unique associated posts ... with LD 2.5 there maybe a double course association
            var associatedPosts = dataObject.courseList[ID].associatedPosts.filter(this.arrayUnique);

            jQuery.each(associatedPosts, function (index, postID) {

                if (typeof dataObject.topicList[postID] !== 'undefined') {

                    var postTitle = dataObject.topicList[postID].post_title;
                    var viewingCurrentUserID = reportingTabs.viewingCurrentUserID;
                    var status = 'Not Complete';

                    if (typeof dataObject.userList[viewingCurrentUserID][ID] !== 'undefined') {

                        if (typeof dataObject.userList[viewingCurrentUserID][ID].topics[postID] !== 'undefined') {
                            status = 'Completed';
                        }
                        else {

                            jQuery.each(dataObject.userList[viewingCurrentUserID][ID].topics, function (lessonID, topicArray) {
                                if (typeof  topicArray[postID] !== 'undefined' && topicArray[postID] === 1) {
                                    status = 'Completed';
                                }
                            })

                        }
                    }

                    tableData.push([postTitle, status]);
                }
            });
        }
        return tableData;
    },

    userSingleCourseQuizzesTableData: function (ID) {

        var tableData = [];
        if (typeof dataObject.courseList[ID].associatedPosts === 'undefined') {
            return tableData;
        }

        // Get all unique associated posts ... with LD 2.5 there maybe a double course association
        var associatedPosts = dataObject.courseList[ID].associatedPosts.filter(this.arrayUnique);

        jQuery.each(associatedPosts, function (index, courseAssociatedPost) {


            var viewingCurrentUserID = reportingTabs.viewingCurrentUserID;
            // Make sure the user has data for the current module before access module data
            if (typeof dataObject.userList[viewingCurrentUserID]['quizzes'] !== 'undefined' && typeof dataObject.userList[viewingCurrentUserID]['quizzes'][0][courseAssociatedPost] !== 'undefined') {

                // Make sure the user has data for the current module before access module data
                if (typeof dataObject.userList[viewingCurrentUserID]['quizzes'] !== 'undefined' && typeof dataObject.userList[viewingCurrentUserID]['quizzes'][0][courseAssociatedPost] !== 'undefined') {

                    // Make sure the current module is a quiz
                    if (typeof dataObject.userList[viewingCurrentUserID] !== 'undefined' &&
                        typeof dataObject.userList[viewingCurrentUserID]['quizzes'][0][courseAssociatedPost].type !== 'undefined' &&
                        dataObject.userList[viewingCurrentUserID]['quizzes'][0][courseAssociatedPost].type === 'quiz') {

                        // Make sure there is quiz data
                        if (1 <= dataObject.userList[viewingCurrentUserID]['quizzes'][0][courseAssociatedPost].attempts) {

                            if (typeof dataObject.quizList[courseAssociatedPost] !== 'undefined') {
                                var postTitle = dataObject.quizList[courseAssociatedPost].post_title;
                                var score = 'Not Complete';
                                var dateCompleted = '';

                                var count;
                                var attempts = dataObject.userList[viewingCurrentUserID]['quizzes'][0][courseAssociatedPost].attempts;

                                for (count = 0; count < attempts; count++) {
                                    score = dataObject.userList[viewingCurrentUserID]['quizzes'][0][courseAssociatedPost][count].percentage + '%';

                                    dateCompleted = dataObject.userList[viewingCurrentUserID]['quizzes'][0][courseAssociatedPost][count].time;
                                    var d = new Date(dateCompleted * 1000);
                                    var month = (d.getMonth() + 1);
                                    var year = d.getFullYear();
                                    if (10 > month) {
                                        month = '0' + month;
                                    }
                                    var date = d.getDate();
                                    dateCompleted = year + '-' + month + '-' + date;

                                    tableData.push([postTitle, score, dateCompleted]);

                                }
                            }
                        }
                    }
                }
            }
        });


        return tableData;
    },

    userSingleCourseAssignmentsTableData: function (ID) {
        var tableData = [];
        if (typeof dataObject.courseList[ID].associatedPosts === 'undefined') {
            return tableData;
        }

        // Get all unique associated posts ... with LD 2.5 there maybe a double course association
        var associatedPosts = dataObject.courseList[ID].associatedPosts.filter(this.arrayUnique);

        if (typeof dataObject.courseList[ID].associatedPosts !== 'undefined') {
            // Get all unique associated posts ... with LD 2.5 there maybe a double course association
            var associatedPosts = dataObject.courseList[ID].associatedPosts.filter(this.arrayUnique);

            jQuery.each(associatedPosts, function (index, postID) {
                if (typeof dataObject.assignmentList[postID] !== 'undefined') {

                    var postTitle = '<a href="' + dataObject.links.assignment + '?post=' + postID + '&action=edit">' + dataObject.assignmentList[postID].post_title + '</a>';
                    var viewingCurrentUserID = reportingTabs.viewingCurrentUserID;
                    var completion = 'Not Approved';
                    var submittedOn = '';

                    if (typeof dataObject.userList[viewingCurrentUserID][ID] !== 'undefined') {
                        if (typeof dataObject.userList[viewingCurrentUserID][postID] !== 'undefined') {

                            completion = dataObject.userList[viewingCurrentUserID][postID].approval_status;
                            if (1 === completion) {
                                completion = 'Approved';
                            }
                            submittedOn = dataObject.userList[viewingCurrentUserID][postID].completed_on;

                            tableData.push([postTitle, completion, submittedOn]);
                        }
                    }


                }
            });
        }
        return tableData;
    },

    userSingleCourseTinCanTableData: function (courseID) {

        var tableData = [];
        var viewingCurrentUserID = reportingTabs.viewingCurrentUserID;

        if (typeof dataObject.userList[viewingCurrentUserID].tinCanStatements !== 'undefined'
            && typeof dataObject.userList[viewingCurrentUserID].tinCanStatements[courseID] !== 'undefined') {

            var courseTinCanStatements = dataObject.userList[viewingCurrentUserID].tinCanStatements[courseID];

            jQuery.each(courseTinCanStatements, function (index, lessons) {

                jQuery.each(lessons, function (index, statement) {

                    tableData.push([statement.lesson_name, statement.module_name, statement.target_name, statement.verb, statement.result, statement.stored]);
                });
            });
        }

        return tableData;
    },

    createCsvFileName: function (tableType, ID) {
        var fileName,
            filePrefix,
            now,
            date,
            table,
            courseId,
            userId;

        filePrefix = "tincanny_export-";


        now = new Date(Date.now());
        date = now.getFullYear() + '_' + (now.getMonth() + 1) + '_' + now.getDate() + '-';

        table = tableType;

        switch (tableType) {
            case 'coursesOverviewTable':
                courseId = '';
                userId = '';
                break;
            case 'courseSingleOverviewSummaryTable':
                courseId = '-' + ID;
                userId = '';
                break;
            case 'courseSingleTable':
                courseId = '-' + ID;
                userId = '';
                break;
            case 'usersOverviewTable':
                courseId = '';
                userId = '';
                break;
            case 'userSingleOverviewTable':
                courseId = '';
                userId = '-' + ID;
                break;
            case 'userSingleCoursesOverviewTable':
            case 'userSingleCourseProgressSummaryTable':
            case 'userSingleCourseLessonsTable':
            case 'userSingleCourseTopicsTable':
            case 'userSingleCourseQuizzesTable':
            case 'userSingleCourseAssignmentsTable':
            case 'userSingleCourseTinCanTable':
                courseId = '-' + ID;
                userId = '-' + reportingTabs.viewingCurrentUserID;
                break;
            default:
                courseId = '';
                userId = '';
        }

        fileName = filePrefix + date + table + courseId + userId;

        return fileName;
    },

    arrayUnique: function (value, index, self) {
        return self.indexOf(value) === index;
    }

};