var reportingTabs = {

    currentTabID: false,

    viewingCurrentUserID: false,

    elTabNav: jQuery('.uo-admin-reporting-tabs a'),

    elTabGroup: jQuery('.uo-admin-reporting-tabgroup > div'),

    triggerTabGroup: function (element) {
        jQuery('a[href="' + element + '"]').trigger("click");
    },

    navigateTabs: function (e) {

        e.preventDefault();
        var currentTab = jQuery(this);
        var tabGroup = '#' + currentTab.parents('.uo-admin-reporting-tabs').data('tabgroup');
        var othersTabs = currentTab.closest('li').siblings().children('a');
        var target = currentTab.attr('href');
        this.currentTabID = target;

        othersTabs.removeClass('active');
        currentTab.addClass('active');
        jQuery(tabGroup).children('div').hide();

        if( '#tin-can' !== target && false === dataObject.dataObjectPopulated ){
            reportingQueryString.isTinCanOnly = false;
            dataObject.dataObjectPopulated = true;
            dataObject.getData(target);

        }else{

            switch (target) {

                case '#courseReportTab':
                    settingsPageModule.removeSwitchEvents();
                    // set up data with data
                    reportingTables.createTable('coursesOverviewTable', '#coursesOverviewTable', 0);
                    chartVars.setCourseOverviewGraphTable(dataObject.dataTables.coursesOverviewTable, 1);
                    //AmCharts.makeChart('coursesOverviewGraph', chartVars.courseOverviewGraph());
                    break;

                case '#userReportTab':
                    settingsPageModule.removeSwitchEvents();
                    if (typeof reportingTables.tableObjects['usersOverviewTable'] === 'undefined') {
                        reportingTables.createTable('usersOverviewTable', '#usersOverviewTable', 0);
                    }
                    break;

                case '#tin-can':
                    settingsPageModule.removeSwitchEvents();
                    break;

                case '#settings':
                    settingsPageModule.addSwitchEvents();
                    break;

                default:
                    settingsPageModule.removeSwitchEvents();
            }

            jQuery(target).show();

        }





    },

    addTabEvents: function () {
        this.elTabNav.on('click', this.navigateTabs);
    },

    removeTabEvents: function () {
        this.elTabNav.off('click', this.navigateTabs);
    },

    addTableEvents: function () {

        var vThis = this;
        var courseNavigateLink = jQuery('#course-navigate-link');

        jQuery('.uo-admin-reporting').on('click', 'tbody tr', function (e) {

            var tableElementID = jQuery(this).closest('table').attr('id');

            if (jQuery(this).hasClass('selected')) {
                jQuery(this).removeClass('selected');
            }
            else {
                jQuery('tr.selected').removeClass('selected');
                jQuery(this).addClass('selected');
            }

            // The first row of the bale is hidden from the user facing chart, it lists the ids for either course or
            // user depending of the chart type
            var rowData = reportingTables.tableObjects[tableElementID].row(this).data();
            var ID = Number(rowData[0]);

            if ('coursesOverviewTable' === tableElementID) {



                // Hide Courses Overview and Show Course Single data
                jQuery('#coursesOverviewContainer').slideUp('slow', function(){

                    jQuery('#courseSingleTitle').show().children('span').text(dataObject.courseList[ID].post_title);

                    // set back navigation
                    courseNavigateLink.css('display', 'block');
                    courseNavigateLink.text('< '+ reportingApiSetup.learnDashLabels.courses +' Summary');

                    jQuery('#courseSingleContainer').delay(250).slideDown('slow')
                });

                // create tables
                reportingTables.createTable('courseSingleOverviewSummaryTable', '#courseSingleOverviewSummaryTable', ID);
                reportingTables.createTable('courseSingleTable', '#courseSingleTable', ID);

                // create charts
                AmCharts.makeChart("courseSingleOverviewPieChart", chartVars.courseSingleOverviewPieChartData(ID));
                AmCharts.makeChart("courseSingleActivitiesGraph", chartVars.courseSingleCompletionChartData(ID));
            }

            if ('courseSingleTable' === tableElementID) {

                jQuery('#courseSingleContainer').slideUp('slow', function(){
                    jQuery('#usersOverviewContainer').hide();
                    reportingTabs.triggerTabGroup('#userReportTab');
                    jQuery('#courseSingleContainer').show();
                });

                if(typeof dataObject.userList[ID].tinCanStatements === 'undefined'){

                    var testTinCan = uoReportingAPI.reportingApiCall('tincan_data', ID);

                    testTinCan.done(function (response) {

                        if( 0 === response.length){
                            dataObject.addToTinCanStatements(ID, false);
                        }else{
                            dataObject.addToTinCanStatements(response.user_ID, response.tinCanStatements);
                        }
                    });

                }

                vThis.viewingCurrentUserID = ID;

                reportingTables.createTable('userSingleOverviewTable', '#userSingleOverviewTable', ID);
                reportingTables.createTable('userSingleCoursesOverviewTable', '#userSingleCoursesOverviewTable', ID);
                if(reportingApiSetup.editUsers === "1"){
                    document.getElementById('singleUserProfileDisplayName').innerHTML = '<a href="'+dataObject.links.profile+'?user_id='+ID+'">'+dataObject.userList[ID].display_name+'</a>';
                }else{
                    document.getElementById('singleUserProfileDisplayName').innerHTML = '<div>'+dataObject.userList[ID].display_name+'</div>';
                }
                document.getElementById('singleUserProfileID').innerHTML = ID;
                document.getElementById('singleUserProfileEmail').innerHTML = dataObject.userList[ID].user_email;


                jQuery('#user-navigate-link')
                    .data('target-last','usersOverviewTable')
                    .text('< Users Overview')
                    .css('display', 'block');

                jQuery('#singleUserProfileContainer').slideDown('slow');
                jQuery('#userSingleOverviewContainer').slideDown('slow', function(){
                    jQuery('#userSingleCoursesOverviewContainer').slideDown('slow');
                });

            }

            if ('usersOverviewTable' === tableElementID) {

                if(typeof dataObject.userList[ID].tinCanStatements === 'undefined'){

                    var testTinCan = uoReportingAPI.reportingApiCall('tincan_data', ID);

                    testTinCan.done(function (response) {

                        dataObject.addToTinCanStatements(response.user_ID, response.tinCanStatements);
                    });

                }

                vThis.viewingCurrentUserID = ID;
                // Populate Simple Profile
                if(reportingApiSetup.editUsers === "1"){
                    document.getElementById('singleUserProfileDisplayName').innerHTML = '<a href="'+dataObject.links.profile+'?user_id='+ID+'">'+dataObject.userList[ID].display_name+'</a>';
                }else{
                    document.getElementById('singleUserProfileDisplayName').innerHTML = '<div>'+dataObject.userList[ID].display_name+'</div>';
                }

                document.getElementById('singleUserProfileID').innerHTML = ID;
                document.getElementById('singleUserProfileEmail').innerHTML = dataObject.userList[ID].user_email;
                reportingTables.createTable('userSingleOverviewTable', '#userSingleOverviewTable', ID);
                reportingTables.createTable('userSingleCoursesOverviewTable', '#userSingleCoursesOverviewTable', ID);

                jQuery('#usersOverviewContainer').slideUp('slow', function(){

                    // Navigation
                    jQuery('#user-navigate-link')
                        .data('target-last','usersOverviewTable')
                        .text('< User Overview')
                        .css('display', 'block');

                    jQuery('#singleUserProfileContainer').slideDown('slow');
                    jQuery('#userSingleOverviewContainer').slideDown('slow', function(){
                        jQuery('#userSingleCoursesOverviewContainer').slideDown('slow');
                    });
                });

            }

            if ('userSingleCoursesOverviewTable' === tableElementID) {

                jQuery('#userCourseSingleTitle').show().children('span').text(dataObject.courseList[ID].post_title);

                reportingTables.createTable('userSingleCourseProgressSummaryTable', '#userSingleCourseProgressSummaryTable', ID);
                reportingTables.createTable('userSingleCourseLessonsTable', '#userSingleCourseLessonsTable', ID);
                reportingTables.createTable('userSingleCourseTopicsTable', '#userSingleCourseTopicsTable', ID);
                reportingTables.createTable('userSingleCourseQuizzesTable', '#userSingleCourseQuizzesTable', ID);
                reportingTables.createTable('userSingleCourseAssignmentsTable', '#userSingleCourseAssignmentsTable', ID);
                reportingTables.createTable('userSingleCourseTinCanTable', '#userSingleCourseTinCanTable', ID);

                jQuery('#userSingleOverviewContainer').slideUp('slow');
                jQuery('#userSingleCoursesOverviewContainer').slideUp('slow', function(){

                    jQuery('#user-navigate-link')
                        .data('target-last','userSingleCoursesOverviewTable')
                        .text('< User\'s '+ reportingApiSetup.learnDashLabels.courses +' Overview')
                        .css('display', 'block');

                    jQuery('#userSingleCourseProgressSummaryContainer').slideDown('slow', function(){
                        jQuery('#userSingleCourseProgressMenu li').css('background-color','black');
                        jQuery('#menuLessons').css('background-color','#29779e');
                        jQuery('#userSingleCourseProgressMenuContainer').slideDown('slow');
                        jQuery('#userSingleCourseLessonsContainer').slideDown('slow');
                    });
                });

            }


        });
    },
    addNavigationEvents : function(){

        // Course Report Back Button
        jQuery('#course-navigate-link').on('click', function(event){

            jQuery('#courseSingleContainer').slideUp('slow', function(){
                jQuery(event.currentTarget).hide();
                jQuery('#courseSingleTitle').hide().children('span').text('');
                jQuery('#coursesOverviewContainer').delay(500).slideDown('slow');
            });

        });

        // User Report Back button
        jQuery('#user-navigate-link').on('click', function(event){

            var target = jQuery(event.currentTarget).data('target-last');

            if(target == 'usersOverviewTable'){

                jQuery('#userSingleCoursesOverviewContainer').slideUp('slow', function(){
                    jQuery('#userSingleOverviewContainer, #singleUserProfileContainer ').slideUp('slow', function(){
                        jQuery('#usersOverviewContainer').slideDown('slow');
                        jQuery(event.currentTarget).hide();
                    });
                });
            }

            if(target == 'userSingleCoursesOverviewTable'){

                jQuery('#userSingleCoursesOverviewContainer').slideUp('slow', function(){
                    var userSingleCoursesOverviewContainers = '#userSingleCourseProgressMenuContainer,#userSingleCourseLessonsContainer,#userSingleCourseTopicsContainer,#userSingleCourseQuizzesContainer,#userSingleCourseAssignmentsContainer,#userSingleCourseTinCanContainer';
                    jQuery(userSingleCoursesOverviewContainers).slideUp('slow', function(){
                        jQuery('#userSingleCourseProgressSummaryContainer').slideUp('slow', function(){

                            jQuery('#userCourseSingleTitle').hide().children('span').text('');

                            // Navigation
                            jQuery('#user-navigate-link')
                                .data('target-last','usersOverviewTable')
                                .text('< User Overview')
                                .css('display', 'block');

                            jQuery('#userSingleOverviewContainer ').slideDown('slow', function() {
                                jQuery('#userSingleCoursesOverviewContainer').slideDown('slow');
                            });
                        });



                    });
                });
            }
        });

        // User's Single Course Module Toggle
        jQuery('#userSingleCourseProgressMenu li').on('click', function(event){
            var listItem = event.currentTarget;
            var toggleTarget  = listItem.id;

            jQuery('#userSingleCourseProgressMenu li').css('background-color','#000000');
            jQuery(listItem).css('background-color','#29779e');

            var lessons = jQuery('#userSingleCourseLessonsContainer');
            var topics = jQuery('#userSingleCourseTopicsContainer');
            var quizzes = jQuery('#userSingleCourseQuizzesContainer');
            var assignments = jQuery('#userSingleCourseAssignmentsContainer');
            var tinCan = jQuery('#userSingleCourseTinCanContainer');

            lessons.hide();
            topics.hide();
            quizzes.hide();
            assignments.hide();
            tinCan.hide();

            switch (toggleTarget){
                case 'menuLessons': lessons.show(); break;
                case 'menuTopics': topics.show(); break;
                case 'menuQuizzes': quizzes.show(); break;
                case 'menuAssignments': assignments .show(); break;
                case 'menuTinCan': tinCan .show(); break;
            }



        });

    }

};