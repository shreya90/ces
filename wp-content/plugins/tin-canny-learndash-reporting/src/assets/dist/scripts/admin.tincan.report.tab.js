/**
 * @package Uncanny TinCan API
 * @author Uncanny Owl
 * @version 1.0.0
 */

jQuery( document ).ready( function($) {
	// Per Page
	$( '#tincan-filters-per_page select' ).change( function() {
		var value = $(this).find( 'option:selected' ).val();

		insertParam( 'per_page', value );
	});

	// <!-- Change Date Range Type
	$( '#tincan-filters-top #tc_filter_date_range_last' ).click( function() {
		$( '#tincan-filters-top input[name="tc_filter_date_range"][value="last"]' ).click();
	});

	$( '#tincan-filters-top input[name="tc_filter_start"], #tincan-filters-top input[name="tc_filter_end"]' ).click( function() {
		$( '#tincan-filters-top input[name="tc_filter_date_range"][value="from"]' ).click();
	});
	// Change Date Range Type -->

	// CSV Export
	$( '#do_tc_export_csv' ).click( function( e ) {
		var fields = [ 'tc_filter_group', 'tc_filter_user', 'tc_filter_course', 'tc_filter_lesson', 'tc_filter_module', 'tc_filter_action', 'tc_filter_date_range', 'tc_filter_date_range_last', 'tc_filter_start', 'tc_filter_end' ];

		$.each( fields, function( index, value ) {
			var value_top = $( '#tincan-filters-top [name="' + value + '"]' ).val();

			$( '#tincan-filters-bottom [name="' + value + '"]' ).val( value_top );
		});

		var tc_filter_date_range = $( '#tincan-filters-top input[name="tc_filter_date_range"]:checked' ).val();

		if ( typeof tc_filter_date_range != 'undefined' ) {
			$( '#tincan-filters-bottom input[name="tc_filter_date_range"]' ).val( tc_filter_date_range );
		} else {
			$( '#tincan-filters-bottom input[name="tc_filter_date_range"]' ).val( '' );
		}
	});

	// Ajax : Modules from Course
	$( '#tincan-filters-top select[name="tc_filter_course"]' ).change( function() {
		if ( !$(this).val() ) {
			ResetModules();
			return;
		}

		var data = {
			'action': 'GET_Modules',
			'tc_filter_course' : $( this ).val()
		};

		$.post( ajaxurl, data, function( response ) {
			// Do Something

			ResetModules();
			$( '#tincan-filters-top select[name="tc_filter_module"]' ).append( response );
		});
	});

	// Change Sorting Indicator
	$( '.uo-admin-reporting-tab-single#tin-can table.wp-list-table thead th' ).each( function() {
		var $indicator = $(this).find( '.sorting-indicator' );

		if ( $indicator.css( 'visibility' ) == 'hidden' ) {
			$indicator.addClass( 'double-headed-triangles' );
		}
	});

	function ResetModules() {
		$( '#tincan-filters-top select[name="tc_filter_module"]' ).children().each( function (i) {
			if ( i !== 0 ) $(this).remove();
		});
	}

	function insertParam(key, value)
	{
	    key = encodeURI(key); value = encodeURI(value);

	    var kvp = document.location.search.substr(1).split('&');

	    var i=kvp.length; var x; while(i--)
	    {
	        x = kvp[i].split('=');

	        if (x[0]==key)
	        {
	            x[1] = value;
	            kvp[i] = x.join('=');
	            break;
	        }
	    }

	    if(i<0) {kvp[kvp.length] = [key,value].join('=');}

	    //this will reload the page, it's likely better to store this until finished
	    document.location.search = kvp.join('&');
	}
});