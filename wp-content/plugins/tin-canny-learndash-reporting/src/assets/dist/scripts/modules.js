/**
 * @package Uncanny TinCan API
 * @author Uncanny Owl
 * @version 1.0.0
 */

var tincannyModuleController = new function() {
	var $this = this;
	this.count = {};
	this.completion = {};
	this.bound = false;
	this.hasCompletionSetting = false;

	this.ready = function() {
		this.iFrameReady();
		this.lightboxReady();
		this.newWindowReady();
		this.setButton();

		tincannyModuleFunctions.checkH5PCompletiton();
	};

	this.setButton = function() {
		// Condition : No Module
		var is_empty    = true;
		var is_complete = false;

		Object.keys( this.count ).forEach( function( key ) {
			if ( $this.count[ key ] !== undefined )
				is_empty = false;
		});

		// Condition : Any Completion
		Object.keys( $this.count ).forEach( function( key ) {
			if ( $this.count[ key ] > 0 && Object.keys( $this.completion[ key ] ).length > 0 )
				is_complete = true;
		});

		if ( is_empty ) {
			console.log('Its empty');
			jQuery( '#sfwd-mark-complete input' ).removeAttr( 'disabled' );
			jQuery( '#sfwd-mark-complete input' ).removeClass( 'disabled' );

		} else if ( is_complete ) {
            console.log('Its complete');
			jQuery( '#sfwd-mark-complete input' ).removeAttr( 'disabled' );
			jQuery( '#sfwd-mark-complete input' ).removeClass( 'disabled' );
			tinCanny.doAction( 'after_tincanny_insert_list' );

		} else {
            console.log('other');
			jQuery( '#sfwd-mark-complete input' ).attr( 'disabled', true );
			jQuery( '#sfwd-mark-complete input' ).addClass( 'disabled' );
		}
	};

	this.iFrameReady = function() {
		jQuery('.AnC-iFrame').bind('ready load', function() {
			tincannyModuleFunctions.checkStatues(jQuery(this)[0].contentWindow, 'iFrame');
		});
	};

	this.lightboxReady = function() {
		jQuery('body').bind('DOMSubtreeModified', function() {
			jQuery('.nivo-lightbox-item').bind('load', function() {
				if (!jQuery(this).attr('data-tcloaded')) {
					tincannyModuleFunctions.checkStatues(jQuery(this)[0].contentWindow, 'lightbox');
					jQuery(this).attr('data-tcloaded', true);
				}
			});
		});
	};

	this.newWindowReady = function() {
		var bindNewWindow = function(targetWin) {
			if ($this.bound)
				return;

			if (
				!targetWin.document.getElementsByTagName('meta') ||
				!targetWin.document.URL ||
				!targetWin.document.URL.includes('uncanny-snc')
			) {
				setTimeout(function() {
					bindNewWindow(targetWin);
				}, 500);

				return;
			}

			tincannyModuleFunctions.checkStatues( targetWin, 'blank' );
		};

		jQuery('.AnC-Link').click(function(e) {
			if (jQuery(this).attr('target') == '_blank' ) {
				e.preventDefault();

				var url = jQuery(this).attr('href');

				if (window.popup)
					window.popup.close();

				window.popup = open(url, 'AnC');

				jQuery(window.popup.document).ready(function() {
					$this.bound = false;
					bindNewWindow(window.popup);
				});
			}
		});
	};

	this.replaceRequest = function( targetWin, targetURL, contentType ) {
		targetWin.XMLHttpRequest.prototype.oldSend = targetWin.XMLHttpRequest.prototype.send;

		var newSend = function( statement ) {
			this.oldSend(statement);

			try {
				$this.checkCompletion(targetURL, contentType, statement);
			} catch(err) {}
		};

		targetWin.XMLHttpRequest.prototype.send = newSend;
	};

	this.checkCompletion = function (targetURL, contentType, statement) {
		var stop = true;
		var regEx = /\/uncanny-snc\/([0-9]+)\//;
		var idResult = regEx.exec(targetURL);
		var id = parseInt(idResult[1]);

		if (statement.includes('Authorization=LearnDashId') && !statement.includes('stateId=')) {
			stop = false;
		} else {
			var json = JSON.parse(statement);

			if (typeof json === 'object')
				stop = false;
		}

		if ($this.completion[contentType][id])
			stop = true;

		if (stop)
			return;

		var data = {
			'action': 'Check ' + contentType + ' Completion',
			'URL': targetURL
		};

		// Check Completion
		jQuery.post( wp_ajax_url, data, function( response ) {
			if ( response ) {
				$this.markComplete( contentType, targetURL );
			}
		});
	};

	this.markComplete = function( contentType, targetURL ) {
		var patternModuleId = /uncanny-snc\/([0-9]+)\//i;
		var resultModuleId = patternModuleId.exec(targetURL);

		if ( resultModuleId[1] ) {
			this.completion[ contentType ][ resultModuleId[1] ] = true;
			this.setButton();
		}
	};
};

var tincannyModuleFunctions = new function() {
	var $this = this;

	this.checkStatues = function( targetWin, openMethod ) {
		var targetURL = targetWin.document.URL;

		// Captivate
		if ( targetURL.includes( "Captivate" ) ) {
			this.processCaptivate( targetWin, targetURL, openMethod );

		// iSpring
		} else if ( targetURL.includes( "/res/" ) ) {
			tincannyModuleController.replaceRequest( targetWin, targetURL, 'iSpring' );

		// Articulate Rise
		} else if ( targetURL.includes( "ArticulateRise" ) ) {
			tincannyModuleController.replaceRequest( targetWin, targetURL, 'ArticulateRise' );

		// Storyline
		} else {
			// Storyline : Replace Function
			if ( targetWin.OnSendComplete ) {
				targetWin.OnSendComplete = function( commObj ) {
					tincannyStorylineFunctions.OnSendComplete(targetWin, commObj);
				};

			} else {
				tincannyModuleController.replaceRequest(targetWin, targetURL, 'Storyline');
			}
		}
	};

	this.processCaptivate = function( targetWin, targetURL, openMethod ) {
		var frame;

		if ( targetURL.includes( "multiscreen.html" ) ) {
			frame = jQuery(targetWin.document)[0].getElementsByTagName('frame')[0];

			if ( frame.contentWindow.DoCPExit ) {
				tincannyModuleController.replaceRequest( frame.contentWindow, targetURL, 'Captivate' );

				frame.contentWindow.DoCPExit = function() {
					tincannyCaptivateFunctions.DoCPExit( window, openMethod );
				};
			}

			jQuery(frame).on( 'load ready', function() {
				tincannyModuleController.replaceRequest( jQuery(this)[0].contentWindow, targetURL, 'Captivate' );

				jQuery(this)[0].contentWindow.DoCPExit = function() {
					tincannyCaptivateFunctions.DoCPExit( window, openMethod );
				};
			});

		// Captivate Normal : index_TINCAN.html
		} else if ( targetURL.includes( "index_TINCAN.html" ) ) {
			tincannyModuleController.replaceRequest( targetWin, targetURL, 'Captivate' );

			targetWin.initializeCP = function() {
				tincannyCaptivateFunctions.initializeCP( window, targetWin );
			};

		// Captivate From SCORM
		} else {
			frame = jQuery( targetWin );

			jQuery(frame).ready( function() {
				tincannyModuleController.replaceRequest( targetWin, targetURL, 'Captivate' );

				targetWin.DoCPExit = function() {
					tincannyCaptivateFunctions.DoCPExit( window, openMethod );
				};
			});
		}
	};

	this.checkH5PCompletiton = function() {
		if (tincannyModuleController.count.H5P <= 0)
			return;

		// Detect Ajax Completion
		jQuery(document).ajaxComplete(function( event, xhr, settings ) {
			// Detect H5P xAPI
			if (settings.url === WP_H5P_XAPI_STATEMENT_URL) {
				// Get Verb
				var verb = settings.data.match(/http%3A%2F%2Fadlnet.gov%2Fexpapi%2Fverbs%2F([a-z]+)/);

				eval(unescape(settings.data));
				var id = statement.object.definition.extensions['http://h5p.org/x-api/h5p-local-content-id'];

				if (!tincannyModuleController.hasCompletionSetting) {
					tincannyModuleController.completion.H5P[ id ] = true;
					tincannyModuleController.setButton();
					return;
				}

				if ( xhr.responseJSON.message == 'true' ) {
					tincannyModuleController.completion.H5P[ id ] = true;
					tincannyModuleController.setButton();
				}
			}
		});
	};
};

var tincannyCaptivateFunctions = new function() {
	var $this = this;

	this.initializeCP = function( win, targetWin ) {
		if( targetWin.initialized )
			return;

		targetWin.initCalled = true ;

		if( targetWin.cp && targetWin.cp.pg && targetWin.deviceReady === false)
			return;

		targetWin.cpInit = function() {
			targetWin.document.body.innerHTML = " <div class='cpMainContainer' id='cpDocument' style='left: 0px; top:0px;' >	<div id='main_container' style='top:0px;position:absolute;width:100%;height:100%;'>	<div id='projectBorder' style='top:0px;left:0px;width:100%;height:100%;position:absolute;display:block'></div>	<div class='shadow' id='project_container' style='left: 0px; top:0px;width:100%;height:100%;position:absolute;overflow:hidden;' >	<div id='project' class='cp-movie' style='width:100% ;height:100%;overflow:hidden;'>		<div id='project_main' class='cp-timeline cp-main'>			<div id='div_Slide' onclick='cp.handleClick(event)' style='top:0px; width:100% ;height:100% ;position:absolute;-webkit-tap-highlight-color: rgba(0,0,0,0);'></div>			<canvas id='slide_transition_canvas'></canvas>		</div>		<div id='autoplayDiv' style='display:block;text-align:center;position:absolute;left:0px;top:0px;'>			<img id='autoplayImage' src='' style='position:absolute;display:block;vertical-align:middle;'/>			<div id='playImage' tabindex='9999' role='button' aria-label='play' onkeydown='cp.CPPlayButtonHandle(event)' onClick='cp.movie.play()' style='position:absolute;display:block;vertical-align:middle;'></div>		</div>	</div>	<div id='toc' style='left:0px;position:absolute;-webkit-tap-highlight-color: rgba(0,0,0,0);'>	</div>	<div id='playbar' style='bottom:0px; position:fixed'>	</div>	<div id='cc' style='left:0px; position:fixed;visibility:hidden;pointer-events:none;' onclick='cp.handleCCClick(event)'>		<div id='ccText' style='left:0px;float:left;position:absolute;width:100%;height:100%;'>		<p style='margin-left:8px;margin-right:8px;margin-top:2px;'>		</p>		</div>		<div id='ccClose' style='background-image:url(./assets/htmlimages/ccClose.png);right:10px; position:absolute;cursor:pointer;width:13px;height:11px;' onclick='cp.showHideCC()'>		</div>	</div>	<div id='gestureIcon' class='gestureIcon'>	</div>	<div id='gestureHint' class='gestureHintDiv'>		<div id='gImage' class='gesturesHint'></div>	</div>	<div id='pwdv' style='display:block;text-align:center;position:absolute;width:100%;height:100%;left:0px;top:0px'></div>	<div id='exdv' style='display:block;text-align:center;position:absolute;width:100%;height:100%;left:0px;top:0px'></div>	</div>	</div></div><div id='blockUserInteraction' class='blocker' style='width:100%;height:100%;'>	<table style='width:100%;height:100%;text-align:center;vertical-align:middle' id='loading' class='loadingBackground'>		<tr style='width:100%;height:100%;text-align:center;vertical-align:middle'>			<td style='width:100%;height:100%;text-align:center;vertical-align:middle'>				<image id='preloaderImage'></image>				<div id='loadingString' class='loadingString'>Loading...</div>			</td>		</tr>	</table></div> <div id='initialLoading'></div>";

			targetWin.cp.DoCPInit();
			targetWin.lCpExit = targetWin.DoCPExit;

			targetWinDoCPExit = function() {
				if ( openMethod == 'iFrame' )
					return;

				if ( openMethod == 'lightbox' ) {
					$this.closeOverlay( win );
					return;
				}

				if( targetWin.cp.UnloadActivties)
					targetWin.cp.UnloadActivties();

				targetWin.lCpExit();
			};
		};

		targetWin.cpInit();
		targetWin.initialized = true;
	};

	this.DoCPExit = function( win, openMethod ) {
		if ( openMethod == 'iFrame' )
			return;

		var win_;

		if( win != win.parent && win.parent && win.parent.DoCPExit !== undefined ) {
			win.parent.DoCPExit();

		} else {
			if( win.top == self ) {
				win_ = win.open("","_self");
				$this.closeWindow( win_, openMethod );

			} else {
				win_ = win.top.open("","_self");
				$this.closeWindow( win_.top, openMethod );
			}
		}
	};

	this.closeWindow = function( win, openMethod ) {
		if ( openMethod == 'lightbox' ) {
			$this.closeOverlay( win );
			return;
		}

		if ( window.popup )
			window.popup.close();
		else
			win.close();
	};

	this.closeOverlay = function( win ) {
		var elements = win.document.getElementsByClassName("nivo-lightbox-overlay");
		while(elements.length > 0){
			elements[0].parentNode.removeChild(elements[0]);
		}
	};
};

var tincannyStorylineFunctions = new function() {
	this.OnSendComplete = function(targetWin, commObj) {
		if (commObj.MessageType == targetWin.TYPE_RESUME_RESTORE) {
			targetWin.GetPlayer().SetTinCanResume(commObj.responseText);
		}

		targetWin.g_bWaitingTinCanResponse = false;
		targetWin.g_oCurrentRequest = null;

		if ( targetWin.g_arrTinCanMsgQueue.length > 0 && !targetWin.g_bStopPosting ) {
			var test = targetWin.SendRequest(targetWin.g_arrTinCanMsgQueue.shift());
		}

		tincannyModuleController.checkCompletion(jQuery(targetWin.document).context.URL, 'Storyline', commObj.responseText);
	};
};

jQuery(document).ready(function() {
	tincannyModuleController.ready();
});
