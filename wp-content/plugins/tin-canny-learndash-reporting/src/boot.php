<?php

namespace uncanny_learndash_reporting;

class Boot extends Config {

	/**
	 * class constructor
	 */
	public function __construct() {

		global $uncanny_learndash_reporting;

		if ( ! isset( $uncanny_learndash_reporting ) ) {
			$uncanny_learndash_reporting = new \stdClass();
		}

		// We need to check if spl auto loading is available when activating plugin
		// Plugin will not activate if SPL extension is not enabled by throwing error
		if ( ! extension_loaded( 'SPL' ) ) {
			$spl_error = esc_html__( 'Please contact your hosting company to update to php version 5.3+ and enable spl extensions.', 'uncanny-learndash-reporting' );
			trigger_error( $spl_error, E_USER_ERROR );
		}

		spl_autoload_register( array( __CLASS__, 'auto_loader' ) );

		$uncanny_learndash_reporting->admin_menu             = new ReportingAdminMenu;
		$uncanny_learndash_reporting->reporting_capabilities = new ReportingCapabilities;
		$uncanny_learndash_reporting->reporting_api          = new ReportingApi;

		/* Licensing */

		// URL of store powering the plugin
		define( 'UO_REPORTING_STORE_URL', 'https://www.uncannyowl.com/' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

		// Store download name/title
		define( 'UO_REPORTING_ITEM_NAME', 'Tin Canny LearnDash Reporting' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

		// include updater
		$updater = self::get_include( 'EDD_SL_Plugin_Updater.php', __FILE__ );
		include_once( $updater );

		add_action( 'admin_init', array( __CLASS__, 'uo_reporting_plugin_updater' ), 0 );
		add_action( 'admin_menu', array( __CLASS__, 'uo_reporting_license_menu' ) );
		add_action( 'admin_init', array( __CLASS__, 'uo_reporting_register_option' ) );
		add_action( 'admin_init', array( __CLASS__, 'uo_reporting_activate_license' ) );
		add_action( 'admin_init', array( __CLASS__, 'uo_reporting_deactivate_license' ) );

	}


	public static function uo_reporting_plugin_updater() {

		// retrieve our license key from the DB
		$license_key = trim( get_option( 'uo_reporting_license_key' ) );

		// setup the updater
		$uo_updater = new EDD_SL_Plugin_Updater( UO_REPORTING_STORE_URL, UO_REPORTING_FILE, array(
			'version'   => UNCANNY_REPORTING_VERSION,                // current version number
			'license'   => $license_key,        // license key (used get_option above to retrieve from DB)
			'item_name' => UO_REPORTING_ITEM_NAME,    // name of this plugin
			'author'    => 'Uncanny Owl'  // author of this plugin
		) );

	}

// Licence options page
	public static function uo_reporting_license_menu() {
		add_submenu_page( 'uncanny-learnDash-reporting', 'Uncanny Reporting License Activation', 'License Activation', 'manage_options', 'uncanny-reporting-license-activation', array(
			__CLASS__,
			'uo_reporting_license_page'
		) );
	}

	public static function uo_reporting_license_page() {
		$license = get_option( 'uo_reporting_license_key' );
		$status  = get_option( 'uo_reporting_license_status' );
		?>
		<div class="uo-admin-reporting">

		<h1 style="text-align: left;" class="uo-admin-reporting-header">
			<a href="http://www.uncannyowl.com" target="_blank">
				<img src="<?php echo esc_url( Config::get_admin_media( 'Uncanny-Owl-logo.png' ) ); ?>"/>
			</a>
			<br>
			<span>Tin Canny LearnDash Reporting License Activation</span>
		</h1>

		<hr class="uo-underline">

		<h2><?php esc_html_e( 'Thanks for using Tin Canny LearnDash Reporting!', 'uncanny-learndash-toolkit' ); ?></h2>
		<br>

		<h3 style="font-weight: normal;"><?php _e( '1. Enter your license key and then click <strong>Save Changes</strong>.<br><br>2. Once the page is reloaded, click <strong>Activate License</strong>.', 'uncanny-learndash-toolkit' ); ?></h3>

		<form id="uo-reporting-activation-form" method="post" action="options.php">

			<?php settings_fields( 'uo_reporting_license' ); ?>

			<table class="form-table">
				<tbody>
				<tr valign="top">
					<th scope="row" valign="top">
						<?php _e( 'License Key' ); ?>
					</th>
					<td>
						<input id="uo_reporting_license_key" name="uo_reporting_license_key" type="text"
						       class="regular-text"
						       value="<?php esc_attr_e( $license ); ?>"/>
						<label class="description"
						       for="uo_reporting_license_key"><?php _e( 'Enter your license key' ); ?></label>
					</td>
				</tr>
				<?php if ( false !== $license ) { ?>
					<tr valign="top">
						<th scope="row" valign="top">
							<?php _e( 'Activate License' ); ?>
						</th>
						<td>
							<?php if ( $status !== false && $status == 'valid' ) { ?>
								<span
									class="uo-reporting-license-active"><?php _e( 'Your license is active.' ); ?></span>
								<?php wp_nonce_field( 'uo_reporting_nonce', 'uo_reporting_nonce' ); ?>
								<input type="submit" class="button-secondary" name="uo_reporting_license_deactivate"
								       value="<?php _e( 'Deactivate License' ); ?>"/>
							<?php } else {
								wp_nonce_field( 'uo_reporting_nonce', 'uo_reporting_nonce' ); ?>
								<input type="submit" class="button-secondary" name="uo_reporting_license_activate"
								       value="<?php _e( 'Activate License' ); ?>"/>
							<?php } ?>
						</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
			<?php submit_button(); ?>

		</form>
		<?php
	}

	public static function uo_reporting_register_option() {
		// creates our settings in the options table
		register_setting( 'uo_reporting_license', 'uo_reporting_license_key', array(
			__CLASS__,
			'uo_reporting_sanitize_license'
		) );
	}


	public static function uo_reporting_sanitize_license( $new ) {
		$old = get_option( 'uo_reporting_license_key' );
		if ( $old && $old != $new ) {
			delete_option( 'uo_reporting_license_status' ); // new license has been entered, so must reactivate
		}

		return $new;
	}


	/************************************
	 * this illustrates how to activate
	 * a license key
	 *************************************/

	public static function uo_reporting_activate_license() {

		// listen for our activate button to be clicked
		if ( isset( $_POST['uo_reporting_license_activate'] ) ) {

			// run a quick security check
			if ( ! check_admin_referer( 'uo_reporting_nonce', 'uo_reporting_nonce' ) ) {
				return;
			} // get out if we didn't click the Activate button

			// retrieve the license from the database
			$license = trim( get_option( 'uo_reporting_license_key' ) );


			// data to send in our API request
			$api_params = array(
				'edd_action' => 'activate_license',
				'license'    => $license,
				'item_name'  => urlencode( UO_REPORTING_ITEM_NAME ), // the name of our product in uo
				'url'        => home_url()
			);

			// Call the custom API.
			$response = wp_remote_post( UO_REPORTING_STORE_URL, array(
				'timeout'   => 15,
				'sslverify' => false,
				'body'      => $api_params
			) );

			// make sure the response came back okay
			if ( is_wp_error( $response ) ) {
				return false;
			}

			// decode the license data
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );

			// $license_data->license will be either "valid" or "invalid"

			update_option( 'uo_reporting_license_status', $license_data->license );

		}
	}


	/***********************************************
	 * Illustrates how to deactivate a license key.
	 * This will descrease the site count
	 ***********************************************/

	public static function uo_reporting_deactivate_license() {

		// listen for our activate button to be clicked
		if ( isset( $_POST['uo_reporting_license_deactivate'] ) ) {

			// run a quick security check
			if ( ! check_admin_referer( 'uo_reporting_nonce', 'uo_reporting_nonce' ) ) {
				return;
			} // get out if we didn't click the Activate button

			// retrieve the license from the database
			$license = trim( get_option( 'uo_reporting_license_key' ) );


			// data to send in our API request
			$api_params = array(
				'edd_action' => 'deactivate_license',
				'license'    => $license,
				'item_name'  => urlencode( UO_REPORTING_ITEM_NAME ), // the name of our product in uo
				'url'        => home_url()
			);

			// Call the custom API.
			$response = wp_remote_post( UO_REPORTING_STORE_URL, array(
				'timeout'   => 15,
				'sslverify' => false,
				'body'      => $api_params
			) );

			// make sure the response came back okay
			if ( is_wp_error( $response ) ) {
				return false;
			}

			// decode the license data
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );

			// $license_data->license will be either "deactivated" or "failed"
			if ( $license_data->license == 'deactivated' ) {
				delete_option( 'uo_reporting_license_status' );
			}

		}
	}


	/************************************
	 * this illustrates how to check if
	 * a license key is still valid
	 * the updater does this for you,
	 * so this is only needed if you
	 * want to do something custom
	 *************************************/

	public static function uo_reporting_check_license() {

		global $wp_version;

		$license = trim( get_option( 'uo_reporting_license_key' ) );

		$api_params = array(
			'edd_action' => 'check_license',
			'license'    => $license,
			'item_name'  => urlencode( UO_REPORTING_ITEM_NAME ),
			'url'        => home_url()
		);

		// Call the custom API.
		$response = wp_remote_post( UO_REPORTING_STORE_URL, array(
			'timeout'   => 15,
			'sslverify' => false,
			'body'      => $api_params
		) );

		if ( is_wp_error( $response ) ) {
			return false;
		}

		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		if ( $license_data->license == 'valid' ) {
			echo 'valid';
			exit;
			// this license is still valid
		} else {
			echo 'invalid';
			exit;
			// this license is no longer valid
		}
	}

	/**
	 *
	 *
	 * @static
	 *
	 * @param $class
	 */
	public static function auto_loader( $class ) {

		// Remove Class's namespace eg: my_namespace/MyClassName to MyClassName
		$class = str_replace( self::get_namespace(), '', $class );
		$class = str_replace( '\\', '', $class );

		// First Character of class name to lowercase eg: MyClassName to myClassName
		$class_to_filename = lcfirst( $class );

		// Split class name on upper case letter eg: myClassName to array( 'my', 'Class', 'Name')
		$split_class_to_filename = preg_split( '#([A-Z][^A-Z]*)#', $class_to_filename, null, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY );

		if ( 1 <= count( $split_class_to_filename ) ) {
			// Split class name to hyphenated name eg: array( 'my', 'Class', 'Name') to my-Class-Name
			$class_to_filename = implode( '-', $split_class_to_filename );
		}

		// Create file name that will be loaded from the classes directory eg: my-Class-Name to my-class-name.php
		$file_name = 'uncanny-reporting/' . strtolower( $class_to_filename ) . '.php';
		if ( file_exists( dirname( __FILE__ ) . '/' . $file_name ) ) {
			include_once $file_name;
		}

		$file_name = strtolower( $class_to_filename ) . '.php';
		if ( file_exists( dirname( __FILE__ ) . '/' . $file_name ) ) {
			include_once $file_name;
		}

	}
}





