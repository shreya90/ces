<?php

namespace uncanny_learndash_reporting;

use ReflectionClass;

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Class AdminMenu
 * @package uncanny_custom_reporting
 */
class ReportingAdminMenu extends Boot {
	private static $tincan_database;
	private static $tincan_opt_per_pages;

	/**
	 * class constructor
	 */
	public function __construct() {
		// Setup Theme Options Page Menu in Admin
		if ( is_admin() ) {
			// TinCan CSV
			self::csv_export();
			add_action( 'admin_init', array( __CLASS__, 'tincan_change_per_page' ) );

			add_action( 'admin_menu', array( __CLASS__, 'register_options_menu_page' ) );
			add_action( 'admin_init', array( __CLASS__, 'register_options_menu_page_settings' ) );
			add_action( 'admin_enqueue_scripts', array( __CLASS__, 'scripts' ) );

		}else{
			add_shortcode('tincanny', array( __CLASS__, 'options_menu_page_output' ) );
			add_action( 'wp_enqueue_scripts', array( __CLASS__, 'scripts' ) );

        }

	}

	/**
	 * Create Plugin options menu
	 */
	public static function register_options_menu_page() {

		$page_title = esc_html__( 'CES Reporting', 'uncanny-learndash-reporting' );
		$menu_title = esc_html__( 'Tin Canny Reporting', 'uncanny-learndash-reporting' );
		$capability = 'tincanny_reporting';
		$menu_slug  = 'uncanny-learnDash-reporting';
		$function   = array( __CLASS__, 'options_menu_page_output' );

		// Menu Icon blends into sidebar when the default admin color scheme is used
		$admin_color_scheme = get_user_meta( get_current_user_id(), 'admin_color', true );
		if ( 'fresh' === $admin_color_scheme ) {
			$icon_url = Config::get_admin_media( 'menu-icon-light.png' );
		} else {
			$icon_url = Config::get_admin_media( 'menu-icon.png' );
		}

		$position = 81; // 81 - Above Settings Menu
		add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );

	}

	/*
	 * Whitelisted Options that are saved on the page
	 */
	public static function register_options_menu_page_settings() {
		register_setting( 'uncanny_learndash_reporting-group', 'uncanny_reporting_active_classes' );
	}

	/**
	 * Populates an array of classes in internal and external file in the classes folder
	 *
	 * @param mixed (Array || false) $external_classes
	 *
	 * @return array
	 */
	public static function get_available_classes() {

		$class_details = Array();

		// loop file in classes folded and call get_details
		// check function exist first
		$path = dirname( __FILE__ ) . '/classes/';

		$files = scandir( $path );

		$internal_details = self::get_class_details( $path, $files, __NAMESPACE__ );

		$class_details = array_merge( $class_details, $internal_details );

		return $class_details;
	}

	/*
	 * get_class_details
	 * @param string $path
	 * @param array $files
	 * @param string $namespace
	 *
	 * @return array $details
	 */
	private static function get_class_details( $path, $files, $name_space ) {

		$details = array();

		foreach ( $files as $file ) {
			if ( is_dir( $path . $file ) || '..' === $file || '.' === $file ) {
				continue;
			}

			//get class name
			$class_name = str_replace( '.php', '', $file );
			$class_name = str_replace( '-', ' ', $class_name );
			$class_name = ucwords( $class_name );
			$class_name = $name_space . '\\' . str_replace( ' ', '', $class_name );

			// test for required functions
			$class = new ReflectionClass( $class_name );
			if ( $class->implementsInterface( 'uncanny_learndash_reporting\RequiredFunctions' ) ) {
				$details[ $class_name ] = $class_name::get_details();
			} else {
				$details[ $class_name ] = false;
			}
		}

		return $details;

	}

	/*
	 * Load Scripts
	 * @paras string $hook Admin page being loaded
	 */
	public static function scripts( $hook ) {

	    if( function_exists('get_current_screen')){
		    $screen = get_current_screen();
        }else{
		    $screen = '';
        }


		global $post;


		if ( 'toplevel_page_uncanny-learnDash-reporting' === $hook ||
             'tin-canny-reporting_page_snc_options' === $hook ||
                ( ! is_admin()
                  && $post instanceof \WP_Post
                  && has_shortcode( $post->post_content, 'tincanny' )
                )
        ) {

			// Admin Page Reporting UI Files

			// Admin CSS
			// TODO add debug and minify
			wp_enqueue_style( 'reporting-admin', Config::get_admin_css( 'admin-style.css' ) );
			wp_enqueue_style( 'data-tables', Config::get_admin_css( 'datatables.min.css' ) );
			wp_enqueue_style( 'uo-menu-slug-css-fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' );

			// Admin JS
			wp_register_script( 'reporting_js_handle', Config::get_admin_js( 'reporting' ), array( 'jquery' ), false, true );

			if ( isset( $_GET['group_id'] ) ) {
				$isolated_group_id = absint( $_GET['group_id'] );
			} else {
				$isolated_group_id = 0;
			}

			// API data
			$reporting_api_setup = array(
				'root'              => esc_url_raw( rest_url() . 'uncanny_reporting/v1/' ),
				'nonce'             => \wp_create_nonce( 'wp_rest' ),
				'learnDashLabels'   => ReportingApi::get_labels(),
				'isolated_group_id' => $isolated_group_id,
                'isAdmin'          => is_admin(),
                'editUsers'        => current_user_can('edit_users')
			);

			wp_localize_script( 'reporting_js_handle', 'reportingApiSetup', $reporting_api_setup );
			wp_enqueue_script( 'reporting_js_handle' );

			// TinCan
			wp_enqueue_script( 'jquery-ui-datepicker' );
			wp_enqueue_style( 'jquery-style', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css' );

			wp_enqueue_style( 'tincanny-admin-tincanny-report-tab', Config::get_gulp_css( 'admin.tincan.report.tab' ) );
			// TODO remove if no issues
			//wp_enqueue_script( 'unTinCanAdmin', UCTINCAN_PLUGIN_URL . 'assets/dist/admin.table-min.js' );


		} else if ( 'tin-canny-reporting_page_uncanny-reporting-license-activation' === $hook ) {

			// Admin Page Licensing UI Files

			// Load Styles for Licensing page located in general plugin styles
			wp_enqueue_style( 'unTinCanAdmin', UCTINCAN_PLUGIN_URL . 'assets/css/admin.css' );

		} elseif ( ! empty($screen) && 'dashboard' === $screen->id ) {

			// Add HTML Element for JS to hook into
			add_action( 'welcome_panel', array( __CLASS__, 'show_uc_reporting_panel' ) );

			// WP Dashboard Reporting UI Files

			// Load Styles for WP Dashboard Reporting  page located in general plugin styles
			wp_enqueue_style( 'reporting-admin', Config::get_admin_css( 'admin-style.css' ) );

			wp_register_script( 'reporting_js_handle', Config::get_admin_js( 'tc-reporting-dashboard' ), array( 'jquery' ), false, true );
			$reporting_api_setup = array(
				'root'            => esc_url_raw( rest_url() . 'uncanny_reporting/v1/' ),
				'nonce'           => \wp_create_nonce( 'wp_rest' ),
				'learnDashLabels' => ReportingApi::get_labels()
			);
			wp_localize_script( 'reporting_js_handle', 'reportingApiSetup', $reporting_api_setup );
			wp_enqueue_script( 'reporting_js_handle' );

		}


	}

	/*
	 * Adds a simple element so we can hook html/ajax JS Reporting charts in wp-admin dashboard.
	 */
	public static function show_uc_reporting_panel() {
		echo '<div id="uo-welcome-panel"></div>';
	}

	/**
	 * Create Theme Options page
	 */
	public static function options_menu_page_output() {

		if( ! is_user_logged_in() ){
			return esc_html__( 'You must be logged in to view this report.', 'uncanny-learndash-reporting' );
		}

		// Get LD Labels
		$course_label      = \LearnDash_Custom_Label::get_label( 'course' );
		$courses_label     = \LearnDash_Custom_Label::get_label( 'courses' );
		$lessons_label     = \LearnDash_Custom_Label::get_label( 'lessons' );
		$topics_label      = \LearnDash_Custom_Label::get_label( 'topics' );
		$quizzes_label     = \LearnDash_Custom_Label::get_label( 'quizzes' );
		$assignments_label = 'Assignments';

		// Options - Restrict for group leader
		if ( current_user_can( 'manage_options' ) ) {
			$show_tincan           = get_option( 'show_tincan_reporting_tables', 'yes' );
			$disable_mark_complete = get_option( 'disable_mark_complete_for_tincan', 'yes' );
			$nonce_protection      = get_option( 'tincanny_nonce_protection', 'yes' );

			$tincan_style     = '';
			$tincan_yes_check = 'checked';
			$tincan_no_check  = '';
			if ( 'no' === $show_tincan ) {
				$tincan_style     = 'style="display: none;"';
				$tincan_yes_check = '';
				$tincan_no_check  = 'checked';
			}
		}

		$current_user_ID = get_current_user_id();
        $groups          = learndash_get_groups( true, $current_user_ID );

		$isolated_group  = 0;
		if ( isset( $_GET['group_id'] ) ) {
			$isolated_group = absint( $_GET['group_id'] );
		}

		?>
        <div class="uo-admin-reporting">

            <h1 class="uo-admin-reporting-header">
                <a href="http://www.uncannyowl.com" target="_blank">
                    <img src="<?php echo esc_url( Config::get_admin_media( 'logo_ces.png' ) ); ?>"/>
                </a>
                <br>
                 <br>
                <span>CES Reporting</span>
            </h1>

            <h3 id="failed-response"></h3>

			<?php if ( ! empty( $groups ) && 1 !== count( $groups )) { ?>
                <div style="padding: 6px;position: absolute;right: 15px;">
                    <form method="get">
                        <?php if( is_admin() ){ ?>
                        <input type="hidden" name="page" value="<?php echo htmlspecialchars( $_GET['page'] ); ?>">
                        <?php } ?>
                        <select name="group_id">
                            <option value="all">All Groups</option>
							<?php
                            foreach ( $groups as $group_id ) {
                                $group = get_post($group_id);

							    ?>
                                <option <?php if ( $group->ID === $isolated_group ) {
									echo 'selected="selected"';
								} ?> value="<?php echo $group->ID; ?>"><?php echo $group->post_title; ?></option>
							<?php } ?>
                        </select>
                        <input value=">"
                               style="border: 2px solid #29779e;background: #29779e;font-weight: bold;font-size: 12px;color: white;"
                               type="submit">
                    </form>
                </div>
			<?php } ?>

            <div id="reporting-loader" class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>

            <ul class="uo-admin-reporting-tabs clearfix" data-tabgroup="first-tab-group">
                <li><a href="#courseReportTab" class="active"><?php echo $course_label; ?> Report</a></li>
                <li><a href="#userReportTab">User Report</a></li>
                <?php if ( is_admin() ) { ?><li><a <?php echo $tincan_style; ?> href="#tin-can">CES Report</a></li><?php } ?>
            </ul>

            <section id="first-tab-group" class="uo-admin-reporting-tabgroup">

                <div class="uo-admin-reporting-tab-single" id="courseReportTab">

                    <button id="course-navigate-link" data-target-last=""></button>
                    <h1 id="courseSingleTitle"><span></span></h1>
                    <div id="coursesOverviewContainer">
                        <h2 id="coursesOverviewGraphHeading">Overview</h2>
                        <div id="coursesOverviewGraph"></div>
                        <h2 class="uo-reporting-space"
                            id="coursesOverviewTableHeading"><?php echo $courses_label; ?></h2>
                        <table id="coursesOverviewTable" class="display reporting-table-selectable"></table>
                    </div>
                    <div id="courseSingleContainer" style="display: none;">

                        <div id="courseSingleNav"></div>

                        <div id="courseSingleOverviewContainer">
                            <h2 id="courseSingleOverviewTitle"><?php echo $course_label; ?> Overview</h2>
                            <table id="courseSingleOverviewSummaryTable" class="display"></table>
                            <div id="courseSingleOverviewPieChart"></div>
                        </div>
                        <div id="courseSingleActivitiesContainer">
                            <div id="courseSingleActivitiesTitle"></div>
                            <h2 id="courseSingleOverviewTitle"><?php echo $course_label; ?> Completions</h2>

                            <div id="courseSingleActivitiesSearch" class="display"></div>
                            <div id="courseSingleActivitiesGraph"></div>
                        </div>
                        <h2 class="uo-reporting-space" id="courseSingleTableHeading">Enrolled Users</h2>
                        <table id="courseSingleTable" class="display  reporting-table-selectable"></table>

                    </div>

                </div>

                <div class="uo-admin-reporting-tab-single" id="userReportTab">

                    <button id="user-navigate-link" data-target-last=""></button>
                    <h1 id="userCourseSingleTitle"><span></span></h1>
                    <div id="usersOverviewContainer">
                        <h2 id="usersOverviewTableHeading">Users</h2>
                        <table id="usersOverviewTable" class="display"></table>
                    </div>

                    <div id="singleUserProfileContainer" style="display: none;">
                        <h2 id="singleUserProfileHeading">Profile</h2>
                        <h3 id="singleUserProfileDisplayName"></h3>
                        <p>
                            ID: <span id="singleUserProfileID"></span><br>
                            Email: <span id="singleUserProfileEmail"></span>
                        </p>
                    </div>

                    <div id="userSingleOverviewContainer" style="display: none;">
                        <h2 id="usersSingleOverviewTableHeading">Overview</h2>
                        <table id="userSingleOverviewTable" class="display"></table>
                    </div>

                    <div id="userSingleCourseProgressSummaryContainer" style="display: none;">
                        <h2 id="userSingleCourseProgressSummaryTableHeading">Progress Summary</h2>
                        <table id="userSingleCourseProgressSummaryTable" class="display"></table>
                    </div>

                    <div id="userSingleCoursesOverviewContainer" style="display: none;">
                        <h2 class="uo-reporting-space" id="userSingleCoursesOverviewTableHeading">
                            User's <?php echo $course_label; ?> List</h2>
                        <table id="userSingleCoursesOverviewTable" class="display  reporting-table-selectable"></table>
                    </div>

                    <div id="userSingleCourseProgressMenuContainer" style="display: none;">
                        <h2 id="userSingleCourseProgressMenuHeading">Activities</h2>
                        <ul id="userSingleCourseProgressMenu">
                            <li id="menuLessons"><?php echo $lessons_label; ?></li>
                            <li id="menuTopics"><?php echo $topics_label; ?></li>
                            <li id="menuQuizzes"><?php echo $quizzes_label; ?></li>
                            <li id="menuAssignments"><?php echo $assignments_label; ?></li>
                            <li <?php echo $tincan_style; ?> id="menuTinCan">CES</li>
                        </ul>
                    </div>

                    <div id="userSingleCourseLessonsContainer" style="display: none;">
                        <h2 id="userSingleCourseLessonsTableHeading"><?php echo $lessons_label; ?></h2>
                        <table id="userSingleCourseLessonsTable" class="display  reporting-table-selectable"></table>
                    </div>

                    <div id="userSingleCourseTopicsContainer" style="display: none;">
                        <h2 id="userSingleCourseTopicsTableHeading"><?php echo $topics_label; ?></h2>
                        <table id="userSingleCourseTopicsTable" class="display  reporting-table-selectable"></table>
                    </div>

                    <div id="userSingleCourseQuizzesContainer" style="display: none;">
                        <h2 id="userSingleCourseQuizzesTableHeading"><?php echo $quizzes_label; ?></h2>
                        <table id="userSingleCourseQuizzesTable" class="display  reporting-table-selectable"></table>
                    </div>

                    <div id="userSingleCourseAssignmentsContainer" style="display: none;">
                        <h2 id="userSingleCourseAssignmentsTableHeading"><?php echo $assignments_label; ?></h2>
                        <table id="userSingleCourseAssignmentsTable"
                               class="display  reporting-table-selectable"></table>
                    </div>

                    <div id="userSingleCourseTinCanContainer" style="display: none;">
                        <h2 id="userSingleCourseTinCanTableHeading">CES</h2>
                        <table id="userSingleCourseTinCanTable" class="display  reporting-table-selectable"></table>
                    </div>

                </div>

	            <?php if ( is_admin() ) { ?>

                <div class="uo-admin-reporting-tab-single" id="tin-can">
					<?php self::show_tincan_list_table(); ?>
                </div>

	            <?php } ?>
            </section>


        </div>
		<?php

	}


	/*
	 * Add add-ons to options page
	 *
	 * @param Array() $classes_available
	 * @param Array() $active_classes
	 *
	 */
	public static function create_features( $classes_available, $active_classes ) {

		/* If Magic Quotes are enable we need to stripslashes from ouw $active classes */
		if ( function_exists( 'get_magic_quotes_gpc' ) ) {
			if ( get_magic_quotes_gpc() ) {
				//strip slashes from all keys in array
				$active_classes = Config::stripslashes_deep( $active_classes );
			}
		}


		// Sort add ons alphabetically by title
		$add_on_titles = array();
		foreach ( $classes_available as $key => $row ) {
			$add_on_titles[ $key ] = $row['title'];
		}
		array_multisort( $add_on_titles, SORT_ASC, $classes_available );

		foreach ( $classes_available as $key => $class ) {

			// skip sample classes
			if ( 'uncanny_learndash_reporting\Sample' === $key || 'uncanny_custom_reporting\Sample' === $key || 'uncanny_pro_reporting\Sample' === $key ) {
				continue;
			}

			if ( false === $class ) {
				?>
                <div class="uo_feature">
                    <div class="uo_feature_title"><?php echo esc_html( $key ) ?></div>
                    <div class="uo_feature_description"><?php
						esc_html_e( 'This class is not configured properly. Contact Support for assistance.', 'uncanny-learndash-reporting' );
						?></div>
                </div>
				<?php
				continue;
			}

			$dependants_exist = $class['dependants_exist'];

			$is_activated = 'uo_feature_deactivated';
			$class_name   = $key;
			if ( isset( $active_classes[ $class_name ] ) ) {
				$is_activated = 'uo_feature_activated';
			}
			if ( true !== $dependants_exist ) {
				$is_activated = 'uo_feature_needs_dependants';
			}

			$icon = '<div class="uo_icon"></div>';
			if ( $class['icon'] ) {
				$icon = $class['icon'];
			}


			if ( ! isset( $class['settings'] ) || false === $class['settings'] ) {
				$class['settings']['modal'] = '';
				$class['settings']['link']  = '';
			}


			?>

			<?php // Setting Modal Popup
			echo $class['settings']['modal']; ?>

            <div class="uo_feature">

				<?php // Settings Modal Popup trigger
				echo $class['settings']['link']; ?>

                <div class="uo_feature_title">

					<?php echo $class['title']; ?>

					<?php
					// Link to KB for Feature
					if ( null !== $class['kb_link'] ) {
						?>
                        <a class="uo_feature_more_info" href="<?php echo $class['kb_link']; ?>" target="_blank">
                            <i class="fa fa-question-circle"></i>
                        </a>
					<?php } ?>

                </div>

                <div class="uo_feature_description"><?php echo $class['description']; ?></div>
                <div class="uo_icon_container"><?php echo $icon; ?></div>
                <div class="uo_feature_button <?php echo $is_activated; ?>">

					<?php
					if ( true !== $dependants_exist ) {
						echo '<div><strong>' . esc_html( $dependants_exist ) . '</strong>' . esc_html__( ' is needed for this add-on', 'uncanny-learndash-reporting' ) . '</div>';
					} else {
						?>
                        <div class="uo_feature_button_toggle"></div>
                        <label class="uo_feature_label" for="<?php echo esc_attr( $class_name ) ?>">
							<?php echo( esc_html__( 'Activate ', 'uncanny-learndash-reporting' ) . $class['title'] ); ?>
                        </label>
                        <input class="uo_feature_checkbox" type="checkbox" id="<?php echo esc_attr( $class_name ); ?>"
                               name="uncanny_reporting_active_classes[<?php echo esc_attr( $class_name ) ?>]"
                               value="<?php echo esc_attr( $class_name ) ?>" <?php
						if ( array_key_exists( $class_name, $active_classes ) ) {
							// Some wp installs remove slashes during db calls, being extra safe when comparing DB vs php values with stripslashes
							checked( stripslashes( $active_classes[ $class_name ] ), stripslashes( $class_name ), true );
						}
						?>
                        />
					<?php } ?>
                </div>
            </div>
			<?php
		}
	}

	/*
	 * Check for Adds that are located in other UO plugins
	 *@param string $uo_plugin
	 *
	 *return mixed(false || String)
	*/
	private static function check_for_other_uo_plugin_classes( $uo_plugin ) {

		// plugins dir
		$directory_contents = scandir( WP_PLUGIN_DIR );

		// loop through all contents
		foreach ( $directory_contents as $content ) {

			// exclude parent directories
			if ( $content !== '.' or $content !== '..' ) {

				// create absolute path
				$plugin_dir = WP_PLUGIN_DIR . '/' . $content;

				if ( is_dir( $plugin_dir ) ) {

					if ( 'pro' === $uo_plugin ) {
						if ( 'uo-plugin-pro' === $content || 'uncanny-reporting-pro' === $content ) {
							// Check if plugin is active
							if ( is_plugin_active( $content . '/uncanny-reporting-pro.php' ) ) {
								return $plugin_dir . '/src/classes/';
							}
						}
					}

					if ( 'custom' === $uo_plugin ) {

						$explode_directory = explode( '-', $content );
						if ( 3 === count( $explode_directory ) ) {
							// custom plugin directory is may be prefixed with client name
							// check suffix uo-custom-plugin
							if ( in_array( 'uo', $explode_directory ) && in_array( 'custom', $explode_directory ) && in_array( 'plugin', $explode_directory ) ) {

								// Check if plugin is active
								if ( is_plugin_active( $content . '/uncanny-reporting-custom.php' ) ) {
									return $plugin_dir . '/src/classes/';
								}

							}

							if ( 'uncanny-reporting-custom' === $content ) {

								// Check if plugin is active
								if ( is_plugin_active( $content . '/uncanny-reporting-custom.php' ) ) {
									return $plugin_dir . '/src/classes/';
								}

							}
						}

					}

				}

			}
		}

		return false;

	}

	private static function csv_export() {
		if ( isset( $_GET['tc_filter_mode'] ) && ! empty( $_GET['tc_filter_mode'] ) && $_GET['tc_filter_mode'] == 'csv' ) {
			add_action( 'admin_init', array( __CLASS__, 'execute_csv_export' ) );
		}
	}

	public static function execute_csv_export() {
		include_once( dirname( __FILE__ ) . '/uncanny-tincan/uncanny-tincan.php' );

		self::$tincan_database = new \UCTINCAN\Database\Admin();

		self::SetTcFilters();
		self::SetOrder();

		$data = self::$tincan_database->get_data( 0, 'csv' );

		new \UCTINCAN\Admin\CSV( $data );
	}

	public static function tincan_change_per_page() {
		if ( isset( $_GET['per_page'] ) && ! empty( $_GET['per_page'] ) ) {
			update_user_meta( get_current_user_id(), 'ucTinCan_per_page', $_GET['per_page'] );
		}
	}

	private static function show_tincan_list_table() {



		self::$tincan_database      = new \UCTINCAN\Database\Admin();
		self::$tincan_opt_per_pages = get_user_meta( get_current_user_id(), 'ucTinCan_per_page', true );
		self::$tincan_opt_per_pages = ( self::$tincan_opt_per_pages ) ? self::$tincan_opt_per_pages : 25;

		if( ! is_admin() ){

			if(file_exists(dirname( __FILE__ ) . '/includes/TinCan_List_Table.php')){
				echo'<script>console.log("I ecist")</script>';
				//echo'<script>console.log("'.ABSPATH . 'wp-admin/includes/class-wp-list-table.php'.'")</script>';
			}

			echo '</pre>';

			require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
			require_once( ABSPATH . 'wp-admin/includes/screen.php' );
			require_once( ABSPATH . 'wp-admin/includes/class-wp-screen.php' );
			require_once( ABSPATH . 'wp-admin/includes/template.php' );

		}



		include_once( dirname( __FILE__ ) . '/includes/TinCan_List_Table.php' );

		$TinCan_List_Table = new \TinCan_List_Table();

		$coulmns = array(
			'Group',
			'User',
			\LearnDash_Custom_Label::get_label( 'course' ),
			'Module',
			'Target',
			'Action',
			'Result',
			'Success',
			'Date Time',
		);

		$TinCan_List_Table->sortable_columns = $coulmns;
		$TinCan_List_Table->column           = $coulmns;

		$TinCan_List_Table->data           = array( __CLASS__, 'patchData' );
		$TinCan_List_Table->count          = array( __CLASS__, 'patchNumRows' );
		$TinCan_List_Table->per_page       = self::$tincan_opt_per_pages;
		$TinCan_List_Table->extra_tablenav = array( __CLASS__, 'ExtraTableNav' );


		$TinCan_List_Table->prepare_items();
		$TinCan_List_Table->views();

		$TinCan_List_Table->display();
	}

	public static function patchData() {
		$data              = array();
		$tincan_post_types = array(
			'sfwd-courses',
			'sfwd-lessons',
			'sfwd-topic',
			'sfwd-quiz',
			'sfwd-certificates',
			'sfwd-assignment',
			'groups'
		);

		self::SetOrder();
		self::$tincan_database->paged = isset( $_GET["paged"] ) ? $_GET["paged"] : 1;

		if ( isset( $_GET['tc_filter_mode'] ) && ! empty( $_GET['tc_filter_mode'] ) && $_GET['tc_filter_mode'] == 'list' ) {
			self::SetTcFilters();

			$data = self::$tincan_database->get_data( self::$tincan_opt_per_pages );
		}

		foreach ( $data as &$row ) {
			$lesson = get_post( $row['lesson_id'] );

			if ( in_array( $lesson->post_type, $tincan_post_types ) ) {
				$group_link = admin_url( "post.php?post={$row[ 'group_id' ]}&action=edit" );
				$group_name = $row['group_name'];
				$group      = sprintf( '<a href="%s">%s</a>', $group_link, $group_name );

				$course_link = admin_url( "post.php?post={$row[ 'course_id' ]}&action=edit" );
				$course_name = $row['course_name'];
				$course      = sprintf( '<a href="%s">%s</a>', $course_link, $course_name );
			} else {
				$group  = 'n/a';
				$course = 'n/a';
			}

			$row[ __( 'Group' ) ]                                  = $group;
			$row[ __( 'User' ) ]                                   = sprintf( '<a href="%s">%s</a>', admin_url( "user-edit.php?user_id={$row[ 'user_id' ]}" ), $row['user_name'] );
			$row[ \LearnDash_Custom_Label::get_label( 'course' ) ] = $course;
			$row[ __( 'Module' ) ]                                 = sprintf( '<a href="%s">%s</a>', $row['module'], $row['module_name'] );
			$row[ __( 'Target' ) ]                                 = sprintf( '<a href="%s">%s</a>', $row['target'], $row['target_name'] );
			$row[ __( 'Action' ) ]                                 = ucfirst( $row['verb'] );

			$result = $row['result'];

			if ( ! is_null( $row['result'] ) && $row['minimum'] ) {
				$result = $row['result'] . ' / ' . $row['minimum'];
			}

			$completion = false;

			if ( ! is_null( $row['completion'] ) ) {
				$completion = ( $row['completion'] ) ? '<span class="dashicons dashicons-yes"></span>' : '<span class="dashicons dashicons-no"></span>';
			}

			$row[ __( 'Result' ) ]    = $result;
			$row[ __( 'Success' ) ]   = $completion;
			$row[ __( 'Date Time' ) ] = $row['stored'];
			$row                      = apply_filters( 'tincanny_row_data', $row );
		}

		return $data;
	}

	// Number of Data
	public static function patchNumRows() {
		if ( isset( $_GET['tc_filter_mode'] ) && ! empty( $_GET['tc_filter_mode'] ) && $_GET['tc_filter_mode'] == 'list' ) {
			return self::$tincan_database->get_count();
		}

		return 0;
	}

	private static function SetOrder() {
		self::$tincan_database->orderby = "stored";
		self::$tincan_database->order   = 'desc';

		if ( ! empty( $_GET["orderby"] ) ) {
			switch ( $_GET["orderby"] ) {
				case 'group' :
				case 'user' :
				case 'course' :
					self::$tincan_database->orderby = $_GET["orderby"] . '_id';
					break;

				case 'action' :
					self::$tincan_database->orderby = 'verb';
					break;

				case 'date-time' :
					self::$tincan_database->orderby = 'stored';
					break;
			}

			self::$tincan_database->order = ( ! empty( $_GET["order"] ) ) ? $_GET["order"] : 'desc';
		}
	}

	private static function SetTcFilters() {
		// Group
		if ( isset( $_GET['tc_filter_group'] ) && ! empty( $_GET['tc_filter_group'] ) ) {
			self::$tincan_database->group = $_GET['tc_filter_group'];
		}

		// Actor
		if ( isset( $_GET['tc_filter_user'] ) && ! empty( $_GET['tc_filter_user'] ) ) {
			self::$tincan_database->actor = $_GET['tc_filter_user'];
		}

		// Course
		if ( isset( $_GET['tc_filter_course'] ) && ! empty( $_GET['tc_filter_course'] ) ) {
			self::$tincan_database->course = $_GET['tc_filter_course'];
		}

		// Lesson
		if ( isset( $_GET['tc_filter_lesson'] ) && ! empty( $_GET['tc_filter_lesson'] ) ) {
			self::$tincan_database->lesson = $_GET['tc_filter_lesson'];
		}

		// Module
		if ( isset( $_GET['tc_filter_module'] ) && ! empty( $_GET['tc_filter_module'] ) ) {
			self::$tincan_database->module = $_GET['tc_filter_module'];
		}

		// Verb
		if ( isset( $_GET['tc_filter_action'] ) && ! empty( $_GET['tc_filter_action'] ) ) {
			self::$tincan_database->verb = strtolower( $_GET['tc_filter_action'] );
		}

		// Date
		if ( isset( $_GET['tc_filter_date_range'] ) && ! empty( $_GET['tc_filter_date_range'] ) ) {
			switch ( $_GET['tc_filter_date_range'] ) {
				case 'last' :
					if ( isset( $_GET['tc_filter_date_range_last'] ) && ! empty( $_GET['tc_filter_date_range_last'] ) ) {
						$current_time = current_time( 'timestamp' );

						switch ( $_GET['tc_filter_date_range_last'] ) {
							case 'week' :
								self::$tincan_database->dateEnd   = date( 'Y-m-d ', $current_time ) . '24:00:00';
								$dateStart                        = strtotime( 'last week', $current_time );
								self::$tincan_database->dateStart = date( 'Y-m-d ', $dateStart );
								break;

								break;
							case 'month' :
								self::$tincan_database->dateEnd   = date( 'Y-m-d ', $current_time ) . '24:00:00';
								$dateStart                        = strtotime( 'first day of last month', $current_time );
								self::$tincan_database->dateStart = date( 'Y-m-d ', $dateStart );
								break;

							case '90days' :
								self::$tincan_database->dateEnd   = date( 'Y-m-d ', $current_time ) . '24:00:00';
								$dateStart                        = strtotime( '-90 days', $current_time );
								self::$tincan_database->dateStart = date( 'Y-m-d ', $dateStart );
								break;
							case '3months' :
								self::$tincan_database->dateEnd   = date( 'Y-m-d ', $current_time ) . '24:00:00';
								$dateStart                        = strtotime( '-3 months', $current_time );
								self::$tincan_database->dateStart = date( 'Y-m-d ', $dateStart );
								break;
							case '6months' :
								self::$tincan_database->dateEnd   = date( 'Y-m-d ', $current_time ) . '24:00:00';
								$dateStart                        = strtotime( '-6 months', $current_time );
								self::$tincan_database->dateStart = date( 'Y-m-d ', $dateStart );
								break;
						}
					}
					break;
				case 'from' :
					if ( isset( $_GET['tc_filter_start'] ) && ! empty( $_GET['tc_filter_start'] ) ) {
						self::$tincan_database->dateStart = $_GET['tc_filter_start'];
					}

					if ( isset( $_GET['tc_filter_end'] ) && ! empty( $_GET['tc_filter_end'] ) ) {
						self::$tincan_database->dateEnd = $_GET['tc_filter_end'] . ' 24:00:00';
					}
					break;
			}
		}
	}

	public static function ExtraTableNav( $which ) {
		switch ( $which ) {
			case 'top' :
				self::ExtraTableNavTop();
				break;
			case 'bottom' :
				self::ExtraTableNavBottom();
				break;
		}
	}

	//! Search Box
	private static function ExtraTableNavTop() {
		// Group
		$ld_groups = self::$tincan_database->get_groups();

		// Courses
		$ld_courses = self::$tincan_database->get_courses();

		// Actions
		$ld_actions = self::$tincan_database->get_actions();

		?>
        <div class="tincan-filters tablenav">
            <form action="<?php echo remove_query_arg( 'paged' ) ?>" id="tincan-filters-top">
                <input type="hidden" name="page" value="<?php echo ! empty( $_GET['page'] ) ? $_GET['page'] : 1 ?>"/>
                <input type="hidden" name="tc_filter_mode" value="list"/>

                <input type="hidden" name="orderby"
                       value="<?php echo ! empty( $_GET['orderby'] ) ? $_GET['orderby'] : 'date-time' ?>"/>
                <input type="hidden" name="order"
                       value="<?php echo ! empty( $_GET['order'] ) ? $_GET['order'] : 'desc' ?>"/>

                <fieldset class="ucTinCan">
                    <legend>Filters</legend>

                    <!-- User & Group -->
                    <div>
                        <h3>User & Group</h3>

                        <label for="tc_filter_group">LearnDash Group: </label>
                        <select name="tc_filter_group" id="tc_filter_group">
                            <option value="">All Groups</option>
							<?php foreach ( $ld_groups as $group ) { ?>
                                <option value="<?php echo $group['group_id'] ?>" <?php echo ( ! empty( $_GET['tc_filter_group'] ) && $_GET['tc_filter_group'] == $group['group_id'] ) ? 'selected="selected"' : '' ?>><?php echo $group['group_name'] ?></option>
							<?php } // foreach( $ld_groups ) ?>
                        </select>

                        <br/>

                        <label for="tc_filter_user">User: </label>
                        <input name="tc_filter_user" id="tc_filter_user" placeholder="User"
                               value="<?php echo ! empty( $_GET['tc_filter_user'] ) ? $_GET['tc_filter_user'] : '' ?>"/>
                    </div>

                    <!-- Content -->
                    <div>
                        <h3>Content</h3>

                        <label for="tc_filter_course"><?php echo \LearnDash_Custom_Label::get_label( 'course' ); ?>
                            : </label>
                        <select name="tc_filter_course" id="tc_filter_course">
                            <option value="">All <?php echo \LearnDash_Custom_Label::get_label( 'courses' ); ?></option>
							<?php foreach ( $ld_courses as $course ) { ?>
                                <option value="<?php echo $course['course_id'] ?>" <?php echo ( ! empty( $_GET['tc_filter_course'] ) && $_GET['tc_filter_course'] == $course['course_id'] ) ? 'selected="selected"' : '' ?>><?php echo $course['course_name'] ?></option>
							<?php } // foreach( $ld_courses ) ?>
                        </select>

                        <br/>

                        <label for="tc_filter_module">Module: </label>
                        <select name="tc_filter_module" id="tc_filter_module">
                            <option value="">All Modules</option>
							<?php self::$tincan_database->print_modules_form_from_URL_parameter() ?>
                        </select>
                    </div>

                    <!-- Activity -->
                    <div>
                        <h3>Activity</h3>

                        <label for="tc_filter_action">Action: </label>
                        <select name="tc_filter_action" id="tc_filter_action">
                            <option value="">All Actions</option>
							<?php foreach ( $ld_actions as $action ) { ?>
                                <option value="<?php echo $action['verb'] ?>" <?php echo ( ! empty( $_GET['tc_filter_action'] ) && strtolower( $_GET['tc_filter_action'] ) == $action['verb'] ) ? 'selected="selected"' : '' ?>><?php echo ucfirst( $action['verb'] ) ?></option>
							<?php } // foreach( $ld_groups ) ?>
                        </select>
                    </div>

                    <!-- Date Range -->
                    <div id="tc_filter_date_range">
                        <h3>Date Range</h3>

                        <label><input name="tc_filter_date_range" value="last"
                                      type="radio" <?php echo ( empty( $_GET['tc_filter_date_range'] ) || $_GET['tc_filter_date_range'] == 'last' ) ? 'checked="checked"' : '' ?> />View
                            :</label>
                        <select name="tc_filter_date_range_last" id="tc_filter_date_range_last">
                            <option value="all" <?php echo ( ! empty( $_GET['tc_filter_date_range'] ) && $_GET['tc_filter_date_range_last'] == 'all' ) ? 'selected="selected"' : '' ?>>
                                All Dates
                            </option>
                            <option value="week" <?php echo ( ! empty( $_GET['tc_filter_date_range'] ) && $_GET['tc_filter_date_range_last'] == 'week' ) ? 'selected="selected"' : '' ?>>
                                Last Week
                            </option>
                            <option value="month" <?php echo ( ! empty( $_GET['tc_filter_date_range'] ) && $_GET['tc_filter_date_range_last'] == 'month' ) ? 'selected="selected"' : '' ?>>
                                Last Month
                            </option>
                            <option value="90days" <?php echo ( ! empty( $_GET['tc_filter_date_range'] ) && $_GET['tc_filter_date_range_last'] == '90days' ) ? 'selected="selected"' : '' ?>>
                                Last 90 Days
                            </option>
                            <option value="3months" <?php echo ( ! empty( $_GET['tc_filter_date_range'] ) && $_GET['tc_filter_date_range_last'] == '3months' ) ? 'selected="selected"' : '' ?>>
                                Last 3 Months
                            </option>
                            <option value="6months" <?php echo ( ! empty( $_GET['tc_filter_date_range'] ) && $_GET['tc_filter_date_range_last'] == '6months' ) ? 'selected="selected"' : '' ?>>
                                Last 6 Months
                            </option>
                        </select>

                        <br/>

                        <label><input name="tc_filter_date_range" value="from"
                                      type="radio" <?php echo ( ! empty( $_GET['tc_filter_date_range'] ) && $_GET['tc_filter_date_range'] == 'from' ) ? 'checked="checked"' : '' ?> />From
                            :</label>
                        <input class="datepicker" name="tc_filter_start" placeholder="Start Date"
                               value="<?php echo ( ! empty( $_GET['tc_filter_start'] ) ) ? $_GET['tc_filter_start'] : '' ?>"/>
                        <span class="dashicons dashicons-calendar-alt"></span>

                        <br/>

                        <input class="datepicker" name="tc_filter_end" placeholder="End Date"
                               value="<?php echo ( ! empty( $_GET['tc_filter_end'] ) ) ? $_GET['tc_filter_end'] : '' ?>"/>
                        <span class="dashicons dashicons-calendar-alt"></span>

                        <br/>
                        <br/>

						<?php submit_button( __( 'Search' ), 'primary', '', false, array( 'id' => "do_tc_filter" ) ); ?>
                        <a href="<?php echo remove_query_arg( array(
							'paged',
							'tc_filter_mode',
							'tc_filter_group',
							'tc_filter_user',
							'tc_filter_course',
							'tc_filter_lesson',
							'tc_filter_module',
							'tc_filter_action',
							'tc_filter_date_range',
							'tc_filter_date_range_last',
							'tc_filter_start',
							'tc_filter_end',
							'orderby',
							'order'
						) ) ?>" class="button">Reset</a>
                    </div>
                </fieldset>
            </form>
        </div>

        <div class="clear"></div>

        <script>
            jQuery(document).ready(function ($) {
                $('.datepicker').datepicker({
                    'dateFormat': 'yy-mm-dd'
                });

                $('.dashicons-calendar-alt').click(function () {
                    $(this).prev().focus();
                });
            });
        </script>
		<?php
	}

	private static function ExtraTableNavBottom() {
		$per_pages = array(
			10,
			25,
			50,
			100,
			200,
			self::$tincan_opt_per_pages
		);

		$per_pages = array_unique( $per_pages );
		asort( $per_pages );

		?>
        <div id="tincan-filters-per_page">
            <select>
				<?php foreach ( $per_pages as $per_page ) { ?>
                    <option value="<?php echo $per_page ?>" <?php echo ( self::$tincan_opt_per_pages == $per_page ) ? 'selected="selected"' : '' ?>><?php echo $per_page ?></option>
				<?php } // foreach( $ld_groups ) ?>
            </select>

            Per Page
        </div>

        <div id="tincan-filters-export">
            <form action="<?php echo remove_query_arg( array(
				'paged',
				'tc_filter_mode',
				'tc_filter_group',
				'tc_filter_user',
				'tc_filter_course',
				'tc_filter_lesson',
				'tc_filter_module',
				'tc_filter_action',
				'tc_filter_date_range',
				'tc_filter_date_range_last',
				'tc_filter_start',
				'tc_filter_end',
				'orderby',
				'order'
			) ) ?>" method="get" id="tincan-filters-bottom">
                <input type="hidden" name="tc_filter_mode" value="csv"/>
                <input type="hidden" name="tc_filter_group" value=""/>
                <input type="hidden" name="tc_filter_user" value=""/>
                <input type="hidden" name="tc_filter_course" value=""/>
                <input type="hidden" name="tc_filter_lesson" value=""/>
                <input type="hidden" name="tc_filter_module" value=""/>
                <input type="hidden" name="tc_filter_action" value=""/>
                <input type="hidden" name="tc_filter_date_range" value=""/>
                <input type="hidden" name="tc_filter_date_range_last" value=""/>
                <input type="hidden" name="tc_filter_start" value=""/>
                <input type="hidden" name="tc_filter_end" value=""/>
                <input type="hidden" name="orderby"
                       value="<?php echo ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : '' ?>"/>
                <input type="hidden" name="order"
                       value="<?php echo ( ! empty( $_GET['order'] ) ) ? $_GET['order'] : '' ?>"/>

				<?php submit_button( __( 'Export To CSV' ), 'action', '', false, array( 'id' => "do_tc_export_csv" ) ); ?>
            </form>
        </div>

		<?php
	}
}
