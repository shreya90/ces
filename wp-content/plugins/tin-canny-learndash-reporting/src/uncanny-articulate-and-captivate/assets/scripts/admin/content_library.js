/**
 * @package storyline-and-captivate
 * @author Uncanny Owl
 * @version 0.0.1
 */

// Content Library
jQuery( document ).ready( function($) {
	var content_library_table = '#snc-content_library_wrap table ';

	$( content_library_table + 'a.content_title, ' + content_library_table + 'a.show' ).click( function( e ) {
		e.preventDefault();
		var id = $(this).attr( 'data-item_id' );

		$( '.embed_information' ).each( function() {
			var this_id = $(this).attr( 'data-item_id' );

			if ( id !== this_id ) {
				$(this).hide();
			} else {
				$(this).toggle();
			}
		});
	});
});