/**
 * @package storyline-and-captivate
 * @author Uncanny Owl
 * @version 0.0.1
 */

// Insert Into Post
jQuery( document ).ready( function($) {
	var $snc_form = $( '.snc-media_enbed_form' );

	// Get Code From PHP
	$snc_form.ajaxForm({
		success : function( data ) {
			data = JSON.parse( data );
			var win = window.dialogArguments || opener || parent || top;
			win.send_to_editor( data.shortcode );
		}
	});
});
