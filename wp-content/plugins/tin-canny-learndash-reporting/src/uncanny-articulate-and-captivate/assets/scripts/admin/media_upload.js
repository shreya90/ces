/**
 * @package storyline-and-captivate
 * @author Uncanny Owl
 * @version 0.0.1
 */

// Medea Upload Form
jQuery( document ).ready( function($) {
	trigger_upload_form( $ );
});

function trigger_upload_form( $, key ) {
	var $snc_form = $( '#snc-media_upload_file_form' );
	var $snc_button = $( '#snc-upload_button' );

	var $snc_progress = $( '#snc-media_upload_file_wrap .progress' );
	var $snc_progress_bar_wrapper = $( '#snc-progress_bar_wrapper' );
	var $snc_progress_bar = $( '#snc-progress_bar' );

	var $snc_message = $( '#snc-media_upload_message' );

	var enable_button = true;

	// Media Upload Form
	$snc_form.ajaxForm({
		beforeSubmit : function() {
			resetLoading();
		},
		uploadProgress : function( event, position, total, percentComplete ) {
			processLoading( percentComplete );
		},
		success : function( data ) {
			data = JSON.parse( data );

			if ( data.id === 'error' ) {
				error_message( data.message );

			} else {
				update_message( data.message );

				$( '#snc-media_upload_file_wrap' ).hide();

				if ( key ) {
					$( '#vc_properties-panel .vc-snc-trigger input' ).attr( 'value', data.id );
					$( '#vc_properties-panel .vc-snc-name input' ).attr( 'value', data.title );

					trigger_vc_snc_mode();
				} else {
					$( '.snc-embed_information' ).show();

					$( 'input#item_id' ).attr( 'value', data.id );
					$( 'input#item_title' ).attr( 'value', data.title );
				}
			}

			hide_loading();
		}
	});

	// Media Upload Button
	$( '.file_upload_button' ).click( function( e ) {
		e.preventDefault();

		var id = $(this).attr( 'data-id' );
		hide_message();

		if ( enable_button ) { $( 'input[type="file"][data-id="' + id + '"]' ).click(); }
	});

	// Media Upload Input
	$( '#snc-media_upload_file' ).change( function() {
		show_loading();

		var file = this.files[0];
		var ext = file_extension( file.name ).trim().toLowerCase();
		var size = file.size;

		if ( !in_array( ext, ['zip'] ) ) {
			error_message( 'Only .zip Files are Allowed.' );
			hide_loading();

			return false;
		}

 		if ( size > $( '#snc-max_file_size' ).val() ) {
			error_message( 'File is too large to upload.' );
			hide_loading();

			return false;
 		}

 		$( '#snc-extension' ).val( ext );

 		$snc_form.submit();
	});

	// HTML Controll
	function show_loading() {
		enable_button = false;

		$snc_progress.width( $snc_form.width() );

		$snc_progress.show();
		$snc_button.hide();
	}

	function hide_loading() {
		enable_button = true;
		$snc_progress.hide();
		$snc_button.show();
	}

	function resetLoading() {
		processLoading( 0 );
	}

	// Show Percent and Change Width
	function processLoading( percentComplete ) {
		var max_width = $snc_progress_bar_wrapper.width();
		var width = ( percentComplete < 10 ) ? max_width / 100 * 10 : max_width / 100 * percentComplete;

		$snc_progress_bar.width( width );
		$snc_progress_bar.html( percentComplete + '%' );
	}

	function error_message( message ) {
		$snc_message.attr( 'class', 'error' );
		$snc_message.show();
		$snc_message.html( '<p>' + message + '</p>' );
	}

	function update_message( message ) {
		$snc_message.attr( 'class', 'updated' );
		$snc_message.show();
		$snc_message.html( '<p>' + message + '</p>' );
	}

	function hide_message() {
		$snc_message.hide();
	}

	// Get File Extension
	function file_extension( file_name ) {
		var file = file_name.split( '.' );
		return file.pop();
	}

	// in_array (PHP Style)
	function in_array(needle, haystack, argStrict) {
		//  discuss at: http://phpjs.org/functions/in_array/
		// original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		// improved by: vlado houba
		// improved by: Jonas Sciangula Street (Joni2Back)
		//    input by: Billy
		// bugfixed by: Brett Zamir (http://brett-zamir.me)
		//   example 1: in_array('van', ['Kevin', 'van', 'Zonneveld']);
		//   returns 1: true
		//   example 2: in_array('vlado', {0: 'Kevin', vlado: 'van', 1: 'Zonneveld'});
		//   returns 2: false
		//   example 3: in_array(1, ['1', '2', '3']);
		//   example 3: in_array(1, ['1', '2', '3'], false);
		//   returns 3: true
		//   returns 3: true
		//   example 4: in_array(1, ['1', '2', '3'], true);
		//   returns 4: false

		var key = '', strict = !! argStrict;

		//we prevent the double check (strict && arr[key] === ndl) || (!strict && arr[key] == ndl)
		//in just one for, in order to improve the performance
		//deciding wich type of comparation will do before walk array
		if (strict) {
			for (key in haystack) {
				if (haystack[key] === needle) {
					return true;
				}
			}
		} else {
			for (key in haystack) {
				if (haystack[key] === needle) {
					return true;
				}
			}
		}

		return false;
	}
}