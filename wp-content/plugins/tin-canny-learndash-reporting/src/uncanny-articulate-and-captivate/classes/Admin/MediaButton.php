<?php
/**
 * Media Button
 *
 * @package    Tin Canny LearnDash Reporting
 * @subpackage Embed Articulate Storyline and Adobe Captivate
 * @author     Uncanny Owl
 * @since      1.0.0
 */

namespace TINCANNYSNC\Admin;

if ( !defined( "ABSPATH" ) ) {
	header( "Status: 403 Forbidden" );
	header( "HTTP/1.1 403 Forbidden" );
	exit();
}

class MediaButton {
	/**
	 * Constructor
	 *
	 * @access public
	 * @return void
	 * @since  1.0.0
	 */
	public function __construct() {
		add_action( 'media_buttons', array( $this, 'media_button' ),100 );
	}

	/**
	 * Print Media Button
	 *
	 * @trigger media_buttons action
	 * @access  public
	 * @return  void
	 * @since   1.0.0
	 */
	public function media_button() {
		?>
			<a href="media-upload.php?type=snc&tab=upload&TB_iframe=true" class="thickbox">
				<img src="<?php echo SnC_ASSET_URL ?>images/media_button.png"  width="56" height="27" />
			</a>
		<?php
	}
}
