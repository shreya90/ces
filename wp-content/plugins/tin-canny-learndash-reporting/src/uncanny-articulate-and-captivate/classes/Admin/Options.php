<?php
/**
 * Admin Options Controller
 *
 * @package    Tin Canny LearnDash Reporting
 * @subpackage Embed Articulate Storyline and Adobe Captivate
 * @author     Uncanny Owl
 * @since      1.0.0
 */

namespace TINCANNYSNC\Admin;

if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

class Options {
	static private $DEFAULT_OPTION = array(
		'default-lightbox-style' => 'colorbox',
		'colorbox-transition'    => 'elastic',
		'colorbox-theme'         => 'default',
		'nivo-transition'        => 'fade',
		'height'                 => 800,
		'height_type'            => 'px',
		'width'                  => 100,
		'width_type'             => '%',
		'tinCanActivation'       => '1',
		'disableMarkComplete'    => '1',
		'nonceProtection'        => '1'
	);

	static private $OPTION = array();

	/**
	 * initialize
	 *
	 * @access  public
	 * @return  void
	 * @since   1.0.0
	 */
	function __construct() {
		add_action( 'admin_menu', array( $this, 'register_admin_menu' ) );
	}

	/**
	 * Return Options
	 *
	 * @access  public static
	 * @return  void
	 * @since   1.0.0
	 */
	public static function get_options() {
		self::$OPTION = get_option( SnC_TEXTDOMAIN );

		if ( ! self::$OPTION ) {
			// Set Default Option
			self::$OPTION = self::$DEFAULT_OPTION;
			update_option( SnC_TEXTDOMAIN, self::$DEFAULT_OPTION );
		}

		return self::$OPTION;
	}

	/**
	 * Register Admin Menu
	 *
	 * @trigger admin_menu Action
	 * @access  public
	 * @return  void
	 * @since   1.0.0
	 */
	public function register_admin_menu() {
		add_submenu_page( 'uncanny-learnDash-reporting', 'Settings', 'Settings', 'manage_options', 'snc_options', array(
			$this,
			'view_options_page'
		) );
	}

	/**
	 * admin_menu Page
	 *
	 * @trigger add_options_page
	 * @access  public
	 * @return  void
	 * @since   1.0.0
	 */
	public function view_options_page() {
		if ( $_POST && wp_verify_nonce( $_POST['security'], 'snc-options' ) ) {

			unset( $_POST['security'] );
			unset( $_POST['submit'] );

			self::$OPTION = $_POST;
			update_option( SnC_TEXTDOMAIN, self::$OPTION );

			/////////////
			// Store data for other uses
			////////////

			// Show TinCan Tables
			$show_tincan_tables = absint( $_POST['tinCanActivation'] );

			if ( 1 == $show_tincan_tables ) {
				$value = 'yes';
			}
			if ( 0 == $show_tincan_tables ) {
				$value = 'no';
			}

			if ( current_user_can( 'manage_options' ) ) {
				update_option( 'show_tincan_reporting_tables', $value );
			}

			// Disable mark complete
			$disable_mark_complete = absint( $_POST['disableMarkComplete'] );

			if ( 1 == $disable_mark_complete ) {
				$value = 'yes';
			}
			if ( 0 == $disable_mark_complete ) {
				$value = 'no';
			}

			if ( current_user_can( 'manage_options' ) ) {
				update_option( 'disable_mark_complete_for_tincan', $value );
			}

			// Enable nonce protection
			$nonce_protection = absint( $_POST['nonceProtection'] );

			if ( 1 == $nonce_protection ) {
				$value = 'yes';
			}
			if ( 0 == $nonce_protection ) {
				$value = 'no';
			}

			if ( current_user_can( 'manage_options' ) ) {
				update_option( 'tincanny_nonce_protection', $value );
			}

			///////////////////////////////

		} else {
			self::$OPTION = self::get_options();
		}

		$nivo_transitions = \TINCANNYSNC\Shortcode::$nivo_transitions;

		include_once( SnC_PLUGIN_DIR . 'views/admin_options.php' );
	}
}
