<?php
/**
 * New File Controller
 *
 * @package    Tin Canny LearnDash Reporting
 * @subpackage Embed Articulate Storyline and Adobe Captivate
 * @author     Uncanny Owl
 * @since      1.0.0
 */

namespace TINCANNYSNC\FileSystem;

if ( !defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

class NewFile {
	use traitModule;

	private $uploaded     = true;
	private $upload_error = '';
	private $file         = '';

	/**
	 * Constructor
	 *
	 * @access public
	 * @return void
	 * @since  1.0.0
	 */
	public function __construct( $item_id, $file ) {
		$this->set_item_id( $item_id );
		$this->file = $file;

		$this->upload();
	}

	public function get_upload_error() {
		return $this->upload_error;
	}

	public function get_uploaded() {
		return $this->uploaded;
	}

	private function upload() {
		$this->extract_zip();

		if ( file_exists( $this->get_target_dir() ) ) {
			$this->set_type( $this->get_file_type() );

			switch( $this->get_type() ) {
				case 'Storyline' :
					$module = new Module\Storyline( $this->get_item_id() );
					if ( ! $module->register() ) {
						return $this->cancel_upload();
					}
				break;

				case 'Captivate' :
					$module = new Module\Captivate( $this->get_item_id() );

					if ( $this->get_subtype() == 'web' )
						$module->set_subtype( 'web' );

					if ( ! $module->register() ) {
						return $this->cancel_upload();
					}
				break;

				case 'Captivate2017' :
					$module = new Module\Captivate2017( $this->get_item_id() );

					if ( ! $module->register() ) {
						return $this->cancel_upload();
					}
				break;

				case 'iSpring' :
					$module = new Module\iSpring( $this->get_item_id() );

					if ( $this->get_subtype() == 'web' )
						$module->set_subtype( 'web' );

					if ( ! $module->register() ) {
						return $this->cancel_upload();
					}
				break;

				case 'ArticulateRise' :
					$module = new Module\ArticulateRise( $this->get_item_id() );
					if ( ! $module->register() ) {
						return $this->cancel_upload();
					}
				break;

				default:
					return $this->cancel_upload();
				break;
			}

		} else {
			$this->upload_error = 'Your server doesn\'t support Zip Archive, or your .zip file is not valid. Please contact your server administrator.';
			return $this->cancel_upload();
		}
	}

	private function extract_zip() {
		$target = $this->get_target_dir();

		if ( class_exists( '\ZipArchive' ) ) {
			$_zip = new \ZipArchive();

			if ( $_zip->open( $this->file ) ) {
				$_zip->extractTo( $target );
				$_zip->close();

			} else {
				shell_exec( sprintf( "unzip %s -d %s", $this->file, $target ) );
			}

		} else {
			shell_exec( sprintf( "unzip %s -d %s", $this->file, $target ) );
		}
	}

	private function get_file_type() {
		if ( $this->is_storyline() )
			return 'Storyline';

		if ( $this->is_captivate() )
			return 'Captivate';

		if ( $this->is_ispring() )
			return 'iSpring';

		if ( $this->is_articulate_rise() )
			return 'ArticulateRise';

		if ( $this->is_ispring_web() )
			return 'iSpring';

		if ( $this->is_captivate2017() )
			return 'Captivate2017';

		return false;
	}

	private function is_storyline() {
		$target = $this->get_target_dir();
		if ( file_exists( $target . '/story_content' ) )
			return true;

		return false;
	}

	private function is_captivate() {
		$target = $this->get_target_dir();

		if ( file_exists( $target . '/project.txt' ) && ! file_exists( $target . '/scormdriver.js' ) )
			$this->set_subtype( 'web' );

		if ( file_exists( $target . '/project.txt' ) )
			return true;

		return false;
	}

	private function is_ispring() {
		$target = $this->get_target_dir();
		if ( file_exists( $target . '/res/index.html' ) )
			return true;

		return false;
	}

	private function is_ispring_web() {
		$target = $this->get_target_dir();
		if ( file_exists( $target . '/data' ) && file_exists( $target . '/metainfo.xml' ) ) {
			$this->set_subtype( 'web' );

			return true;
		}

		return false;
	}


	private function is_articulate_rise() {
		$target = $this->get_target_dir();
		if ( file_exists( $target . '/scormcontent/lib/main.bundle.js' ) )
			return true;

		return false;
	}

	private function is_captivate2017() {
		$target = $this->get_target_dir();
		if ( file_exists( $target . '/captivate.css' ) )
			return true;

		return false;
	}

	private function cancel_upload() {
		\TINCANNYSNC\Database::delete( $this->get_item_id() );
		$this->uploaded = false;
	}

	public function get_result_json( $title ) {
		$array = array(
			'id'      => $this->get_item_id(),
			'message' => __( 'Uploaded! Pick Options Below.', "uncanny-learndash-reporting" ),
			'title'   => $title,
		);

		return json_encode( $array );
	}
}

