<?php
/**
 * New File Controller
 *
 * @package    Tin Canny LearnDash Reporting
 * @subpackage Embed Articulate Storyline and Adobe Captivate
 * @author     Uncanny Owl
 * @since      1.0.0
 */

namespace TINCANNYSNC;

if ( !defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

class Module {
	public static function get_module( $item_id ) {
		$item   = Database::get_item( $item_id );
		$module = false;

		if ( ! $item[ 'ID' ] )
			return false;

		$item[ 'type' ] = strtolower( str_replace( ' ', '', $item[ 'type' ] ) );

		switch( $item[ 'type' ] ) {
			case 'articulaterise':
				$module = new FileSystem\Module\ArticulateRise( $item_id );
				break;
			case 'storyline':
				$module = new FileSystem\Module\Storyline( $item_id );
				break;
			case 'ispring':
				$module = new FileSystem\Module\iSpring( $item_id );
				break;
			case 'captivate':
				$module = new FileSystem\Module\Captivate( $item_id );
				break;
			case 'captivate2017':
				$module = new FileSystem\Module\Captivate2017( $item_id );
				break;
		}

		if ( version_compare( $item['version'], '2.4', '>=' ) ) {
			$item[ 'url' ] = get_site_url() . $item[ 'url' ];
		}

		if ( $module ) {
			if ( is_ssl() )
				$item[ 'url' ] = str_replace( 'http://', 'https://' , $item[ 'url' ] );

			$module->set_url( $item[ 'url' ] );
			$module->set_name( $item[ 'file_name' ] );

			return $module;
		}

		return false;
	}
}

