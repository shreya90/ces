<?php
/**
 * Shortcode
 *
 * @package    Tin Canny LearnDash Reporting
 * @subpackage Embed Articulate Storyline and Adobe Captivate
 * @author     Uncanny Owl
 * @since      1.0.0
 */

namespace TINCANNYSNC;

if ( !defined( "ABSPATH" ) ) {
	header( "Status: 403 Forbidden" );
	header( "HTTP/1.1 403 Forbidden" );
	exit();
}

class Shortcode {
	static public $nivo_transitions = array(
		'Fade' => 'fade',
		'Fade Scale' => 'fadeScale',
		'Slide Left' => 'slideLeft',
		'Slide Right' => 'slideRight',
		'Slide Up' => 'slideUp',
		'Slide Down' => 'slideDown',
		'Fall' => 'fall',
	);

	/**
	 * Constructor
	 *
	 * @access public
	 * @return void
	 * @since  1.0.0
	 */
	function __construct() {
		add_shortcode( 'vc_snc', array( $this, 'shortcode' ) );
	}

	/**
	 * Is TinCan Module Exists
	 *
	 * @access  private
	 * @return  bool
	 * @since   1.0.0
	 */
	private function check_tincan() {
			return (
				class_exists( '\\UCTINCAN\\Init' ) &&
				!empty( \UCTINCAN\Init::$TinCan ) &&
				is_object( \UCTINCAN\Init::$TinCan ) &&
				get_class( \UCTINCAN\Init::$TinCan ) == 'TinCan\RemoteLRS'
			);
	}

	/**
	 * Shortcode Main
	 *
	 * @trigger add_shortcode( 'vc_snc' )
	 * @access  public
	 * @return  string
	 * @since   1.0.0
	 */
	public function shortcode( $atts ) {
		extract( shortcode_atts( array(
			'embed_type' => 'iframe',
			'item_id' => '',
			'item_name' =>'',
			'title' =>'',
			'button' =>'',
			'user_button_type' => '',
			'button_text' =>'',
			'button_image' => '',
			'nivo_transition' =>'',
			'colorbox_theme' =>'',
			'colorbox_transition' =>'',
			'colorbox_scrollbar' => '',
			'width' => '',
			'height' => ''
		), $atts, 'vc_snc' ) );

		if ( !$item_id )
			return '';

		global $post;
		$Module     = Module::get_module( $item_id );
		$postmeta   = get_post_meta( $post->ID, '_WE-meta_', true );

		$global_protection = get_option( 'tincanny_nonce_protection', 'yes' );
		$protection = 'Yes';

		if ( ! empty( $postmeta['protect-scormtin-can-modules'] ) ) {
			switch( $postmeta['protect-scormtin-can-modules'] ) {
				case 'Yes' :
					$protection = 'Yes';
					break;
				case 'No' :
					$protection = 'No';
					break;
				default :
					if ( $global_protection === 'yes' )
						$protection = 'Yes';

					if ( $global_protection === 'no' )
						$protection = 'No';
					break;
			}
		}

		if ( $protection === 'Yes' && ! get_current_user_id() )
			return '';

		if ( !empty( $Module ) && $Module->is_available() ) {
			$src = $Module->get_url();
			$User = wp_get_current_user();

			if ( $this->check_tincan() ) {
				$user_name = @( $User->data->display_name ) ? $User->data->display_name : 'Unknown';
				$user_email = @( $User->data->user_email ) ? $User->data->user_email : 'Unknown@anonimous.com';

				global $post;

				$args = array(
					'endpoint'    => \UCTINCAN\Init::$endpint_url . '/',
					'auth'        => 'LearnDashId' . $post->ID,
					'actor'       => rawurlencode( sprintf( '{"name": ["%s"], "mbox": ["mailto:%s"]}', $user_name, $user_email ) ),
					'activity_id' => $src,
					'client'      => $Module->get_type(),
					'base_url'    => get_option( 'home' ),
					'nonce'       => wp_create_nonce( 'tincanny-module' ),
				);

				$src = add_query_arg( $args, $src );
			}
		} else if ( empty( $Module ) ) {
			return 'The module you are trying to access does not exist.';

		} else {
			return false;
		}

		if ( $button == 'text' && $button_text  ) {
			$button_image = '';
		} else if ( ( $button == 'image' || $button == 'url' ) && $button_image  ) {
			$button_text = '';
		} else {
			switch( $button ) {
				case 'small' :
					$button_text = '';
					$button_image = SnC_ASSET_URL . 'images/btn-launch-s.png';
				break;

				case 'medium' :
					$button_text = '';
					$button_image = SnC_ASSET_URL . 'images/btn-launch-m.png';
				break;

				case 'large' :
					$button_text = '';
					$button_image = SnC_ASSET_URL . 'images/btn-launch-l.png';
				break;

				default :
					$button_text = '';
					$button_image = '';
				break;
			}
		}

		switch( $embed_type ) {
			case 'iframe' :
				$width = ( !$width ) ? '100%' : $width;
				$height = ( !$height ) ? '600px' : $height;

				return $this->return_iframe( array(
					'width' => $width,
					'height' => $height,
					'src' => $src
				));
			break;

			case '_blank' :
			case '_self' :
				return $this->return_link( array(
					'link_text' => $button_text,
					'button' => $button_image,
					'target' => $embed_type,
					'href' => $src,
				));
			break;

			case 'lightbox' :
				$slider_script = 'nivo';
				$theme = '';
				$transition = $nivo_transition;

				$default_options = Admin\Options::get_options();

				$width = ( $width ) ? $width : $default_options['width'] . $default_options['width_type'];
				$height = ( $height ) ? $height : $default_options['height'] . $default_options['height_type'];

				return $this->return_lightbox( array(
					'title' => $title,
					'link_text' => $button_text,
					'button' => $button_image,
					'scrollbar' => $colorbox_scrollbar,
					'width' => $width,
					'height' => $height,
					'slider_script' => $slider_script,
					'href' => $src,
					'theme' => $theme,
					'transition' => $transition,
				));
			break;
		}
	}

	/**
	 * Shortcode Return [lightbox]
	 *
	 * @since 0.0.1
	 * @access private
	 */
	private function return_lightbox( $atts ) {
		extract( shortcode_atts( array(
			'title' => '',
			'link_text' => '',
			'button' =>'',
			'scrollbar' =>'',
			'size_optiton' =>'',
			'width' =>'',
			'height' =>'',
			'href' =>'',
			'theme' => '',
			'transition' => ''
		), $atts, 'link' ) );
		if ( !$href ) return '';

		$options = Admin\Options::get_options();

		if ( $link_text ) {
			$text = $link_text;
		} else if ( $button ) {
			$text = sprintf( '<img class="launch_presentation" src="%s" title="launch_presentation" />', $button );
		} else {
			$text = 'Launch';
		}

		$title = ( $title ) ? sprintf( ' title="%s"', $title ) : '';
		$scrolling = ( $scrollbar == 'no' ) ? ' data-scroll="false"' : '';
		$width = ( $width ) ? ' data-width="' . $width . '"' : '';
		$height = ( $height ) ? ' data-height="' . $height . '"' : '';
		$transition = ( $transition ) ? ' data-transition="' . $transition . '"' : '';

		wp_enqueue_script( 'nivo-lightbox', SnC_ASSET_URL . 'venders/nivo-lightbox/nivo-lightbox.min.js', array( 'jquery' ), '1.2.0', true );
		wp_enqueue_style( 'nivo-lightbox', SnC_ASSET_URL . 'venders/nivo-lightbox/nivo-lightbox.css' );
		wp_enqueue_style( 'nivo-lightbox-default', SnC_ASSET_URL . 'venders/nivo-lightbox/themes/default/default.css' );

		return sprintf( '<a class="nivo_iframe" data-lightbox-type="iframe" href="%s" %s %s %s %s %s>%s</a>', $href, $title, $scrolling, $width, $height, $transition, $text );
	}

	/**
	 * Shortcode Return [iframe]
	 *
	 * @since 0.0.1
	 * @access private
	 */
	private function return_iframe( $atts ) {
		extract( shortcode_atts( array(
			'width' => '',
			'height' => '',
			'src' =>''
		), $atts, 'link' ) );
		if ( !$src ) return '';

		return sprintf( '<iframe class="AnC-iFrame" data-src="%s" width="%s" height="%s" frameborder="0"></iframe>', $src, $width, $height );
	}

	/**
	 * Shortcode Return [link]
	 *
	 * @since 0.0.1
	 * @access private
	 */
	private function return_link( $atts ) {
		extract( shortcode_atts( array(
			'link_text' => '',
			'button' => '',
			'target' => '_blank',
			'href' => '',
		), $atts, 'link' ) );
		if ( !$href ) return '';

		$options = Admin\Options::get_options();

		if ( $link_text ) {
			$text = $link_text;
		} else if ( $button ) {
			$text = sprintf( '<img class="launch_presentation" src="%s" title="launch_presentation" />', $button );
		} else {
			$text = 'Launch';
		}

		return sprintf( '<a class="AnC-Link" href="%s" target="%s">%s</a>', $href, $target, $text );
	}

	/**
	 * Shortcode Generator with $_POST data
	 *
	 * @since 0.0.1
	 * @access public
	 */
	static public function generate_shortcode( $data ) {
		if ( !$data['id'] ) return false;

		$default_options = Admin\Options::get_options();
		$Module          = Module::get_module( $data['id'] );
		$url             = false;

		if ( $Module->is_available() )
			$url = $Module->get_url();

		if ( !$url ) return false;

		$embed_type = $lightbox_title_text = $link_text = $btn_src = $scrollbar = $size_option = $width = $height = $slider = $theme = $transition = '';
		$embed_type = $data['insert_type'];

		// Modifying Name : TODO
		$data['title'] = Database::ChangeNameFromId( $data['id'], $data['title'] );

		$shortcode = sprintf( '[vc_snc embed_type="%s" item_id="%s" item_name="%s"', $embed_type, $data['id'], $data['title'] );

		switch( $embed_type ) {
			case 'iframe' :
				$width = ( !$data['iframe_width'] ) ? ' width="100%"' : ' width="' . $data['iframe_width'] . $data['iframe_width_type'] . '"';
				$height = ( !$data['iframe_height'] ) ? ' height="600px"' : ' height="' . $data['iframe_height'] . $data['iframe_height_type'] . '"';

				return sprintf( $shortcode . '%s%s frameborder="0" src="%s"]', $width, $height, $url );
			break;

			case 'lightbox' :
				// Title
				$lightbox_title_text = ( $data['lightbox_title'] == 'With Title' && $data['lightbox_title_text'] ) ? ' title="' . $data['lightbox_title_text'] . '"' : '';

				// Button
				switch ( $data['lightbox_button'] ) {
					case 'url' :
						$btn_src = ' button_image="' . $data['lightbox_button_url'] . '"';
					break;

					case 'text' :
						if ( $data['lightbox_button_text'] ) $link_text = ' button_text="' . $data['lightbox_button_text'] . '"';
					break;

					case 'image' :
						if ( !$_FILES['lightbox_button_custom_file']['error'] && $_FILES['lightbox_button_custom_file']['name'] ) {
							$file = self::insert_attachment( $_FILES['lightbox_button_custom_file'] );
 							$btn_src = ' button_image="' . $file['url'] . '"';
						}
					break;
				}

				// Scrollbar
				$scrollbar = ( isset( $data['scrollbar'] ) && $data['scrollbar'] == 'false' ) ? ' colorbox_scrollbar="no"' : '';

				$data['width'] = ( isset( $data['width'] ) && $data['width'] ) ? $data['width'] : $default_options['width'];
				$data['height'] = ( isset( $data['height'] ) && $data['height'] ) ? $data['height'] : $default_options['height'];

				$width = ' width="' . $data['width'] . $data['width_type'] . '"';
				$height = ' height="' . $data['height'] . $data['height_type'] . '"';

				// Slider
				$slider = ' slider_script="nivo"';

				$transition = ' nivo_transition="' . $data['nivo_transition'] . '"';

				return sprintf( $shortcode . ' button="%s"%s%s%s%s%s%s%s%s%s href="%s"]', $data['lightbox_button'], $lightbox_title_text, $link_text, $btn_src, $scrollbar, $width, $height, $slider, $theme, $transition, $url );
			break;

			case '_blank' :
				switch ( $data['_blank'] ) {
					case 'url' :
						$btn_src = ' button_image="' . $data['_blank_url'] . '"';
					break;

					case 'text' :
						if ( $data['_blank_text'] ) $link_text = ' button_text="' . $data['_blank_text'] . '"';
					break;

					case 'image' :
						if ( !$_FILES['upload_blank_lightbox_custom_button']['error'] && $_FILES['upload_blank_lightbox_custom_button']['name'] ) {
							$file = self::insert_attachment( $_FILES['upload_blank_lightbox_custom_button'] );
 							$btn_src = ' button_image="' . $file['url'] . '"';
						}
					break;
				}

				return sprintf( $shortcode . ' button="%s"%s%s href="%s"]', $data['_blank'], $link_text, $btn_src, $url );
			break;

			case '_self' :
				switch ( $data['_self'] ) {
					case 'url' :
						$btn_src = ' button_image="' . $data['_self_url'] . '"';
					break;

					case 'text' :
						if ( $data['_self_text'] ) $link_text = ' button_text="' . $data['_self_text'] . '"';
					break;

					case 'image' :
						if ( !$_FILES['upload_self_lightbox_custom_button']['error'] && $_FILES['upload_self_lightbox_custom_button']['name'] ) {
							$file = self::insert_attachment( $_FILES['upload_self_lightbox_custom_button'] );
 							$btn_src = ' button_image="' . $file['url'] . '"';
						}
					break;
				}

				return sprintf( $shortcode . ' button="%s"%s%s href="%s"]', $data['_self'], $link_text, $btn_src, $url );
			break;
		}
	}

	static public function insert_attachment( $file ) {
		$name = $file['name'];
		$temp_name = $file['tmp_name'];

		$error = new \WP_Error();
		$wp_upload_dir = wp_upload_dir();
		$uploadfile = "{$wp_upload_dir['path']}/{$name}";

		if (move_uploaded_file($temp_name, $uploadfile)) {
			$wp_filetype = wp_check_filetype(basename($name), null );

			$attachment = array(
				'guid' => $wp_upload_dir['url'] . '/' . basename( $name ),
				'post_mime_type' => $wp_filetype['type'],
				'post_title' => preg_replace('/\.[^.]+$/', '', basename($name)),
				'post_content' => '',
				'post_status' => 'inherit'
			);

			$attach_id = wp_insert_attachment($attachment, $uploadfile);

			require_once( ABSPATH . 'wp-admin/includes/image.php' );

			$attach_data = wp_generate_attachment_metadata( $attach_id, $uploadfile );
			wp_update_attachment_metadata( $attach_id, $attach_data );

			return array(
				'ID'   => $attach_id,
				'post' => $attachment,
				'url'  => $wp_upload_dir['url'] . '/' . basename( $name ),
			);
		} else {
			$error->add('File_upload', __( 'Failed to upload a file.', "uncanny-learndash-reporting" ));

			return $error;
		}
	}
}
