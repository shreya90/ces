<div class="wrap" id="snc_options">
	<h2>Tin Canny Settings</h2>
	<div class="clear"></div>
	by <a href="http://www.uncannyowl.com/" target="_blank">Uncanny Owl</a>

	<form enctype="multipart/form-data" id="snc_options_form" method="POST">
		<input type="hidden" name="security" value="<?php echo wp_create_nonce( "snc-options" ) ?>" />

		<table class="form-table">
			<tbody>
				<tr class="nivo">
					<th scope="row"><label for="nivo-transition">Nivo Transition</label></th>
					<td>
						<select name="nivo-transition" id="nivo-transition">
							<?php foreach ( $nivo_transitions as $key => $nivo_transition ) : ?>
								<option value="<?php echo $nivo_transition ?>"<?php if ( self::$OPTION['nivo-transition'] === $nivo_transition ) echo ' selected="selected"'; ?>><?php echo $key ?></option>
							<?php endforeach; ?>
						</select>
					</td>
				</tr>

				<tr>
					<th scope="row"><label for="width">Default Lightbox Size</label></th>
					<td>
						<ul>
							<li>
								<label for="width" class="size_label">Width</label>
								<input type="text" name="width" id="width" value="<?php echo self::$OPTION['width'] ?>" />
								<select name="width_type" id="width_type">
									<option value="px"<?php if ( self::$OPTION['width_type'] === 'px' ) echo ' selected="selected"'; ?>>px</option>
									<option value="%"<?php if ( self::$OPTION['width_type'] === '%' ) echo ' selected="selected"'; ?>>%</option>
								</select>
							</li>
							<li>
								<label for="height" class="size_label">Height</label>
								<input type="text" name="height" id="height" value="<?php echo self::$OPTION['height'] ?>" />
								<select name="height_type" id="height_type">
									<option value="px"<?php if ( self::$OPTION['height_type'] === 'px' ) echo ' selected="selected"'; ?>>px</option>
									<option value="%"<?php if ( self::$OPTION['height_type'] === '%' ) echo ' selected="selected"'; ?>>%</option>
								</select>
							</li>
						</ul>
					</td>
				</tr>
			</tbody>
		</table>

        <h3><span>Do you want to capture Tin Can Data?</span></h3>
        <div>
            <input <?php if ( self::$OPTION['tinCanActivation'] === '1' ) echo ' checked="checked"'; ?> type="radio" name="tinCanActivation" value="1">Yes<br>
            <input <?php if ( self::$OPTION['tinCanActivation'] === '0' ) echo ' checked="checked"'; ?> type="radio" name="tinCanActivation" value="0">No<br>
        </div>

        <h3><span>Disable LearnDash Mark Complete button until the learner completes all Tin Can modules in the Lesson                                    /Topic?</span></h3>
        <div>
            <input <?php if ( self::$OPTION['disableMarkComplete'] === '1' ) echo ' checked="checked"'; ?> type="radio" name="disableMarkComplete" value="1"> Yes<br>
            <input <?php if ( self::$OPTION['disableMarkComplete'] === '0' ) echo ' checked="checked"'; ?> type="radio" name="disableMarkComplete" value="0">No<br>
        </div>

        <h3><span>Protect SCORM/Tin Can Modules?</span></h3>
        <div>
            <input <?php if ( self::$OPTION['nonceProtection'] === '1' ) echo ' checked="checked"'; ?> type="radio" name="nonceProtection" value="1"> Yes<br>
            <input <?php if ( self::$OPTION['nonceProtection'] === '0' ) echo ' checked="checked"'; ?> type="radio" name="nonceProtection" value="0"> No<br>
        </div>
        

		<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p>
	</form>

    <h3><span>Reset Tin Can Data</span></h3>
    <div>
        <button id="btnResetTinCanData">Reset Tin Can Data</button>
    </div>

    <h3><span>Reset Bookmark Data</span></h3>
    <div>
        <button id="btnResetBookmarkData">Reset Bookmark Data</button>
    </div>

</div>