<div id="snc-content_library_wrap" class="wrap snc_TB">
	<span class="dashicons dashicons-admin-media"></span> <div class="title">Content Library</div>

	<div class="clear"></div>

	<table class="widefat">
		<tbody>
			<?php if ( !empty( $posts ) ) : foreach( $posts as $post ) : if ( !empty( $post ) ) : ?>
			<tr id="item_<?php echo $post->ID ?>" data-item_id="<?php echo $post->ID ?>">
				<td>
					<a class="content_title" href="#" data-item_id="<?php echo $post->ID ?>" data-item_name="<?php echo $post->file_name ?>"><?php echo $post->file_name ?></a>
					<span style="float:right">

						<?php if ( empty( $content_library_vc_mode ) ) : ?>
						<a href="#" class="show" data-item_id="<?php echo $post->ID ?>">Show</a> |
						<?php else : ?>
						<a href="#" class="choose" data-item_id="<?php echo $post->ID ?>" data-item_name="<?php echo $post->file_name ?>">Choose</a> |
						<?php endif; ?>

						<a href="#" class="delete" data-item_id="<?php echo $post->ID ?>">Delete</a>
					</span>

					<div class="embed_information" data-item_id="<?php echo $post->ID ?>">
						<?php
							$snc_post = $post;
							if ( !$content_library_vc_mode ) include( SnC_PLUGIN_DIR . 'views/embed_information.php' );
						?>

					</div>
				</td>
			 </tr>
			<?php endif; endforeach; endif; ?>
		</tbody>
	</table>
</div>



