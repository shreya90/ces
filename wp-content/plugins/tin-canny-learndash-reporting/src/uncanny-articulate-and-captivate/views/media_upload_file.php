<div id="snc-media_upload_file_wrap" class="wrap snc_TB">
	<div class="title">Upload File</div>

	<div class="clear"></div>

	<form enctype="multipart/form-data" id="snc-media_upload_file_form" action="<?php echo admin_url( 'admin-ajax.php' ) ?>" method="POST">
		<input type="hidden" name="action" value="SnC_Media_Upload" />
		<input type="hidden" name="security" value="<?php echo wp_create_nonce( "snc-media_upload_form" ) ?>" />

		<input type="hidden" name="extension" id="snc-extension" value="" />
		<input type="hidden" name="max_file_size" id="snc-max_file_size" value="<?php echo wp_max_upload_size(); ?>" />

		<p class="description">Please upload a zip file published from one of <a href="https://www.uncannyowl.com/knowledge-base/authoring-tools-supported/" target="_blank">the supported authoring tools.</a></p>
		<p class="description">
			<a href="http://www.elearningfreak.com/uncategorized/increase-maximum-upload-file-size/" target="_blank">Maximum upload file size</a>: <strong><?php echo $this->format_bytes( wp_max_upload_size() ); ?></strong>
		</p>
		<p class="description">(To change this size, increase your PHP upload limit or contact your web host)</p>

		<div class="clear"></div>

		<!-- Button -->
		<section id="snc-upload_button" class="file_upload_button" data-id="snc-media_upload_file">
			<span class="dashicons dashicons-plus-alt"></span>
			<div>Click to Upload</div>
		</section>

		<div class="progress">
			<div id="snc-progress_bar_wrapper"><div id="snc-progress_bar"></div></div>
			<p class="description">Please wait while your file is uploaded.</p>
		</div>

		<!-- Message -->
		<h2></h2>
		<div id="snc-media_upload_message" class="updated"><p></p></div>

		<!-- File Upload -->
		<input name="media_upload_file"  id="snc-media_upload_file" type="file" data-id="snc-media_upload_file" />
	</form>
</div>



