<?php

namespace uncanny_learndash_reporting;

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Class ReportingCapabilities
 * @package uncanny_custom_toolkit
 */
class ReportingCapabilities extends Config {

	/**
	 * Class constructor
	 */
	public function __construct() {

		// Add custom capabilities to roles
		add_action( 'admin_init', array( __CLASS__, 'add_reporting_capability' ) );

	}

	/*
	 * Define roles to add capabilities to
	 * @since 1.x
	 */
	public static function add_reporting_capability() {

		// Set which roles will need access to reporting
		$set_role_for_reporting = array('group_leader', 'administrator');

		self::add_reporting_capabilities_to_role($set_role_for_reporting);

	}

	/*
	 * Add reporting capability to all roles in array
	 * @since 1.x
	 *
	 * @param Array $set_role_for_reporting
	 */
	public static function add_reporting_capabilities_to_role($set_role_for_reporting) {

		// Loop through all roles that need the reporting capability added
		foreach($set_role_for_reporting as $role ){

			// Get the role class instance
			$group_leader_role = get_role( $role );

			// Add the reporting capability to the role
			$group_leader_role->add_cap( 'tincanny_reporting' );

		}

	}

}