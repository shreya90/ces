<?php
/**
 * Metabox
 *
 * @package    Tin Canny LearnDash Reporting
 * @subpackage TinCan Module
 * @author     Uncanny Owl
 * @since      1.0.0
 */

namespace UCTINCAN\Admin;

if ( !defined( "ABSPATH" ) ) {
	header( "Status: 403 Forbidden" );
	header( "HTTP/1.1 403 Forbidden" );
	exit();
}

class Metabox {
	/**
	 * Constructor
	 *
	 * @access public
	 * @return void
	 * @since  1.0.0
	 */
	function __construct() {
		require_once( UCTINCAN_PLUGIN_DIR . 'vendors/wp_express/autoload.php' );

		$this->set_metabox( 'sfwd-lessons' );
		$this->set_metabox( 'sfwd-topic' );
	}

	/**
	 * Set Metabox
	 *
	 * @access private
	 * @param  string $post_type
	 * @return void
	 * @since  1.0.0
	 */
	private function set_metabox( $post_type ) {
		$lesson = new \WE_TINCANNY\PostType( $post_type );

		$lesson->version = '1.4';

		$lesson->section = 'TinCanny Settings';
		$lesson->setting = "Restrict Mark Complete";
		$lesson->setting->type = 'select';
		$lesson->setting->option = 'Use Global Setting';
		$lesson->setting->option = 'Yes';
		$lesson->setting->option = 'No';
		$lesson->setting->description = 'Choose whether or not the Mark Complete button will be disabled until users complete all Tin Can modules on the page';

		$lesson->setting = "Completion Condition";
		$lesson->setting->type = 'text';
		$lesson->setting->description = 'Comma separated TinCanny verb. For result, you can enter the condition like <code>result > 80.</code>';

		$lesson->setting = "Protect SCORM/Tin Can Modules?";
		$lesson->setting->type   = 'select';
		$lesson->setting->option = 'Use Global Setting';
		$lesson->setting->option = 'Yes';
		$lesson->setting->option = 'No';
	}
}
