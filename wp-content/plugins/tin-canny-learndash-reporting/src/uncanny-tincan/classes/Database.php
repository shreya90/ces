<?php
/**
 * Database
 *
 * @package    Tin Canny LearnDash Reporting
 * @subpackage TinCan Module
 * @author     Uncanny Owl
 * @since      1.0.0
 *
 * @wp_option  UncannyOwl TinCanny DB Version
 */

namespace UCTINCAN;

if ( !defined( "ABSPATH" ) ) {
	header( "Status: 403 Forbidden" );
	header( "HTTP/1.1 403 Forbidden" );
	exit();
}

class Database {
	// Constants
	const TABLE_REPORTING = 'uotincan_reporting';
	const TABLE_LOG       = 'uotincan_log';
	const TABLE_RESUME    = 'uotincan_resume';

	// Static Values
	private   static $table_exists = false; // Yes / No
	protected static $user_id;

	// Upgraded
	public static $upgraded = false;

	/**
	 * Check Table Exists
	 *
	 * @access protected
	 * @return boolean
	 * @since  1.0.0
	 */
	protected function is_table_exists() {
		if ( ! self::$user_id )
			self::$user_id = get_current_user_id();

		return true;
	}

	public function upgrade() {
		$table_exists  = $this->query_table_exists();
		$query_results = array();

		// Create Not Existed Tables
		foreach( $table_exists as $key => $result ) {
			if ( false == $result )
				if ( false == call_user_func( array( $this, 'create_table_' . $key ) ) )
					$query_results[ $key ] = true;
		}

		if ( empty( $query_results ) ) {
			update_option( Init::TABLE_VERSION_KEY, UNCANNY_REPORTING_VERSION );
			self::$table_exists == true;
		}
	}

	/**
	 * Check Table Exists ( Query Part )
	 *
	 * @access private
	 * @return void
	 * @since  1.0.0
	 */
	private function query_table_exists() {
		global $wpdb;

		$query_reporting = sprintf( "show tables like '%s%s';", $wpdb->prefix, self::TABLE_REPORTING );
		$query_log       = sprintf( "show tables like '%s%s';", $wpdb->prefix, self::TABLE_LOG );
		$query_resume    = sprintf( "show tables like '%s%s';", $wpdb->prefix, self::TABLE_RESUME );

		return array(
			self::TABLE_REPORTING => $wpdb->query( $query_reporting ),
			self::TABLE_LOG       => $wpdb->query( $query_log ),
			self::TABLE_RESUME    => $wpdb->query( $query_resume ),
		);
	}

	/**
	 * Create Table : TABLE_REPORTING
	 *
	 * @access private
	 * @return bool
	 * @since  1.0.0
	 */
	private function create_table_uotincan_reporting() {
		global $wpdb;

		$query = sprintf( "CREATE TABLE %s%s (
					`id`          BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
					`group_id`    BIGINT(20) UNSIGNED NULL DEFAULT NULL,
					`user_id`     BIGINT(20) UNSIGNED NOT NULL,
					`course_id`   BIGINT(20) UNSIGNED NULL DEFAULT NULL,
					`lesson_id`   BIGINT(20) UNSIGNED NULL DEFAULT NULL,
					`module`      VARCHAR(255),
					`module_name` VARCHAR(255),
					`target`      VARCHAR(255),
					`target_name` VARCHAR(255),
					`verb`        VARCHAR(50),
					`result`      INT(7),
					`minimum`     INT(7),
					`completion`  BOOL,
					`stored`      DATETIME,

					PRIMARY KEY (`id`),
					CONSTRAINT `fk_TinCanUser`   FOREIGN KEY(`user_id`)   REFERENCES {$wpdb->users}(`ID`) ON UPDATE CASCADE ON DELETE CASCADE,
					CONSTRAINT `fk_TinCanGroup`  FOREIGN KEY(`group_id`)  REFERENCES {$wpdb->posts}(`ID`) ON UPDATE CASCADE ON DELETE CASCADE,
					CONSTRAINT `fk_TinCanCourse` FOREIGN KEY(`course_id`) REFERENCES {$wpdb->posts}(`ID`) ON UPDATE CASCADE ON DELETE CASCADE,
					CONSTRAINT `fk_TinCanLesson` FOREIGN KEY(`lesson_id`) REFERENCES {$wpdb->posts}(`ID`) ON UPDATE CASCADE ON DELETE CASCADE
				);", $wpdb->prefix, self::TABLE_REPORTING );
		$wpdb->query( $query );

		// For Lower Version
		if ( $wpdb->last_error ) {
			$query = sprintf( "CREATE TABLE %s%s (
						`id`          BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
						`group_id`    BIGINT(20) UNSIGNED NULL DEFAULT NULL,
						`user_id`     BIGINT(20) UNSIGNED NOT NULL,
						`course_id`   BIGINT(20) UNSIGNED NULL DEFAULT NULL,
						`lesson_id`   BIGINT(20) UNSIGNED NULL DEFAULT NULL,
						`module`      VARCHAR(255),
						`module_name` VARCHAR(255),
						`target`      VARCHAR(255),
						`target_name` VARCHAR(255),
						`verb`        VARCHAR(50),
						`result`      INT(7),
						`minimum`     INT(7),
						`completion`  BOOL,
						`stored`      DATETIME,

						PRIMARY KEY (`id`)
					);", $wpdb->prefix, self::TABLE_REPORTING );

			$wpdb->query( $query );
		}

		if ( $wpdb->last_error )
			return false;

		return true;
	}

	/**
	 * Create Table : TABLE_LOG
	 *
	 * @access private
	 * @return bool
	 * @since  1.0.0
	 */
	private function create_table_uotincan_log() {
		global $wpdb;

		$query = sprintf( "CREATE TABLE %s%s (
					`id`       BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
					`stored`   DATETIME,
					`log`      LONGTEXT,

					PRIMARY KEY (id)
				);", $wpdb->prefix, self::TABLE_LOG );
		$wpdb->query( $query );

		if ( $wpdb->last_error )
			return false;

		return true;
	}

	/**
	 * Create Table : TABLE_RESUME
	 *
	 * @access private
	 * @return bool
	 * @since  1.0.0
	 */
	private function create_table_uotincan_resume() {
		global $wpdb;

		$query = sprintf( "
			CREATE TABLE %s%s (
				`user_id`     BIGINT(20) UNSIGNED NOT NULL,
				`module_id`   BIGINT(20) NOT NULL,
				`state`       VARCHAR(50),
				`value`       TEXT
			", $wpdb->prefix, self::TABLE_RESUME );

		$foreign_keys = sprintf( "
			CONSTRAINT `fk_Resume_User`   FOREIGN KEY(`user_id`)   REFERENCES %s(`ID`) ON UPDATE CASCADE ON DELETE CASCADE,
			CONSTRAINT `fk_Resume_Module` FOREIGN KEY(`module_id`) REFERENCES %s(`ID`) ON UPDATE CASCADE ON DELETE CASCADE
		", $wpdb->users, $wpdb->prefix . SnC_TABLE_NAME );

		// With Foreagn Key
		$wpdb->query( $query . ',' . $foreign_keys . ')' );

		// For Lower Version: Without Foreagn Key
		if ( $wpdb->last_error )
			$wpdb->query( $query . ')' );

		if ( $wpdb->last_error )
			return false;

		return true;
	}

	/**
	 * Set Report
	 *
	 * @access public
	 *
	 * @param string $group_id
	 * @param int    $user_id
	 * @param int    $course_id
	 * @param int    $lesson_id
	 * @param string $module
	 * @param string $module_name
	 * @param string $target
	 * @param string $target_name
	 * @param string $verb
	 * @param int    $result
	 * @param int    $maximum
	 * @param string $completion
	 *
	 * @return bool
	 * @since  1.0.0
	 *
	 * @todo data type of $completion
	 */
	public function set_report( $group_id, $course_id, $lesson_id, $module, $module_name, $target, $target_name, $verb, $result, $maximum, $completion, $user_id = 0 ) {
		if ( ! $this->is_table_exists() )
			return false;

		global $wpdb;

		if ( !$group_id ) $group_id = 'NULL';
		if ( !$course_id ) $course_id = 'NULL';
		if ( !$lesson_id ) $lesson_id = 'NULL';
		if ( $result === false ) $result = 'NULL';
		if ( $maximum === false ) $maximum = 'NULL';
		if ( $completion === false ) $completion = 'NULL';

		$query = sprintf( "INSERT INTO %s%s
				( `group_id`, `user_id`, `course_id`, `lesson_id`, `module`, `module_name`, `target`, `target_name`, `verb`, `result`, `minimum`, `completion`, `stored` )
				VALUES ( %s, %s, %s, %s, '%s', '%s', '%s', '%s', '%s', %s, %s, %s, NOW() );
			",
			$wpdb->prefix,
			self::TABLE_REPORTING,
			$group_id,
			($user_id) ? $user_id : self::$user_id,
			$course_id,
			$lesson_id,
			$module,
			$module_name,
			$target,
			$target_name,
			$verb,
			$result,
			$maximum,
			$completion
		);

		$query = $wpdb->query( $query );

		if ( $wpdb->last_error )
			return false;

		return true;
	}

	/**
	 * Writing Logs for this
	 *
	 * @access private
	 * @param  mixed $value
	 * @return void
	 * @since  1.0.0
	 */
	protected function write_log( $value ) {
		self::WriteLog( $value );
	}

	/**
	 * Writing Logs for Other Classes
	 *
	 * @access public
	 * @param  mixed $value
	 * @return void
	 * @since  1.0.0
	 */
	public static function WriteLog( $value ) {
		$value = print_r( $value, true );
		$now   = current_time( 'mysql' );

		global $wpdb;
		$query = sprintf( "
			INSERT INTO %s%s
				( `log`, `stored` ) VALUES ( '%s', '%s' );
			",
			$wpdb->prefix,
			self::TABLE_LOG,
			$value,
			$now
		);

		$query = $wpdb->query( $query );
	}

	public static function ShowLog() {
		global $wpdb;
		$query = sprintf( "SELECT `log` FROM %s%s",
			$wpdb->prefix,
			self::TABLE_LOG
		);

		return $wpdb->get_results( $query );
	}

	public static function ResetLog() {
		global $wpdb;
		$query = sprintf( "TRUNCATE %s%s",
			$wpdb->prefix,
			self::TABLE_LOG
		);

		$wpdb->query( $query );
	}
}
