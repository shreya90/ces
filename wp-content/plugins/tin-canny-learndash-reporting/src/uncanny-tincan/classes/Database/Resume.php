<?php
/**
 * Database\Completion
 *
 * @package    Tin Canny LearnDash Reporting
 * @subpackage TinCan Module
 * @author     Uncanny Owl
 * @since      1.3.0
 */

namespace UCTINCAN\Database;

if ( !defined( "ABSPATH" ) ) {
	header( "Status: 403 Forbidden" );
	header( "HTTP/1.1 403 Forbidden" );
	exit();
}

class Resume extends \UCTINCAN\Database {
	use \UCTINCAN\Modules;

	public function get_resume( $url ) {
		global $wpdb;

		if ( ! $this->is_table_exists() )
			return false;

		$module_id = $this->get_slide_id_from_url( $url );

		$query = sprintf( "
			SELECT `value` FROM %s%s
				WHERE
					`user_id`   = %s AND
					`module_id` = %s
			LIMIT 1
			",
			$wpdb->prefix,
			self::TABLE_RESUME,
			self::$user_id,
			$module_id[1]
		);

		$value = $wpdb->get_var( $query );

		return $value;
	}

	public function save_resume( $url, $content ) {
		global $wpdb;

		if ( ! $this->is_table_exists() )
			return;

		if ( ! $content )
			return;

		if ( $this->get_resume( $url ) )
			$this->delete_resume( $url );

		$module_id = $this->get_slide_id_from_url( $url );

		$query = sprintf( "
			INSERT INTO %s%s
				( `user_id`, `module_id`, `value` )
				VALUES ( %s, %s, '%s' );
		", $wpdb->prefix, self::TABLE_RESUME, self::$user_id, $module_id[1], $content );

		$query = $wpdb->query( $query );
	}

	private function delete_resume( $url ) {
		global $wpdb;

		$module_id = $this->get_slide_id_from_url( $url );

		$query = sprintf( "
			DELETE FROM %s%s WHERE user_id = %s AND module_id = %s
		", $wpdb->prefix, self::TABLE_RESUME, self::$user_id, $module_id[1] );

		$query = $wpdb->query( $query );
	}
}
