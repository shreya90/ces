<?php
/**
 * Initializing
 *
 * @package    Tin Canny LearnDash Reporting
 * @subpackage TinCan Module
 * @author     Uncanny Owl
 * @since      1.0.0
 *
 * @TODO Admin Table Header RWD
 * @TODO Mark Complete Hooks
 */

namespace UCTINCAN;

if ( !defined( "ABSPATH" ) ) {
	header( "Status: 403 Forbidden" );
	header( "HTTP/1.1 403 Forbidden" );
	exit();
}

class Init {
	// Constants
	const TINCAN_URL_KEY    = 'ucTinCan';
	const TABLE_VERSION_KEY = 'UncannyOwl TinCanny DB Version';

	// Instances
	public static $TinCan;

	// Endpoint URL
	public static $endpint_url;

	// Upgraded Commited
	private static $done_upgraded = false;

	/**
	 * Constructor
	 *
	 * @access public
	 * @return void
	 * @since  1.0.0
	 */
	public function __construct() {
		add_action( 'rest_api_init', array( $this, 'reporting_api' ) );

		$this->load_tincan_api();
		$this->set_objects();
		$this->create_hooks();

		$this->check_upgrade();
	}

	/**
	 * Upgrade
	 *
	 * @access private
	 * @return void
	 * @since  1.3.9
	 */
	private function check_upgrade() {
		if ( self::$done_upgraded )
			return;

		// If Option doesn't Exists
		if ( get_option( self::TABLE_VERSION_KEY ) != UNCANNY_REPORTING_VERSION ) {
			$database = new Database();
			$database->upgrade();
		}

		self::$done_upgraded = true;
	}

	/**
	 * Load TinCan API
	 *
	 * @access private
	 * @return void
	 * @since  1.0.0
	 */
	private function load_tincan_api() {
		require_once( UCTINCAN_PLUGIN_DIR . 'vendors/tincan_api/autoload.php' );
	}

	/**
	 * Set Objects
	 *
	 * @access private
	 * @return void
	 * @since  1.0.0
	 */
	private function set_objects() {
		new Server();
		new Services();

		// For Edge
		header( "Content-Security-Policy: script-src * 'self' 'unsafe-inline' 'unsafe-eval'" );

		if ( is_admin() ) {
			new Admin\Metabox();
			new Admin\WP_UserProfile();
		}
	}

	/**
	 * Create Hooks
	 *
	 * @access private
	 * @return void
	 * @since  1.0.0
	 */
	private function create_hooks() {
		add_action( "init", array( $this, "set_objects_on_init" ), 100 );
		add_action( "init", array( $this, "activate_h5p_xapi" ), 110 );

		// Admin Ajax
		add_action( 'wp_ajax_GET_Modules', array( $this, 'print_modules_form_from_URL_parameter' ) );
	}

	/**
	 * Set Objects on init Hooking Point
	 *
	 * @access public
	 * @return void
	 * @since  1.0.0
	 */
	public function set_objects_on_init() {
		$permalink = get_option( 'permalink_structure' );
		$pathinfo  = '';

		if ( strstr( $permalink, 'index.php' ) )
			$pathinfo = 'index.php/';

		self::$endpint_url = get_bloginfo( 'wpurl' ) . '/' . $pathinfo . self::TINCAN_URL_KEY;
		self::$TinCan      = new \TinCan\RemoteLRS( self::$endpint_url, '1.0.1', 0, 0 );
	}

	public function reporting_api() {
		$controller = new RestEndpoint();
		$controller->register_routes();
	}

	/**
	 * Set H5P xAPI if doesn't exist
	 *
	 * @access public
	 * @return void
	 * @since  1.0.0
	 */
	public function activate_h5p_xapi() {
		if ( has_action( 'admin_menu', 'h5pxapi_admin_menu' ) )
			return;

		include_once( dirname( dirname( dirname( __FILE__ ) ) ) . '/h5p-xapi/wp-h5p-xapi.php' );
		remove_action( 'admin_menu', 'h5pxapi_admin_menu' );

		$endpoint_url = get_option( "h5pxapi_endpoint_url" );
		$username     = get_option( "h5pxapi_username" );
		$password     = get_option( "h5pxapi_password" );

		if ( $endpoint_url != self::$endpint_url )
			update_option( 'h5pxapi_endpoint_url', self::$endpint_url );

		if ( empty( $username ) )
			update_option( 'h5pxapi_username', 1 );

		if ( empty( $password ) )
			update_option( 'h5pxapi_password', 1 );
	}

	/**
	 * Ajax Callback For Admin Module <option>s
	 *
	 * @access public
	 * @return void
	 * @since  1.0.0
	 */
	public function print_modules_form_from_URL_parameter() {
		$database = new Database\Admin();
		$database->print_modules_form_from_URL_parameter();

		wp_die();
	}
}
