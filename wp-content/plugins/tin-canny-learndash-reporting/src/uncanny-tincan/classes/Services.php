<?php
/**
 * Services : H5P and Storyline
 *
 * @package    Tin Canny LearnDash Reporting
 * @subpackage TinCan Module
 * @author     Uncanny Owl
 * @since      1.0.0
 * @todo       descriptions
 */

namespace UCTINCAN;

use uncanny_learndash_reporting\Config;

if ( !defined( "ABSPATH" ) ) {
	header( "Status: 403 Forbidden" );
	header( "HTTP/1.1 403 Forbidden" );
	exit();
}

class Services {
	use Modules;

	private static $H5P_SUPPORTED_CONTENT_TYPES = array(
		'H5P.Summary',
		'H5P.SingleChoiceSet',
		'H5P.QuestionSet',
		'H5P.MultiChoice',
		'H5P.MemoryGame',
		'H5P.MarkTheWords',
		'H5P.ImageHotspotQuestion',
		'H5P.Blanks',
		'H5P.DragText',
		'H5P.DragQuestion',
		'H5P.Collage',
		'H5P.Boardgame',
		'H5P.InteractiveVideo',
	);

	private static $snc_upload_dir;

	public function __construct() {
		if ( ! is_admin() )
			add_action( 'wp', array( $this, 'activate_mark_complete_control' ) );

		# Ajax (Storyline)
		add_action( 'wp_ajax_Check Storyline Completion',      array( $this, 'ajax_check_slide_completion' ) );
		add_action( 'wp_ajax_Check Captivate Completion',      array( $this, 'ajax_check_slide_completion' ) );
		add_action( 'wp_ajax_Check iSpring Completion',        array( $this, 'ajax_check_slide_completion' ) );
		add_action( 'wp_ajax_Check ArticulateRise Completion', array( $this, 'ajax_check_slide_completion' ) );

		add_action( 'wp_ajax_Check H5P Completion', array( $this, 'ajax_check_h5p_completion' ) );
	}

	// wp
	public function activate_mark_complete_control() {
		global $post;

		if ( empty( $post ) )
			return;

		$post_option   = ( is_object( $post ) ) ? get_post_meta( $post->ID, '_WE-meta_', true ) : array();
		$global_option = get_option( 'disable_mark_complete_for_tincan', 'yes' );


		if( "" === $post_option ){
			$post_option = array();
		}
		// Post Option Default : Use Global Setting
		if ( ! is_array( $post_option ) || ! isset( $post_option['restrict-mark-complete'] ) )
			$post_option['restrict-mark-complete'] = 'Use Global Setting';

		switch( $post_option['restrict-mark-complete'] ) {
			case 'Use Global Setting' :
				if ( $global_option == 'no' )
					return;
			break;

			case 'No' :
				return;
			break;
		}

		// trait: Modules
		if ( ! $this->prepare_modules() )
			return;

		global $post;

		if ( is_a( $post, 'WP_Post' ) &&
			(
				has_shortcode( $post->post_content, 'h5p' ) ||
				has_shortcode( $post->post_content, 'vc_snc')
			)
        ) {
			# Extract H5P, Storyline Shortcode
			add_filter( 'the_content', array( $this, 'extract_shortcuts' ) );

			# Script
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

			# Set js var for completion
			add_action( 'wp_footer', array( $this, 'print_js_completion_vars' ) );
		}

	}

	private function get_snc_upload_dir() {
		if ( ! self::$snc_upload_dir ) {
			$wp_upload_dir = wp_upload_dir();
			self::$snc_upload_dir = $wp_upload_dir['basedir'] . '/' . SnC_UPLOAD_DIR_NAME;
		}

		return self::$snc_upload_dir;
	}

	// Extract H5P and Storyline/Articulate Shortcodes
	// the_content
	public function extract_shortcuts( $content ) {
		preg_match_all( self::$PATTERN_SHORTCODE_ID, $content, $match_id );

		if ( empty( $match_id[4] ) )
			return $content;

		foreach( $match_id[4] as $key => $content_id ) {
			if ( empty( $content_id ) || !is_numeric( $content_id ) )
				continue;

			switch( $match_id[1][ $key ] ) {
				case 'h5p' :
					if ( $this->available[ 'H5P' ] )
						$this->set_h5p_module_info( $content_id );

					break;

				case 'vc_snc' :
					if ( $this->available[ 'SnC' ] )
						$this->set_slide_module_info( $content_id );

					break;
			}
		}

		$this->set_completion();

		return $content;
	}

	private function set_h5p_module_info( $id ) {
		global $wpdb;

		$query = sprintf( "
			SELECT library.`name` FROM %s%s contents
			INNER JOIN %s%s library ON contents.`library_id` = library.`id`
			WHERE contents.`id` = %s
			",
			$wpdb->prefix,
			self::$TABLE_H5P_CONTENTS,
			$wpdb->prefix,
			self::$TABLE_H5P_LIBRARY,
			$id
		);

		$library_name = $wpdb->get_var( $query );

		if ( in_array( $library_name, self::$H5P_SUPPORTED_CONTENT_TYPES ) )
			$this->module_info[ 'H5P' ][ 'contents' ][ $id ] = $id;
	}

	// todo Check Availability into snc classes
	private function set_slide_module_info( $item_id ) {
		$Module = \TINCANNYSNC\Module::get_module( $item_id );

		if ( $Module && $Module->is_available() ) {
			$module_type = $Module->get_type();
			$this->module_info[ $module_type ][ 'contents' ][ $item_id ] = $item_id;
		}
	}

	private function set_completion() {
		global $post;
		$user_id = get_current_user_id();
		$course_id = false;

		if ( !$user_id ) return;

		if ( function_exists( 'learndash_get_course_id' ) )
			$course_id = learndash_get_course_id( $post->ID );

		$database = new Database\Completion();

		// H5P Completion
		foreach( $this->module_info[ 'H5P' ][ 'contents' ] as $id )
			if ( $database->get_H5P_completion( $id, $course_id, $post->ID ) )
				$this->module_info[ 'H5P' ][ 'complete' ][ $id ] = true;

		$module_types = array( 'Storyline', 'Captivate', 'iSpring', 'ArticulateRise' );

		foreach( $module_types as $type ) {
			foreach( $this->module_info[ $type ][ 'contents' ] as $id ) {
				$Module = \TINCANNYSNC\Module::get_module( $id );

				if ( $Module->is_available() ) {
					$module_url  = $Module->get_url();

					if ( $database->get_completion_by_URL( $module_url, $course_id, $post->ID ) )
						$this->module_info[ $type ][ 'complete' ][ $id ] = true;
				}
			}
		}
	}

	# TODO : Course ID
	public function ajax_check_slide_completion() {
		$completion = $this->check_slide_completion( $_POST[ 'URL' ] );

		if ( $completion )
			echo $completion;

		wp_die();
	}

	public function check_slide_completion( $url ) {
		$database  = new Database\Completion();
		$course_id = false;

		$url_converted = remove_query_arg( array( 'endpoint', 'auth', 'activity_id', 'actor', 'client', 'tincan' ), $url );

		parse_str( $url, $decoded );

		if ( $lesson_id = $this->get_lesson_id_from_url( $url ) ) {
			if ( function_exists( 'learndash_get_course_id' ) )
				$course_id = learndash_get_course_id( $lesson_id );
		}

		if ( $database->get_completion_by_URL( $url_converted, $course_id, $lesson_id ) ) {
			$matches = $this->get_slide_id_from_url( $url_converted );
			return $matches[1];
		}

		return false;
	}

	public function ajax_check_h5p_completion() {
		echo $this->check_h5p_completion( $_POST[ 'lesson_id' ] );
		wp_die();
	}

	public function check_h5p_completion( $lesson_id ) {
		$database = new Database\Completion();
		return $database->get_completion_by_lesson_meta( $lesson_id );
	}

	public function check_h5p_completion_( $snc_id, $lesson_id, $user_id = false ) {
		$database = new Database\Completion();
		return $database->get_H5P_completion( $snc_id, false, $lesson_id, $user_id );
	}

	public function enqueue_scripts() {
		wp_enqueue_script( 'tincanny-hooks', Config::get_gulp_js( 'hooks' ), array( 'jquery' ) );
		wp_enqueue_script( 'tincanny-modules', Config::get_gulp_js( 'modules' ), array( 'jquery', 'tincanny-hooks' ), '', true );
	}

	public function print_js_completion_vars() {
		global $post;

		$has_complete_setting = 0;
		$post_meta = ( $post->ID ) ? get_post_meta( $post->ID, '_WE-meta_', true ) : array();

		if ( !empty( $post_meta[ 'completion-condition' ] ) )
			$has_complete_setting = 1;

		?>
		<script type="text/javascript">
		jQuery(document).ready(function() {
			<?php foreach( $this->modules as $module ) : ?>

			tincannyModuleController.count[ '<?php echo $module ?>' ]      = <?php echo count( $this->module_info[ $module ][ 'contents' ] ) ?>;
			tincannyModuleController.completion[ '<?php echo $module ?>' ] = JSON.parse( '<?php echo json_encode( $this->module_info[ $module ][ 'complete' ] ) ?>' );

			<?php endforeach; ?>
			tincannyModuleController.hasCompletionSetting = <?php echo $has_complete_setting ?>;
		});

		var wp_ajax_url = '<?php echo admin_url( 'admin-ajax.php' ) ?>';
		</script>
		<?php
	}

	/**
	 * Writing Logs
	 *
	 * @access private
	 * @param  mixed $value
	 * @return void
	 * @since  1.0.0
	 */
	private function write_log( $value ) {
		Database::WriteLog( $value );
	}
}
