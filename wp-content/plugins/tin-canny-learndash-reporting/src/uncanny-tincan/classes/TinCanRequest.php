<?php
/**
 * Processing Request
 *
 * @package    Tin Canny LearnDash Reporting
 * @subpackage TinCan Module
 * @author     Uncanny Owl
 * @since      1.0.0
 */

namespace UCTINCAN;

if ( ! defined( "ABSPATH" ) ) {
	header( "Status: 403 Forbidden" );
	header( "HTTP/1.1 403 Forbidden" );
	exit();
}

abstract class TinCanRequest {
	use \UCTINCAN\Modules;

	/**
	 * TinCan Objects
	 *
	 * @access protected
	 * @since  1.0.0
	 */
	protected $TC_Agent, $TC_Actitity, $TC_Context, $TC_Verb, $TC_Result;

	/**
	 * Content ID
	 *
	 * @access protected
	 * @since  1.0.0
	 */
	protected $content_id;

	protected $lesson_id;
	protected $user_id;

	/**
	 * Set TinCan Objects from Requested Array
	 *
	 * @access protected
	 *
	 * @param  array $decoded
	 *
	 * @return void
	 * @since  1.0.0
	 */
	protected function init_tincan_objects( $decoded ) {
		$this->TC_Agent    = ( ! empty( $decoded['actor'] ) ) ? new \TinCan\Agent( $decoded['actor'] ) : new \TinCan\Agent();
		$this->TC_Actitity = ( ! empty( $decoded['object'] ) ) ? new \TinCan\Activity( $decoded['object'] ) : new \TinCan\Activity();
		$this->TC_Context  = ( ! empty( $decoded['context'] ) ) ? new \TinCan\Context( $decoded['context'] ) : new \TinCan\Context();
		$this->TC_Verb     = ( ! empty( $decoded['verb'] ) ) ? new \TinCan\Verb( $decoded['verb'] ) : new \TinCan\Verb();

		if ( isset( $decoded['result'] ) ) {
			$result = $decoded['result'];

			if ( isset( $result['score'] ) && is_array( $result['score'] ) && isset( $result['score']['min'] ) && isset( $result['score']['max'] ) && $result['score']['min'] >= $result['score']['max'] ) {
				unset( $result['score'] );
			}
		}

		$this->TC_Result = ( ! empty( $result ) ) ? new \TinCan\Result( $result ) : new \TinCan\Result();

		return $this->TC_Agent->getMbox();
	}

	/**
	 * Save Data
	 *
	 * @access protected
	 * @return bool
	 * @since  1.0.0
	 */
	protected function save() {

		global $post;

		if ( ! $this->TC_Agent ) {
			return;
		}

		// Agent
		$userEmail = $this->TC_Agent->getMbox();

		if ( ! $userEmail ) {
			return;
		}

		$userEmail = str_replace( 'mailto:', '', $userEmail );
		$wpUser    = get_user_by( 'email', $userEmail );

		if ( ! $wpUser->ID ) {
			return;
		}

		$user_id       = $wpUser->ID;
		$this->user_id = $user_id;

		// Lesson, Course, Group
		$grouping = $this->TC_Context->getContextActivities()->getGrouping();
		$grouping = array_pop( $grouping );

		$lesson_id       = $this->get_lesson_id( $grouping );
		$this->lesson_id = $lesson_id;
		$course_id       = ( $grouping ) ? learndash_get_course_id( $lesson_id ) : 0;

		// If Lesson is shared
		if ( empty( $course_id ) ) {
			$course_id = get_user_meta( $user_id, 'tincan_last_known_ld_course', true );
		}

		$group_id  = $this->get_learndash_user_enrolled_group_id( $wpUser->ID, $course_id );
		$group_id  = ( $group_id ) ? $group_id : 0;
		$course_id = ( $course_id ) ? $course_id : 0;

		\uncanny_learndash_reporting\Config::log( array(
			$grouping->getId(),
			$lesson_id,
			$group_id,
			learndash_get_course_id( $lesson_id ),
			$course_id,
			$grouping
		), 'save() data', true, 'tincan' );

		// Verb
		$verb = array_filter( explode( '/', $this->TC_Verb->getId() ) );
		$verb = array_pop( $verb );
		$verb = strtolower( $verb );

		// Module and Target
		extract( $this->get_module_and_target() );

		// Result
		$completion = false;
		$result     = false;
		$maximum    = false;

		if ( $this->TC_Result->getScore() ) {
			if ( ! is_null( $this->TC_Result->getScore()->getScaled() ) ) {
				$result  = $this->TC_Result->getScore()->getScaled() * 100;
				$maximum = 100;
			}

			if ( $result === false && $this->TC_Result->getScore()->getRaw() ) {
				$result = $this->TC_Result->getScore()->getRaw();
			}
		}

		if ( ! is_null( $this->TC_Result->getSuccess() ) ) {
			$completion = ( $this->TC_Result->getSuccess() ) ? 1 : 0;
		}

		if ( ! $verb ) {
			return;
		}

		// Save
		$database = new Database();
		$database->set_report( $group_id, $course_id, $lesson_id, $module, $module_name, $target, $target_name, $verb, $result, $maximum, $completion, $user_id );

		return compact( 'group_id', 'course_id', 'lesson_id', 'module', 'module_name', 'target', 'target_name', 'verb', 'result', 'maximum', 'completion', 'user_id' );
	}

	/**
	 * Get Module and Target
	 *
	 * @access abstract protected
	 * @return void
	 * @since  1.0.0
	 */
	abstract protected function get_module_and_target();

	/**
	 * Get Group ID from Course ID
	 *
	 * @access private
	 *
	 * @param  int $user_id
	 * @param  int $course_id
	 *
	 * @return void
	 * @since  1.0.0
	 */
	private function get_learndash_user_enrolled_group_id( $user_id, $course_id ) {
		$group_ids = learndash_get_users_group_ids( $user_id );

		foreach ( $group_ids as $group_id ) {
			if ( learndash_group_has_course( $group_id, $course_id ) ) {
				return $group_id;
			}
		}

		return false;
	}

	private function get_lesson_id( $grouping ) {
		if ( ! $grouping )
			return 0;

		if ( ! $grouping->getId() )
			return 0;

		if ( url_to_postid( $grouping->getId() ) )
			return url_to_postid( $grouping->getId() );

		parse_str( $grouping->getId(), $request_url );
		$queried_post = get_page_by_path( array_pop($request_url), OBJECT, array( 'post', 'sfwd-quiz', 'sfwd-lessons', 'sfwd-topic' ) );

		if ( $queried_post && $queried_post->ID )
			return $queried_post->ID;

		return 0;

	}

	/**
	 * Write Log
	 *
	 * @access protected
	 *
	 * @param  mixed $value
	 *
	 * @return void
	 * @since  1.0.0
	 */
	protected function write_log( $value ) {
		Database::WriteLog( $value );
	}
}
