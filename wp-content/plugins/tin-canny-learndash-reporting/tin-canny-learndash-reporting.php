<?php
/*
Plugin Name:  Tin Canny LearnDash Reporting
Version: 2.4
Description: Add a Tin Can xAPI Learning Record Store (LRS) inside WordPress with powerful reporting tools for LearnDash and Tin Can statements.
Author: Uncanny Owl
Author URI: www.uncannyowl.com
Plugin URI: https://www.uncannyowl.com/tin-can-lrs-learndash-report-toolkit/
Text Domain: uncanny-learndash-reporting
Domain Path: /languages
*/

// All Class instance are store in Global Variable $uncanny_learndash_reporting
global $uncanny_learndash_reporting;

// Define version
if ( ! defined( 'UNCANNY_REPORTING_VERSION' ) ) {
	define( 'UNCANNY_REPORTING_VERSION', '2.4' );
}

if ( ! defined( 'UO_REPORTING_FILE' ) ) {
	define( 'UO_REPORTING_FILE', __FILE__ );
}

if ( ! defined( 'UO_REPORTING_DEBUG' ) ) {
	define( 'UO_REPORTING_DEBUG', false );
}

// Show admin notices for minimum versions of PHP, WordPress, and LearnDash
add_action( 'admin_notices', 'uo_reporting_learndash_version_notice' );

function uo_reporting_learndash_version_notice() {

	global $wp_version;

	//Minimum versions
	$wp         = '4.0';
	$php        = '5.3';
	$learn_dash = '2.1';

	// Set LearnDash version
	$learn_dash_version = 0;
	if ( defined( 'LEARNDASH_VERSION' ) ) {
		$learn_dash_version = LEARNDASH_VERSION;
	}

	// Get current screen
	$screen = get_current_screen();

	if ( ! version_compare( PHP_VERSION, '5.3', '>=' ) && ( isset( $screen ) && 'plugins.php' === $screen->parent_file ) ) {

		// Show notice if php version is less than 5.3 and the current admin page is plugins.php
		$version = $php;
		$current = PHP_MAJOR_VERSION . '.' . PHP_MINOR_VERSION . '.' . PHP_RELEASE_VERSION;

		?>
		<div class="notice notice-error">
			<h3><?php echo sprintf(

					esc_html__( 'The %s requires PHP version %s or higher (5.6 or higher is recommended).
						Because you are using an unsupported version of PHP (%s), the Reporting plugin will not initialize.
						Please contact your hosting company to upgrade to PHP 5.6 or higher.', 'uncanny-learndash-reporting' ),

					'Uncanny LearnDash Reporting', $version, $current ); ?>
			</h3>
		</div>
		<?php

	} elseif ( version_compare( $wp_version, $wp, '<' ) && ( isset( $_REQUEST['page'] ) && 'uncanny-learnDash-reporting' === $_REQUEST['page'] ) ) {

		// Show notice if WP version is less than 4.0 and the current page is the Reporting settings page
		$flag    = 'WordPress';
		$version = $wp;
		$current = $wp_version;

		?>
		<!-- No Notice Style below WordPress -->
		<style>
			.notice-error {
				border-left-color: #dc3232 !important;
			}

			.notice {
				background: #fff;
				border-left: 4px solid #fff;
				-webkit-box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
				box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
				margin: 5px 15px 2px;
				padding: 1px 12px;
			}
		</style>
		<div class="notice notice-error">
			<h3><?php echo sprintf(

					esc_html__( 'The %s plugin requires %s version %s or greater. Your current version is %s.', 'uncanny-learndash-reporting' ),

					'Uncanny LearnDash Reporting', $flag, $version, $current ); ?>
			</h3>
		</div>
		<?php

	} elseif ( ! version_compare( $learn_dash_version, $learn_dash, '>=' ) && ( isset( $_REQUEST['page'] ) && 'uncanny-learnDash-reporting' === $_REQUEST['page'] ) ) {

		// Show notice if LearnDash is less than 2.1 and the current page is the Reporting settings page
		if ( 0 !== $learn_dash_version ) {

			?>
			<div class="notice notice-error">
				<h3><?php echo sprintf(

						esc_html__( 'Uncanny LearnDash Reporting requires LearnDash version 2.1 or higher to work properly.
						Please make sure you have LearnDash version 2.1 or higher installed. Your current version is: %s', 'uncanny-learndash-reporting' ),

						$learn_dash_version ); ?>
				</h3>
			</div>
			<?php

		} elseif ( ! class_exists( 'SFWD_LMS' ) ) {

			?>
			<div class="notice notice-error">
				<h3><?php echo sprintf(

						esc_html__( 'Uncanny LearnDash reporting requires LearnDash version 2.1 or higher to work properly.
						Please make sure you have LearnDash version 2.1 or higher installed.', 'uncanny-learndash-reporting' ),

						$learn_dash_version ); ?>
				</h3>
			</div>
			<?php

		}

	}
}

// Allow Translations to be loaded
add_action( 'plugins_loaded', 'uncanny_learndash_reporting_text_domain' );

function uncanny_learndash_reporting_text_domain() {
	load_plugin_textdomain( 'uncanny-learndash-reporting', false, basename( dirname( __FILE__ ) ) . '/languages/' );
}

add_action( 'wp', 'tc_last_known_course' );

function tc_last_known_course() {


	$user = wp_get_current_user();

	if ( is_user_logged_in() ) {

		/* declare $post as global so we get the post->ID of the current page / post */
		global $post;

		/* Limit the plugin to LearnDash specific post types */
		$learn_dash_post_types =
			array(
				'sfwd-courses',
				'sfwd-lessons',
				'sfwd-topic',
				'sfwd-quiz',
				'sfwd-assignment'
			);

		if ( is_singular( $learn_dash_post_types ) ) {
			update_user_meta( $user->ID, 'tincan_last_known_ld_module', $post->ID );
			$course_id = learndash_get_course_id($post);
			update_user_meta( $user->ID, 'tincan_last_known_ld_course', $course_id );
			\uncanny_learndash_reporting\Config::log( array(
				$post->ID,
				$course_id,
			), 'learndash_last_known_page', true, 'tincan' );
		}
	}
}

// PHP version 5.3 and up only
if ( version_compare( PHP_VERSION, '5.3', '>=' ) ) {

	// On first activation, redirect to reporting settings page if min php version is met
	register_activation_hook( __FILE__, 'uncanny_learndash_reporting_plugin_activate' );
	add_action( 'admin_init', 'uncanny_learndash_reporting_plugin_redirect' );

	function uncanny_learndash_reporting_plugin_activate() {
		update_option( 'uncanny_learndash_ reporting_plugin_do_activation_redirect', 'yes' );

	}

	function uncanny_learndash_reporting_plugin_redirect() {
		if ( 'yes' === get_option( 'uncanny_learndash_ reporting_plugin_do_activation_redirect', 'no' ) ) {

			update_option( 'uncanny_learndash_ reporting_plugin_do_activation_redirect', 'no' );

			if ( ! isset( $_GET['activate-multi'] ) ) {
				wp_redirect( admin_url( 'admin.php?page=uncanny-reporting-license-activation' ) );
			}
		}
	}

	// Add settings link on plugin page
	$uncanny_learndash_reporting_plugin_basename = plugin_basename( __FILE__ );

	add_filter( 'plugin_action_links_' . $uncanny_learndash_reporting_plugin_basename, 'uncanny_learndash_reporting_plugin_settings_link' );

	function uncanny_learndash_reporting_plugin_settings_link( $links ) {
		$settings_link = '<a href="' . admin_url( 'admin.php?page=uncanny-learnDash-reporting' ) . '">Settings</a>';
		array_unshift( $links, $settings_link );

		return $links;
	}

	if ( defined( 'LEARNDASH_VERSION' ) ) {
		/* Load Reporting */
		// Plugins Configurations File
		include_once( dirname( __FILE__ ) . '/src/config.php' );

		// Load all plugin classes(functionality)
		include_once( dirname( __FILE__ ) . '/src/boot.php' );

		$boot                              = 'uncanny_learndash_reporting\Boot';
		$uncanny_learndash_reporting_class = new $boot;

		/* Load Storyline/Captivate*/
		include_once( dirname( __FILE__ ) . '/src/uncanny-articulate-and-captivate/storyline-and-captivate.php' );

		/* Load Storyline/Captivate*/
		include_once( dirname( __FILE__ ) . '/src/uncanny-tincan/uncanny-tincan.php' );
	} else {

		add_action( 'admin_notices', 'uo_reporting_learndash_not_activated' );


	}
	function uo_reporting_learndash_not_activated() {
		?>
		<div class="notice notice-error">
			<h4>Warning: Tin Canny LearnDash Reporting requires LearnDash. Please install LearnDash before using the plugin.</h4>
		</div>
		<?php

	}

}
