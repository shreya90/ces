<?php
/**
 * Displays a user's profile.
 *
 * Available Variables:
 *
 * $user_id 		: Current User ID
 * $current_user 	: (object) Currently logged in user object
 * $user_courses 	: Array of course ID's of the current user
 * $quiz_attempts 	: Array of quiz attempts of the current user
 *
 * @since 2.1.0
 *
 * @package LearnDash\User
 */
?>

<?php

function get_url($id) {
    global $wpdb;
    $image = $wpdb->get_row("select wp_postmeta.meta_value from wp_postmeta where wp_postmeta.post_id ='$id' and wp_postmeta.meta_key ='_thumbnail_id'");
    return $image;
}

function my_account($user_id) {
    global $wpdb;
//$user_id = $_REQUEST['user_id'];
    $query = "select wp_posts.ID, wp_postmeta.meta_key, wp_postmeta.meta_value from wp_posts inner join wp_postmeta where wp_posts.post_author = '$user_id' and wp_postmeta.post_id = wp_posts.ID and wp_posts.post_type = 'sfwd-transactions'";

    $account = $wpdb->get_results($query);
//print_r($account);
    $accounts_data = array();
    $i = 0;

    foreach ($account as $accounts) {
        if ($accounts->meta_key == 'stripe_name' || $accounts->meta_key == 'stripe_price' || $accounts->meta_key == 'stripe_price_type' || $accounts->meta_key == '_edit_lock') {
            $accounts_data[$i]['ID'] = $accounts->ID;
            $accounts_data[$i]['meta_key'] = $accounts->meta_key;
            $accounts_data[$i]['meta_value'] = $accounts->meta_value;
            $i++;
        }
    }
    foreach ($accounts_data as $element) {
        $result[$element['ID']][] = $element;
    }
    return $result;
}
?>

<div  class="row">
    <div id="course-left-sidebar" class="col-md-3">
        <div class="course-image-widget">
<?php echo get_avatar($user_id, 262, '', $current_user->display_name, array('class' => 'img-responsive')) ?>
        </div><!-- end image widget -->
        <div class="course-meta">
            <p><?php echo $current_user->display_name ?></p>
            <hr>
            <!--<p><?php esc_html_e('My Profile', 'learnplus') ?><small class="label label-primary"><a href="<?php echo get_edit_user_link() ?>"><?php esc_html_e('Edit', 'learnplus') ?></a></small></p>-->
            <p><?php esc_html_e('My Profile', 'learnplus') ?><small class="label label-primary" style="cursor:pointer;" onclick='get_profile()'>Edit</small></p>
            <!--<p><?php esc_html_e('My Profile', 'learnplus') ?><small><a href="<?php echo 'http://192.168.1.140/ces2/registration/' ?>"><?php esc_html_e('Edit', 'learnplus') ?></a></small></p>-->
            <hr>
            <p><?php esc_html_e('Email', 'learnplus') ?><small><?php echo $current_user->user_email; ?></small></p>
            <hr>
            <p><?php esc_html_e('My Courses', 'learnplus') ?><small class="label label-primary" onclick='get_course()'><?php echo count($user_courses) ?></small></p>
            <hr>
<!--            <p><?php esc_html_e('Completed Quiz', 'learnplus') ?><small class="label label-primary"><?php echo count($quiz_attempts, 1) ?></small></p>
            <hr>-->
            <p><?php esc_html_e('Purchase History', 'learnplus') ?><small class="label label-primary" style="cursor:pointer;" onclick='get_history()'>View</small></p>
            <hr>
            <p><?php esc_html_e('Change Password', 'learnplus') ?><small class="label label-primary" style="cursor:pointer;" onclick='get_password()'>Change</small></p>

        </div><!-- end meta -->
    </div>




    <div id="course-content" class="col-md-9">
        <div class="course-description" id="learndash_profile">
            <h3 class="course-title"><?php esc_html_e('Courses', 'learnplus') ?></h3>
            <p><?php echo $current_user->description ?></p>
            <div><div style="float: left;margin: 10px; color: white;"><button  class = "course-meta" onclick='get_allcourse()'>All course</button></div>
                <div style="float: left;margin: 10px; color: white;"><button  class = "course-meta" onclick='get_inprogress()'>In Progress</button></div>
                <div style="float: left;margin: 10px;color: white;"><button  class = "course-meta" onclick='get_comleted()'>Completed</button></div>
            </div>
            <div class="learndash_profile_heading no_radius clear_both">
                <span><?php _e('Registered Courses', 'learnplus'); ?></span>
                <!--<span class="ld_profile_status"><?php _e('Status', 'learnplus'); ?></span>-->
            </div>

            <div id="course_list">
                <?php if (!empty($user_courses)) : ?>

                    <?php foreach ($user_courses as $course_id) : ?>
                        <?php
                        $course = get_post($course_id);
                       // print_r($course);
                        $id = $course->ID;
                        $images = get_url($id);
                        $imageurl = $images->meta_value;
                        //print_r($imageurl);
                        $course_link = get_permalink($course_id);

                        $progress = learndash_course_progress(array(
                            'user_id' => $user_id,
                            'course_id' => $course_id,
                            'array' => true
                        ));

                        $status = ( $progress['percentage'] == 100 ) ? 'completed' : 'notcompleted';
                        ?>
                        <div class="ld_course_grid col-sm-3 col-md-4 post-9117 sfwd-courses type-sfwd-courses status-publish has-post-thumbnail hentry xyz" style="height: 320px !important;
                             width: 275px !important;">                
                            <div id='course-<?php echo esc_attr($user_id) . '-' . esc_attr($course->ID); ?>' >

                                <div class="shop-item-list entry">
                                    <img style="height: 140px !important;width: 275px !important;" src="<?php echo wp_get_attachment_url($imageurl); ?>">
                                    <div class="shop-item-title clearfix" style="padding: 0px;">
                                        <h4><a style="color: black;font-size: inherit; padding: 6px 7px 5px;" href="<?php echo esc_attr($course_link); ?>"><?php echo $course->post_title; ?></a></h4>
                                        <div>
                                            <dd class="course_progress" style="width: 95%;" title='<?php echo sprintf(esc_html__('%s out of %s steps completed', 'learnplus'), $progress['completed'], $progress['total']); ?>'>
                                                <div class="course_progress_blue" style='width: <?php echo esc_attr($progress['percentage']); ?>%;'>
                                            </dd>

                                            <div class="right abc">
                                                <?php echo sprintf(esc_html__('%s%% Complete', 'learnplus'), $progress['percentage']);
                                                ?>
                                            </div>

                                        </div>


        <?php if (!empty($quiz_attempts[$course_id])) : ?>
                                            <div class="learndash_profile_quizzes clear_both">

                                                <!--											<div class="learndash_profile_quiz_heading">
                                                                                                                                                <div class="quiz_title"><?php _e('Quizzes', 'learnplus'); ?></div>
                                                                                                                                                <div class="certificate"><?php _e('Certificate', 'learnplus'); ?></div>
                                                                                                                                                <div class="scores"><?php _e('Score', 'learnplus'); ?></div>
                                                                                                                                                <div class="quiz_date"><?php _e('Date', 'learnplus'); ?></div>
                                                                                                                                        </div>-->
                                                <!--
                                                <?php /* foreach ($quiz_attempts[$course_id] as $k => $quiz_attempt) : ?>
                                                  <?php /*
                                                  $certificateLink = @$quiz_attempt['certificate']['certificateLink'];

                                                  $status = empty($quiz_attempt['pass']) ? 'failed' : 'passed';

                                                  $quiz_title = !empty($quiz_attempt['post']->post_title) ? $quiz_attempt['post']->post_title : @$quiz_attempt['quiz_title'];

                                                  $quiz_link = !empty($quiz_attempt['post']->ID) ? get_permalink($quiz_attempt['post']->ID) : '#';
                                                  ?>
                                                  <?php if (!empty($quiz_title)) : */ ?>
                                                                                                        <div class='<?php echo esc_attr($status); ?>'>-->

                                                <!--														<div class="quiz_title">
                                                                                                                                                                        <span class='<?php echo esc_attr($status); ?>_icon'></span>
                                                                                                                                                                        <a href='<?php echo esc_attr($quiz_link); ?>'><?php echo esc_attr($quiz_title); ?></a>
                                                                                                                                                                </div>-->
                                                <?php $link = learndash_get_course_certificate_link($course_id, $user_id) ?>
                                                    <?php if (!empty($link)) : ?>
                                                    <div class="certificate" style="background-color: #2c69cf;">
                <?php if (!empty($link)) : ?>
                                                            <a style="color:white;font-size: 14px;" href='<?php echo $link; ?>&time=<?php echo esc_attr($quiz_attempt['time']) ?>' target="_blank">
                                                                Download Certificate</a>
                                                        <?php else : ?>
                                                            <?php echo ''; ?>
                <?php endif; ?>

                                                    </div><?php endif; ?>

                                                                                                                                                                                                <!--<div class="scores"><?php echo round($quiz_attempt['percentage'], 2); ?>%</div>-->

                                                                                                                                                                                                <!--<div class="quiz_date"><?php echo date_i18n('d-M-Y', $quiz_attempt['time']); ?></div>-->

                                                <!--</div>-->
                                                <?php /* endif; ?>
                                                  <?php endforeach; */ ?>
                                            </div>
        <?php endif; ?>                        




                                        <div class="shopmeta">
                                            <?php if ($students) : ?>
                                                <span class="pull-left"><?php echo $students . ' ' . esc_attr__('Students', 'learnplus') ?></span>
                                            <?php endif; ?>

        <?php /* echo LearnPlus_LearnDash::get_rating_html(get_the_ID(), 'pull-right') */?>
                                        </div>
                                        <!-- end shop-meta -->
                                    </div>
                                </div>
                            </div>                         

                                                <!--<div class="list_arrow collapse flippable"><a href='<?php echo esc_attr($course_link); ?>'><?php esc_html_e('View Detail', 'learnplus'); ?></a></div>-->


                            <?php
                            /**
                             * @todo Remove h4 container.
                             */
                            ?>
                            <!--                            <h4>
                                                            <a class='<?php echo esc_attr($status); ?>'  onClick='return flip_expand_collapse("#course-<?php echo esc_attr($user_id); ?>", <?php echo esc_attr($course->ID); ?>);' href='<?php echo esc_attr($course_link); ?>'><?php echo $course->post_title; ?></a>
                            
                                                            <div class="flip" style="display:none;">
                            
                                                                <div class="learndash_profile_heading course_overview_heading"><?php _e('Course Progress Overview', 'learnplus'); ?></div>
                            
                                                                <div>
                                                                    <dd class="course_progress" title='<?php echo sprintf(esc_html__('%s out of %s steps completed', 'learnplus'), $progress['completed'], $progress['total']); ?>'>
                                                                        <div class="course_progress_blue" style='width: <?php echo esc_attr($progress['percentage']); ?>%;'>
                                                                    </dd>
                            
                                                                    <div class="right abc">
        <?php echo sprintf(esc_html__('%s%% Complete', 'learnplus'), $progress['percentage']); ?>
                                                                    </div>
                                                                </div>
                            
        <?php if (!empty($quiz_attempts[$course_id])) : ?>
                                                                                            <div class="learndash_profile_quizzes clear_both">
                                                    
                                                                                                <div class="learndash_profile_quiz_heading">
                                                                                                    <div class="quiz_title"><?php _e('Quizzes', 'learnplus'); ?></div>
                                                                                                    <div class="certificate"><?php _e('Certificate', 'learnplus'); ?></div>
                                                                                                    <div class="scores"><?php _e('Score', 'learnplus'); ?></div>
                                                                                                    <div class="quiz_date"><?php _e('Date', 'learnplus'); ?></div>
                                                                                                </div>
                                                    
                                <?php foreach ($quiz_attempts[$course_id] as $k => $quiz_attempt) : ?>
                                    <?php
                                    $certificateLink = @$quiz_attempt['certificate']['certificateLink'];

                                    $status = empty($quiz_attempt['pass']) ? 'failed' : 'passed';

                                    $quiz_title = !empty($quiz_attempt['post']->post_title) ? $quiz_attempt['post']->post_title : @$quiz_attempt['quiz_title'];

                                    $quiz_link = !empty($quiz_attempt['post']->ID) ? get_permalink($quiz_attempt['post']->ID) : '#';
                                    ?>
                <?php if (!empty($quiz_title)) : ?>
                                                                                                                                                        <div class='<?php echo esc_attr($status); ?>'>
                                                                                                    
                                                                                                                                                            <div class="quiz_title">
                                                                                                                                                                <span class='<?php echo esc_attr($status); ?>_icon'></span>
                                                                                                                                                                <a href='<?php echo esc_attr($quiz_link); ?>'><?php echo esc_attr($quiz_title); ?></a>
                                                                                                                                                            </div>
                                                                                                    
                                                                                                                                                            <div class="certificate">
                    <?php if (!empty($certificateLink)) : ?>
                                                                                                                                                                                            <a href='<?php echo esc_attr($certificateLink); ?>&time=<?php echo esc_attr($quiz_attempt['time']) ?>' target="_blank">
                                                                                                                                                                                                <div class="certificate_icon"></div></a>
                                        <?php else : ?>
                                            <?php echo '-'; ?>
                    <?php endif; ?>
                                                                                                                                                            </div>
                                                                                                    
                                                                                                                                                            <div class="scores"><?php echo round($quiz_attempt['percentage'], 2); ?>%</div>
                                                                                                    
                                                                                                                                                            <div class="quiz_date"><?php echo date_i18n('d-M-Y', $quiz_attempt['time']); ?></div>
                                                                                                    
                                                                                                                                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                                                                            </div>
        <?php endif; ?>
                                                            </div>
                                                        </h4>-->
                        </div>
                    <?php endforeach; ?>
<?php endif; ?>

            </div>




        </div><!-- end desc -->

    </div>



    <div id="purchase_history" class="col-md-9">
<!--        <div class="course-description" id="learndash_profile">
            <h3 class="course-title"><?php esc_html_e('Purchase History', 'learnplus') ?></h3>
            <p><?php echo $current_user->description ?></p>

            <div class="learndash_profile_heading no_radius clear_both">
                <div class ="course" style="float:left; margin-right: 27.5%;">
                    <h3><?php _e('Registered Courses', 'learnplus'); ?></h3></div>
                <div class ="price" style="float:left; margin-right: 22%;">
                    <h3><?php _e('Price', 'learnplus'); ?></h3></div>
                <div class ="paytype" style="float:left; margin-right: 12%;">
                    <h3><?php _e('Payment type', 'learnplus'); ?></h3></div>
                
                
                <span style="margin-left: px;"><?php _e('Price', 'learnplus'); ?></span>
                <span style="margin-left: 180px;"><?php _e('Payment type', 'learnplus'); ?></span>

                <span class="ld_profile_status"><?php _e('Status', 'learnplus'); ?></span>
            </div>

            <div id="course_list" style="margin: 14px;">
                <?php
                $account1 = my_account($user_id);
                // print_r($account1);
                foreach ($account1 as $accounts) :
                    ?>
                    <div id='course-<?php echo esc_attr($user_id) . '-' . esc_attr($course->ID); ?>' >
                        <div class="list_arrow collapse flippable">  <h4> <?php echo $accounts[0]['meta_value']; ?></h4></div>
                        <div class="course" style="width: 30%;padding-right: 15px;margin-left: -13px;">  <h4> <?php echo $accounts[0]['meta_value']; ?></h4></div>
                        <div class="price" style="padding-left: 20px;">  <h4> <?php echo $accounts[1]['meta_value']; ?></h4></div>
                        <div class="paytype" style="padding-left: 20px;">  <h4> Stripe</h4></div>

                        <h4>

                            <div style="float: left;margin: 0px 148px 0px 200px;"><h4><?php echo $accounts[1]['meta_value']; ?> </h4></div>
                            <div style="float: left;margin: 0px 150px 0px 52px;">  <h4>  Stripe  <?php /* echo $accounts[2]['meta_value']; */ ?></h4> </div>
                            <div > <h4>  <?php echo $accounts[3]['meta_value']; ?></h4> </div>
                        </h4>
                                                            <div class="flip" style="display:none;">
                        
                                                                <div class="learndash_profile_heading course_overview_heading"><?php _e('Course Progress Overview', 'learnplus'); ?></div>
                        
                                                                <div>
                                                                    <dd class="course_progress" title='<?php echo sprintf(esc_html__('%s out of %s steps completed', 'learnplus'), $progress['completed'], $progress['total']); ?>'>
                                                                        <div class="course_progress_blue" style='width: <?php echo esc_attr($progress['percentage']); ?>%;'>
                                                                    </dd>
                        
                                                                    <div class="right">
    <?php echo sprintf(esc_html__('%s%% Complete', 'learnplus'), $progress['percentage']); ?>
                                                                    </div>
                                                                </div>
                        
    <?php if (!empty($quiz_attempts[$course_id])) : ?>
                                                                                    <div class="learndash_profile_quizzes clear_both">
                                            
                                                                                        <div class="learndash_profile_quiz_heading">
                                                                                            <div class="quiz_title"><?php _e('Quizzes', 'learnplus'); ?></div>
                                                                                            <div class="certificate"><?php _e('Certificate', 'learnplus'); ?></div>
                                                                                            <div class="scores"><?php _e('Score', 'learnplus'); ?></div>
                                                                                            <div class="quiz_date"><?php _e('Date', 'learnplus'); ?></div>
                                                                                        </div>
                                            
                            <?php foreach ($quiz_attempts[$course_id] as $k => $quiz_attempt) : ?>
                                <?php
                                $certificateLink = @$quiz_attempt['certificate']['certificateLink'];

                                $status = empty($quiz_attempt['pass']) ? 'failed' : 'passed';

                                $quiz_title = !empty($quiz_attempt['post']->post_title) ? $quiz_attempt['post']->post_title : @$quiz_attempt['quiz_title'];

                                $quiz_link = !empty($quiz_attempt['post']->ID) ? get_permalink($quiz_attempt['post']->ID) : '#';
                                ?>
            <?php if (!empty($quiz_title)) : ?>
                                                                                                                                        <div class='<?php echo esc_attr($status); ?>'>
                                                                                    
                                                                                                                                            <div class="quiz_title">
                                                                                                                                                <span class='<?php echo esc_attr($status); ?>_icon'></span>
                                                                                                                                                <a href='<?php echo esc_attr($quiz_link); ?>'><?php echo esc_attr($quiz_title); ?></a>
                                                                                                                                            </div>
                                                                                    
                                                                                                                                            <div class="certificate">
                <?php if (!empty($certificateLink)) : ?>
                                                                                                                                                                        <a href='<?php echo esc_attr($certificateLink); ?>&time=<?php echo esc_attr($quiz_attempt['time']) ?>' target="_blank">
                                                                                                                                                                            <div class="certificate_icon"></div></a>
                                    <?php else : ?>
                                        <?php echo '-'; ?>
                <?php endif; ?>
                                                                                                                                            </div>
                                                                                    
                                                                                                                                            <div class="scores"><?php echo round($quiz_attempt['percentage'], 2); ?>%</div>
                                                                                    
                                                                                                                                            <div class="quiz_date"><?php echo date_i18n('d-M-Y', $quiz_attempt['time']); ?></div>
                                                                                    
                                                                                                                                        </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                                                                                    </div>
    <?php endif; ?>
                                                            </div>
                        </h4>
                    </div>
<?php endforeach; ?>                        


            </div>
        </div> end desc -->
        
        <div class="container">
  <h3>Purchase History</h3>
  <table class="table table-striped table-bordered">
    <thead>
        <tr style="background-color: #3f4451;">
        <th style="color: #ffffff;">Registered Courses</th>
        <th style="color: #ffffff;">Price</th>
        <th style="color: #ffffff;">Payment type</th>
      </tr>
    </thead>
  <?php  $account1 = my_account($user_id);
                // print_r($account1);
                foreach ($account1 as $accounts) :
                    ?>
    <tbody>
      <tr>
        <td style="width: 42%;"><?php echo $accounts[0]['meta_value']; ?></td>
        <td style="width: 35%;"><?php echo $accounts[1]['meta_value']; ?></td>
        <td>Stripe</td>
      </tr>
      
    </tbody>
      <?php endforeach; ?>
  </table>
</div>
        
    </div>

    <div id="change_password" class="col-md-9">
        <form method='post' id="form1">
            <div class='mypageMyDetailsBox'>
                <h3 class="course-title titleSub" style="margin-left: 20px;"><?php esc_html_e('Change Password', 'learnplus') ?></h3>

                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6">
                        <label for="student_first_name">Current Password<label style ="color:red;">*</label></label>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="currentpassword" id="currentpassword" maxlength="20" minlength="3">
                            </div>
                            <span id="current" style="color:red"></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6">
                        <label for="student_first_name">New Password<label style ="color:red;">*</label></label>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="newpassword" id="newpassword" maxlength="20" minlength="3">
                            </div>
                        <span id="new" style="color:red"></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-lg-6">
                        <label for="student_first_name">Confirm Password<label style ="color:red;">*</label></label>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="confirmpassword" id="confirmpassword" maxlength="20" minlength="3">
                            </div>
                       <span id="confirm" style="color:red"></span>
                        </div>
                    </div>
                </div>
            </div>
                <div class = "button">
                    <button type="submit" class="btn btn-blue m-t-15 waves-effect subUpt" name='submit_update' value='Update' style="margin-left: 30px;">Add Student</button>
                </div>
            </form>
            </div>
        

        <?php 
        if (isset($_POST['submit_update'])) {
            $currentpassword = $_POST['currentpassword'];
            $newpassword = $_POST['newpassword'];
            require_once ABSPATH . 'wp-includes/class-phpass.php';
            $wp_hasher = new PasswordHash(8, true);
            $user_info = get_userdata($user_id);
            $user_pass = $user_info->user_pass;
            if ($wp_hasher->CheckPassword($currentpassword, $user_pass)) {
               wp_set_password( $newpassword, $user_id );
            }else{
                   echo'<script>alert("hrllo");'
                . ' $( "#current" ).text( "Please enter current passworduiayhfuidsafhguijui" ).show();'
                 . 'return false;</script>';
            }
        }
        ?>
       

<script>
jQuery( ".subUpt" ).click(function( event ) {
       if ($("#currentpassword").val() == "" ) {
            $( "#current" ).text( "Please enter current password" ).show();
    return false;
  }else{
      $( "#current" ).text( "Please enter current password" ).hide();
  }
       if ($("#newpassword").val() == "" ) {
            $( "#new" ).text( "Please enter new password" ).show();
    return false;
  }  else{
      $( "#new" ).text( "Please enter current password" ).hide();
  }
       if ($("#confirmpassword").val() == "" ) {
            $( "#confirm" ).text( "Please enter confirm password" ).show();
    return false;
  }  else{
      $( "#confirm" ).text( "Please enter current password" ).hide();
  }
  $( "#form1" ).submit();
});
</script>
    

    <?php
    if (is_user_logged_in()) {
        $fetched_user_id = get_current_user_id();
    } else if ($_SESSION['created_user_id_from_the_popup_reg_form'] != '') {
        $fetched_user_id = $_SESSION['created_user_id_from_the_popup_reg_form'];
    }

    if ($fetched_user_id) {
        $user_info = get_userdata($fetched_user_id);
        $user_email = $user_info->data->user_email;
        $first_name = get_user_meta($user_info->data->ID, 'first_name', true);
        $middle_name = get_user_meta($user_info->data->ID, 'middle_name', true);
        $last_name = get_user_meta($user_info->data->ID, 'last_name', true);
        $address = get_user_meta($user_info->data->ID, 'address', true);
        $business = get_user_meta($user_info->data->ID, 'business', true);
        $contact = get_user_meta($user_info->data->ID, 'contact', true);
        $referral_date = get_user_meta($user_info->data->ID, 'referral_date', true);
        $broker = get_user_meta($user_info->data->ID, 'broker', true);
        $career_counselor = get_user_meta($user_info->data->ID, 'career_counselor', true);
        $initial_contact = get_user_meta($user_info->data->ID, 'initial_contact', true);
        $coumpany_name = get_user_meta($user_info->data->ID, 'coumpany_name', true);
        $broker_may_contact = get_user_meta($user_info->data->ID, 'broker_may_contact', true);
        $do_not_contact = get_user_meta($user_info->data->ID, 'do_not_contact', true);
        $broker_active = get_user_meta($user_info->data->ID, 'broker_active', true);
        $type = get_user_meta($user_info->data->ID, 'type', true);
        $jurisdiction = get_user_meta($user_info->data->ID, 'jurisdiction', true);
        $number = get_user_meta($user_info->data->ID, 'number', true);
        $expiration_date = get_user_meta($user_info->data->ID, 'expiration_date', true);
        
//    $user_company_name_profession = get_user_meta( $user_info->data->ID, 'company_name_profession', true );
        ?>

        <div id="user_profile" class="col-md-9">
            <?php echo do_shortcode('[formidable id = 3]'); ?>
        </div>    
        <script type="text/javascript">
              
              jQuery(".frm_final_submit").click(function(){
                  alert("hjsadfgdsuajgfvij");
              });
              
              
              
            jQuery(document).ready(function () {
                
//              jQuery("input[name = frm_action]").val('update');   
//              jQuery("#field_ogv6x").after("<input name = 'id' value = '44' type= 'hidden'>");
                jQuery("#field_ogv6x").val("<?php echo $first_name; ?>");
                jQuery("#field_1haz0").val("<?php echo $middle_name; ?>");
                jQuery("#field_xt16n").val("<?php echo $last_name; ?>");
                jQuery("#field_aoenw_line1").val("<?php echo $address['line1'];?>");            
                jQuery("#field_aoenw_line2").val("<?php echo $address['line2'];?>");            
                jQuery("#field_aoenw_city").val("<?php echo $address['city'];?>");            
                jQuery("#field_aoenw_state").val("<?php echo $address['state'];?>");            
                jQuery("#field_aoenw_zip").val("<?php echo $address['zip'];?>");            
                jQuery("#field_aoenw_country").val("<?php echo $address['country'];?>"); 
                
                var arrayValues = jQuery('input[type=radio]');
                       jQuery.each(arrayValues, function(val){ 
 
                    if(jQuery(this).val() == "<?php echo $business; ?>")
                    {
                    jQuery(this).prop('checked', true);
                    }
                  });
                
                jQuery("#field_joxri").val("<?php echo $contact;?>");            
                jQuery("#field_71x94").val("<?php echo $user_email;?>"); 
                jQuery("#field_71x94").attr('readonly','readonly');
                jQuery("#field_u7v6i").val("<?php echo $referral_date;?>"); 
                jQuery("#field_zmbz4").val("<?php echo $broker;?>"); 
                jQuery("#field_5kuv7").val("<?php echo $career_counselor;?>"); 
                jQuery("#field_j9v3n").val("<?php echo $initial_contact;?>"); 
                jQuery("#field_2fcwu").val("<?php echo $coumpany_name;?>"); 
                
                 
                var arrayValues = jQuery('input[name="item_meta[178]"]');
              //var arrayValues = jQuery('input[type=radio]');
    
                     jQuery.each(arrayValues, function(val){ 
 
                    if(jQuery(this).val() == "<?php echo $broker_may_contact; ?>")
                    {
                    jQuery(this).prop('checked', true);
                    }
                  });
                  
                 var arrayValues = jQuery('input[type=radio]');
                       jQuery.each(arrayValues, function(val){ 
 
                    if(jQuery(this).val() == "<?php echo $do_not_contact; ?>")
                    {
                    jQuery(this).prop('checked', true);
                    }
                  });
                  
                 var arrayValues = jQuery('input[type=radio]');
                       jQuery.each(arrayValues, function(val){ 
 
                    if(jQuery(this).val() == "<?php echo $broker_active; ?>")
                    {
                    jQuery(this).prop('checked', true);
                    }
                  });
                  
                jQuery("#field_i9nx9").val("<?php echo $type;?>"); 
                jQuery("#field_wjnpz").val("<?php echo $jurisdiction;?>"); 
                jQuery("#field_5xym8").val("<?php echo $number;?>");  
                jQuery("#field_48694a").val("<?php echo $expiration_date;?>"); 
    //					$("#field_wgq9u").val("<?php /*echo $fetched_user_id; */?>");
           
           });
            
            
            
        </script>
    <?php } ?>

         </div>
        
     <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.js"></script>    
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
    jQuery("#purchase_history").hide();
    jQuery("#change_password").hide();
    jQuery("#user_profile").hide();
    function get_allcourse() {
        var d = jQuery(".xyz");
        jQuery(".xyz").show();
    }
    function get_inprogress() {
        var a = jQuery(".abc");
        jQuery(".xyz").hide();
        a.each(function (key, value) {
            var b = value.textContent;
            b = b.replace(/\s/g, "");
            var c = b.split("%");
            e = parseInt(c[0]);
            var d = jQuery(".xyz");
            if (e > 0)
            {
                if (e != 100)
                {
                    jQuery(d[key]).show();
                }
            }
        });
    }
    function get_comleted() {
        jQuery(".xyz").hide();
        var a = jQuery(".abc");
        a.each(function (key, value) {
            var b = value.textContent;
            b = b.replace(/\s/g, "");
            var c = b.split("%");
            e = parseInt(c[0]);
            var d = jQuery(".xyz");
            if (e == 100)
            {
//                  alert(d);
//                  alert(key);
                jQuery(d[key]).show();
            }
        });
    }
    function get_history() {
        jQuery("#course-content").hide();
        jQuery("#purchase_history").show();
        jQuery("#change_password").hide();
        jQuery("#user_profile").hide();
    }

    function get_course() {
        jQuery("#course-content").show();
        jQuery("#purchase_history").hide();
        jQuery("#change_password").hide();
        jQuery("#user_profile").hide();

    }
    function get_password() {
        jQuery("#course-content").hide();
        jQuery("#purchase_history").hide();
        jQuery("#change_password").show();
        jQuery("#user_profile").hide();


    }
    function get_profile() {
        jQuery("#course-content").hide();
        jQuery("#purchase_history").hide();
        jQuery("#change_password").hide();
        jQuery("#user_profile").show();

    }
</script>