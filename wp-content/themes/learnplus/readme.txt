LearnPlus – Education LMS Courses Events WordPress Theme
------------------------------

LearnPlus is an unique and modern Learning Management System WordPress theme for WordPress built with Bootstrap and powered by Visual Composer. It provides awesome features for creating online courses, online shop, teacher profile, user profiles, lesson management, quiz system, questions system, event management etc.


Change log:

Version 1.1.4
- Update: Compatible WooCommerce 3.1.0
- Update: Visual Composer 5.2.
- Update: Revolution Slider 5.4.5.1
- Fix: Can't rating course when log in.

Version 1.1.3
- Update: Compatible WooCommerce 3.0.7
- Update: Add link to image of related courses

Version 1.1.2
- Update: Visual Composer 5.1.1
- Update: Revolution Slider 5.4.1
- Update: Compatible WooCommerce 3.0.1

Version 1.1.1
- Update: Visual Composer 5.0.1
- Update: Revolution Slider 5.3.1.5

Version 1.1
- Update: Style in the Theme Options.
- Update: Add new option allow to enter your google api key if Google Map doesn't display.
- Update: Add link for course image in course list, grid.
- Update: Add new options in the Theme Options->LMS->Course Rating allow disable rating in the single course.
- Fix: Can't change margin for logo in the Theme Options.
- Fix: The text in about slider is cut.


Version 1.0
- Initial
